#!/usr/bin/env bash

# scraped 20241016 from `man 5 githooks`
declare -ra gitValidHooks=(
  applypatch-msg commit-msg fsmonitor-watchman p4-changelist p4-post-changelist
  p4-prepare-changelist p4-pre-submit post-applypatch post-checkout post-commit
  post-index-change post-merge post-receive post-rewrite post-update
  pre-applypatch pre-auto-gc pre-commit pre-merge-commit prepare-commit-msg
  pre-push pre-rebase pre-receive proc-receive push-to-checkout
  reference-transaction sendemail-validate update
)

# $1=possibleHook
function isValidGitHook() {
  local possibleHook="$1"
  printf '%s\n' "${gitValidHooks[@]}" |
    grep --quiet --extended-regexp '^'"$possibleHook"'$'
}

thisScript="$(readlink -f "${BASH_SOURCE[0]}")"

function failureFindingHooks() {
  local failReason="${1:-'unknown reason'}"
  printf 'ERROR: cannot find git pre-commit hooks! %s

  ignore this by commenting out the "exit 1" line below this log; this file at:\n\t%s\n' \
    "$failReason" \
    "$thisScript" >&2
}

[[ -n "$HOSTFOREST_ROOT" ]] || {
  failureFindingHooks '$HOSTFOREST_ROOT missing'
  exit 1
}

declare -r hooksRootDir="$HOSTFOREST_ROOT"/common/hostless/git-hooks
[[ -n "$hooksRootDir" ]] || {
  failureFindingHooks 'missing subdir of all git hooks: '"$hooksRootDir"
  exit 1
}

hookName="$(basename "${BASH_SOURCE[0]}")"
isValidGitHook "$hookName" || {
  printf -- \
    'BUG: somehow executing under a non-valid git-hook name: "%s"\nactual: %s\t\n' \
    "$hookName" \
    "$thisScript" \
    >&2
  exit 1
}

declare -r hooksDir="$hooksRootDir"/"$hookName"
[[ -n "$hookName" && -n "$hooksDir" && -d "$hooksDir" ]] || {
  failureFindingHooks 'missing subdir of hook-specific ('"$hookName"') scripts: '"$hooksDir"
  exit 1
}

function listHookScripts() {
  find \
    "$hooksDir" \
    -mindepth 1 \
    -maxdepth 1 \
    -type f \
    \! \( -name '*.ignore' \) \
    \! \( -name '*.config' \) \
    -print
}

[[ "$(listHookScripts | wc -l)" -gt 0 ]] || {
  printf 'GIT HOOKS: no hook scripts found, skipping\n' >&2
  exit 0
}

printf --\
  'GIT HOOKS: evaluating all %s "%s"-hooks found: %s\n' \
  "$(listHookScripts | wc -l)" \
  "$hookName" \
  "$(printf ' "%s", ' $(listHookScripts | sed -e 's,.*/,,g'))" \
  >&2
while read -r hookScript; do
  hookScriptName="$(basename "$hookScript")"
  if [[ -e "$hookScript".ignore ]]; then
    printf -- \
      'SKIPPING "%s"-hook, disabled via .ignore file, "%s"\n' \
      "$hookName" \
      "$hookScriptName" >&2
    continue
  fi

  "$hookScript" || {
    printf -- \
      'HALTING for git "%s"-hook: "%s"; see failure error message above

      TIP: quickly disable this hook by `touch`ing this file:
        %s.ignore\n' \
      "$hookName" \
      "$hookScriptName" \
      "$(readlink -f "$hookScript")"
    exit 1
  }
done < <(listHookScripts)

echo >&2
exit 0
