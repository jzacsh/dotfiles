#!/usr/bin/env bash

# Have bash apply its own command-completion (ie: behavior you see when you
# tab-complete at an empty prompt) to the first arguments of these programs.
# See complete's documentation of "-A action" under "SHELL BUILTIN COMMANDS" of
# bash(1), or visit the web version of that place in the docs:
# https://manpages.debian.org/bullseye/bash/bash.1.en.html#command
complete -A command descmd
complete -A command pagecmd
