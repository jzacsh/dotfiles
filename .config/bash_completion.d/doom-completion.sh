#!/usr/bin/env bash

declare -ar __doomIwadCompletionDirs=(
  /usr/share/games/doom
)

function __doomIwadCompletion() {
  [[ "${COMP_WORDS[COMP_CWORD-1]}" = -iwad ]] || return 1

  local dirs_to_search; dirs_to_search=()

  for dir in "${__doomIwadCompletionDirs[@]}"; do
    [[ -e "$dir" ]] || continue
    dirs_to_search+=( "$dir" )
  done

  # TODO figure out how to fallback to completion behavior of _any_ file rather
  # than just .wad files. That is: something similar in spirit to
  # `yblib.cloneCompletion file doom` but would let us hand-off the work to the
  # other completion _right now_rather than bind the completion for the _future_.
  [[ "${#dirs_to_search[@]}" -gt 0 ]] || return 1

  nextArgs_wads="$(
    find \
      "${dirs_to_search[@]}" \
      "$PWD" \
      -mindepth 1 -maxdepth 1 \
      -type f \
      -regex '.*\.wad$' \
      -printf '%P\n'
  )"

  COMPREPLY=( $(compgen -W "${nextArgs_wads[@]}" "${COMP_WORDS[$COMP_CWORD]}") )
}


function __doomFlagCompletion() {
  local curr="${COMP_WORDS[$COMP_CWORD]}"
  local first_letter="${curr:0:1}"
  [[ "$first_letter" = - ]] || return 1

  local __doomFlagCompletion_subcmds
  declare -a __doomFlagCompletion_subcmds=(
    complevel
    width
    height
    viewangle
    vidmode
    fullscreen
    nofullscreen
    window
    nowindow
    iwad
    file
    deh
    loadgame
    warp
    skill
    respawn
    fast
    nomonsters
    net
    port
    deathmatch
    altdeath
    timer
    avg
    solo-net
    record
    playdemo
    timedemo
    fastdemo
    ffmap
    nosound
    nosfx
    nomusic
    nojoy
    nomouse
    noaccel
    1
    2
    3
    config
    save
    devparm
    debugfile
    nodrawers
    noblit
    bexout
  )

  local subcmds="$(printf ' -%s ' "${__doomFlagCompletion_subcmds[@]}")"
  COMPREPLY=( $(compgen -W "$subcmds" -- "${COMP_WORDS[$COMP_CWORD]}") )
}

function __doomCompletions() {
  __doomFlagCompletion ||
    __doomIwadCompletion
}
complete -F __doomCompletions doom prboom
# "doom" is my local alias for prboom
