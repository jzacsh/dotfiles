--[[
lualib: language basics I want when interacting with lua.

By "language" basics I mean stdlib~esque things (not language-keywords).
--]]
lualib = {
  -- Basic string helpers
  -- See http://lua-users.org/wiki/StringRecipes for more help
  str = {},

  -- Basic type coercion helpers.
  any = {},

  -- Basic environment variable helpers.
  env = {},
}

lualib.str.IsEmpty = function(s)
  return s == nil or s == ''
end

lualib.str.IsPresent = function(s)
  return not lualib.str.IsEmpty(s)
end

lualib.str.Ends = function(haystack, needle)
  return lualib.str.IsEmpty(needle) or (
    lualib.str.IsPresent(haystack)
    and haystack:sub(-#needle) == needle
  )
end

lualib.any.ToBool = function(val)
  if type(val) == "boolean" then
    return val
  end
  if type(val) == "number" then
    return not (val == 0)
  end
  if type(val) == "string" then
    return val ~= '' and val ~= nil and string.lower(val) ~= 'false' and string.lower(val) ~= '0'
  end
  return val ~= nil
end

lualib.env.IsPresent = function(val)
  envVar = os.getenv(val)
  return envVar ~= nil and envVar ~= ''
end

lualib.env.IsMissing = function(val)
  return not lualib.env.IsPresent(val)
end
