# nvim lua runtime codebase

This subdir was originally a fork of the template provided by
https://github.com/nvim-lua/kickstart.nvim (at their fad8c304).

## Status: jzacsh's neovim/vim usage is UNDER CONSTRUCTION

my nvim settings are broken and in-flight. if this readme is merged to main it's
an accident.

I'm currently experimenting with kickstart-nvim after 15 years of using vim
(and nvim) happily. So I'm taking this as a multi-step process:

- [x] mark where my old-world (on `main`) left-off happily working: `bdd7b9fe`
- [x] test out kickstar.nvim's newer opinionated layout in a branch
- [ ] get used to the new setup for a while (days? weeks?)
  - [ ] personal usage; all good?
    - [x] p0: re-fix `nvim -u ~/.vimrc` (fix: ensure `runtimepath` includes
         ~/.vim/ and co)
      - [x] ensure it's really working again by: `rm -rf ~/.local/share/vim/`
            first, then opening nvim
    - [x] p1: disable auto-insert of _any_ type (Eg: close-parenthesis,
          curlies, ticks, etc) (fix: this was being done with a tiny
          optionally-enabled script that shipped with kickstart.nvim project at
          `./lua/kickstart/plugins/autopairs.lua`)
    - [x] fix bug: seeing (fix: required `markdownlint-cli` be in `$PATH`)

      ```none
      Error detected while processing BufWritePost Autocommands for "*":
      Error running markdownlint: ENOENT: no such file or directory
      ```

      ```none
      E5108: Error executing lua: ...share/nvim/lazy/telescope.nvim/lua/telescope/pickers.lua:762: BufEnter Autocommands for "*": Vim(append):Error running markdownlint: ENOENT: no such file or directory
      stack traceback:
        [C]: in function 'nvim_win_close'
        ...share/nvim/lazy/telescope.nvim/lua/telescope/pickers.lua:762: in function 'close_windows'
        ...share/nvim/lazy/telescope.nvim/lua/telescope/pickers.lua:1508: in function 'on_close_prompt'
        .../nvim/lazy/telescope.nvim/lua/telescope/actions/init.lua:338: in function 'run_replace_or_original'
        ...re/nvim/lazy/telescope.nvim/lua/telescope/actions/mt.lua:65: in function 'key_func'
        ...hare/nvim/lazy/telescope.nvim/lua/telescope/mappings.lua:253: in function <...hare/nvim/lazy/telescope.nvim/lua/telescope/mappings.lua:252>
      ```

    - [x] lazy install: setup NERDComment, so T hotkey works
    - [x] lazy install: setup physics-scroll
    - [x] fix over-zealous clipboard syncing; every single yank is polutting my
      clipboard and it turns out to be a bit much.
      - fixed: via 'unnamedplus' commented out (left in palce because it's a
      neat trick that was definitely not possible when I last wanted it 10 years ago)
    - [x] fix inability to backspace when in insert mode (via ^H); seems NOT
          to be  a mismatch in nocompat
  - [x] personal usage across multiple laptops; all good?
  - [x] corp+personal usage; all good?
- [ ] manually walk through my old configs (see first bullet) and port over:
  - [ ] any plugins I still want
    - [ ] get shellcheck working again
    - [x] github.com/luochen1990/rainbow
    - [x] github.com/mbbill/undotree sjl/gundo.vim to replace github.com/sjl/gundo.vim
       - [x] in vendored-world (ie: vim) replace gundo.vim with newer mbbill/undotree
    - [x] installed github.com/chrisbra/csv.vim
    - [x] installed github.com/rust-lang/rust.vim
    - [x] installed github.com/mhinz/vim-signify
    - [x] installed github.com/rafi/awesome-vim-colorschemes
    - [ ] vcscommand.vim is _ANCIENT_; find something maintained? or maybe
          tackle this myself as part of my other vcs work?
    - [x] installed github.com/nvim-telescope/telescope-file-browser.nvim
  - [ ] AutoFormatBuffer?
  - [x] any hotkeys I still want
  - [x] ensure _something basic_ still works with machines with only vim (eg:
       my .vimrc fallback works at my left-off point, but neovim configs
       takeover when appropriate. is that still true in this new world? if not,
       can we fix it easily?)
- [x] audit things I'm not using in the original fork: what else is in this
      directory that I don't want?
- [x] document the forked content's license and delete the license file as
      that's confusing to be sitting in my folder for which I only keep "how to
      use which-key" config lines (or move the license file to whatever
      subfolder I actually keep).
- [x] unvendor kickstart and just leave it inlined...
   - [x] inline the contents as plain files `.config/nvim`, tracked by dotfiles repo itself
   - [x] delete my fork https://gitlab.com/jzacsh/kickstar-nvim
- [x] merge to main and profit
    - [x] pre-reqs: update this readme to for "prod" (merge to `main`) usage:
       - [x] outline the history for quick lookup (eg: the first bullet)
       - [x] outline what I've learned about the conventions I've now adopted
         from kickstart
        - which-key.nvim is a brilliant idea
        - lua is a million times nicer than vimscript (not really news)
        - it's possible to use vimscript as a bunch of chunks, split up
       - [x] outline how it's maybe different (eg: given my dotfiles' `./vendor/`)
        - lazy save sfiles to .local/share/nvim/ but I'm sure I _could_
        configure that, and therefore maybe just erradicate all the custom code
        I have for /vendor/ in this directory? (but maybe not because Lazy is
        specific to vim repos, and I've already benefited from /vendor/ being a
        general toolchain).
    - [ ] now that we're in a new world: do annoying systems like java LSP
      finally "just work"?!?

## Introduction

A starting point for Neovim that is:

* Small
* Single-file
* Completely Documented

**NOT** a Neovim distribution, but instead a starting point for your configuration.

## Installation

### Install Neovim

Kickstart.nvim targets *only* the latest
['stable'](https://github.com/neovim/neovim/releases/tag/stable) and latest
['nightly'](https://github.com/neovim/neovim/releases/tag/nightly) of Neovim.
If you are experiencing issues, please make sure you have the latest versions.

### Install External Dependencies

External Requirements:
- Basic utils: `git`, `make`, `unzip`, C Compiler (`gcc`)
- [ripgrep](https://github.com/BurntSushi/ripgrep#installation)
- Clipboard tool (xclip/xsel/win32yank or other depending on platform)
- A [Nerd Font](https://www.nerdfonts.com/): optional, provides various icons
  - if you have it set `vim.g.have_nerd_font` in `init.lua` to true
- Language Setup:
  - If you want to write Typescript, you need `npm`
  - If you want to write Golang, you will need `go`
  - etc.

> **NOTE**
> See [Install Recipes](#Install-Recipes) for additional Windows and Linux specific notes
> and quick install snippets

### Install Kickstart

> **NOTE**
> [Backup](#FAQ) your previous configuration (if any exists)

Neovim's configurations are located under the following paths, depending on your OS:

| OS | PATH |
| :- | :--- |
| Linux, MacOS | `$XDG_CONFIG_HOME/nvim`, `~/.config/nvim` |
| Windows (cmd)| `%localappdata%\nvim\` |
| Windows (powershell)| `$env:LOCALAPPDATA\nvim\` |

#### Recommended Step

[Fork](https://docs.github.com/en/get-started/quickstart/fork-a-repo) this repo
so that you have your own copy that you can modify, then install by cloning the
fork to your machine using one of the commands below, depending on your OS.

> **NOTE**
> Your fork's url will be something like this:
> `https://github.com/<your_github_username>/kickstart.nvim.git`

You likely want to remove `lazy-lock.json` from your fork's `.gitignore` file
too - it's ignored in the kickstart repo to make maintenance easier, but it's
[recommmended to track it in version control](https://lazy.folke.io/usage/lockfile).

#### Clone kickstart.nvim
> **NOTE**
> If following the recommended step above (i.e., forking the repo), replace
> `nvim-lua` with `<your_github_username>` in the commands below

<details><summary> Linux and Mac </summary>

```sh
git clone https://github.com/nvim-lua/kickstart.nvim.git "${XDG_CONFIG_HOME:-$HOME/.config}"/nvim
```

</details>

<details><summary> Windows </summary>

If you're using `cmd.exe`:

```
git clone https://github.com/nvim-lua/kickstart.nvim.git "%localappdata%\nvim"
```

If you're using `powershell.exe`

```
git clone https://github.com/nvim-lua/kickstart.nvim.git "${env:LOCALAPPDATA}\nvim"
```

</details>

### Post Installation

Start Neovim

```sh
nvim
```

That's it! Lazy will install all the plugins you have. Use `:Lazy` to view
current plugin status. Hit `q` to close the window.

Read through the `init.lua` file in your configuration folder for more
information about extending and exploring Neovim. That also includes
examples of adding popularly requested plugins.


### Getting Started

[The Only Video You Need to Get Started with Neovim](https://youtu.be/m8C0Cq9Uv9o)

### FAQ

* What should I do if I already have a pre-existing neovim configuration?
  * You should back it up and then delete all associated files.
  * This includes your existing init.lua and the neovim files in `~/.local`
    which can be deleted with `rm -rf ~/.local/share/nvim/`
* Can I keep my existing configuration in parallel to kickstart?
  * Yes! You can use [NVIM_APPNAME](https://neovim.io/doc/user/starting.html#%24NVIM_APPNAME)`=nvim-NAME`
    to maintain multiple configurations. For example, you can install the kickstart
    configuration in `~/.config/nvim-kickstart` and create an alias:
    ```
    alias nvim-kickstart='NVIM_APPNAME="nvim-kickstart" nvim'
    ```
    When you run Neovim using `nvim-kickstart` alias it will use the alternative
    config directory and the matching local directory
    `~/.local/share/nvim-kickstart`. You can apply this approach to any Neovim
    distribution that you would like to try out.
* What if I want to "uninstall" this configuration:
  * See [lazy.nvim uninstall](https://lazy.folke.io/usage#-uninstalling) information
* Why is the kickstart `init.lua` a single file? Wouldn't it make sense to split it into multiple files?
  * The main purpose of kickstart is to serve as a teaching tool and a reference
    configuration that someone can easily use to `git clone` as a basis for their own.
    As you progress in learning Neovim and Lua, you might consider splitting `init.lua`
    into smaller parts. A fork of kickstart that does this while maintaining the
    same functionality is available here:
    * [kickstart-modular.nvim](https://github.com/dam9000/kickstart-modular.nvim)
  * Discussions on this topic can be found here:
    * [Restructure the configuration](https://github.com/nvim-lua/kickstart.nvim/issues/218)
    * [Reorganize init.lua into a multi-file setup](https://github.com/nvim-lua/kickstart.nvim/pull/473)

### Install Recipes

Below you can find OS specific install instructions for Neovim and dependencies.

After installing all the dependencies continue with the [Install Kickstart](#Install-Kickstart) step.

#### Windows Installation

<details><summary>Windows with Microsoft C++ Build Tools and CMake</summary>
Installation may require installing build tools and updating the run command for `telescope-fzf-native`

See `telescope-fzf-native` documentation for [more details](https://github.com/nvim-telescope/telescope-fzf-native.nvim#installation)

This requires:

- Install CMake and the Microsoft C++ Build Tools on Windows

```lua
{'nvim-telescope/telescope-fzf-native.nvim', build = 'cmake -S. -Bbuild -DCMAKE_BUILD_TYPE=Release && cmake --build build --config Release && cmake --install build --prefix build' }
```
</details>
<details><summary>Windows with gcc/make using chocolatey</summary>
Alternatively, one can install gcc and make which don't require changing the config,
the easiest way is to use choco:

1. install [chocolatey](https://chocolatey.org/install)
either follow the instructions on the page or use winget,
run in cmd as **admin**:
```
winget install --accept-source-agreements chocolatey.chocolatey
```

2. install all requirements using choco, exit previous cmd and
open a new one so that choco path is set, and run in cmd as **admin**:
```
choco install -y neovim git ripgrep wget fd unzip gzip mingw make
```
</details>
<details><summary>WSL (Windows Subsystem for Linux)</summary>

```
wsl --install
wsl
sudo add-apt-repository ppa:neovim-ppa/unstable -y
sudo apt update
sudo apt install make gcc ripgrep unzip git xclip neovim
```
</details>

#### Linux Install
<details><summary>Ubuntu Install Steps</summary>

```
sudo add-apt-repository ppa:neovim-ppa/unstable -y
sudo apt update
sudo apt install make gcc ripgrep unzip git xclip neovim
```
</details>
<details><summary>Debian Install Steps</summary>

```
sudo apt update
sudo apt install make gcc ripgrep unzip git xclip curl

# Now we install nvim
curl -LO https://github.com/neovim/neovim/releases/latest/download/nvim-linux64.tar.gz
sudo rm -rf /opt/nvim-linux64
sudo mkdir -p /opt/nvim-linux64
sudo chmod a+rX /opt/nvim-linux64
sudo tar -C /opt -xzf nvim-linux64.tar.gz

# make it available in /usr/local/bin, distro installs to /usr/bin
sudo ln -sf /opt/nvim-linux64/bin/nvim /usr/local/bin/
```
</details>
<details><summary>Fedora Install Steps</summary>

```
sudo dnf install -y gcc make git ripgrep fd-find unzip neovim
```
</details>

<details><summary>Arch Install Steps</summary>

```
sudo pacman -S --noconfirm --needed gcc make git ripgrep fd unzip neovim
```
</details>

