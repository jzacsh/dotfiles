# Script Forest
Allows for machine and OS-specific dotfiles and environment scripts.

This is an _organizational_ solution to a very specific problem: using the same
exact dotfiles repo for every machine I have an account on, but wanting
specific shell functions and dotfiles configured per-machine or per-distro.

## Forest Entry
Run [`pick`](pick) script from interactive or non-interactive shell start-up _(I use
`~/.bashrc`)_, causing the following under `$HOSTFOREST_ROOT/*`:
+ **distro**-specific scripts to trigger, prefixed with "distro." (eg: [`distro.ubuntu`](distro.ubuntu))
+ **host**-specific scripts to trigger, prefixed with "host." (eg: [`host.jznuc`](host.jznuc))

Note: Directories in this tree are arbitrary structures various scripts make
use of and expect to find. There is no logic in `pick` that is aware of
directories.

### Diagram of Execution on Arch Linux
```
  +--------------------+      +-----------------------------------+
  | bashrc calls `pick`| -->  | `pick` runs distro and host files |
  +--------------------+      +-----------------------------------+
                                               |
                                               |
                                   +------------------------+        
                                   | `$ source distro.arch` |
                                   +------------------------+
                                               |
                                               |
                                  +--------------------------+        
                                  | `$ source host.blackbox` |
                                  +--------------------------+
```

## Layout

The layout of files in this repo _try_ to follow some conventions. Here are the
conventions:

- `pick` - The core runner of the dotforest. This is the script that you invoke
  yourself (eg: via `~/.bashrc`) so that all of the host-specific logic in the
  files below will be run or sourced as appropriate
- `common/` - for scripts/sources run by myriad hosts, but not run by **every**
  host (if that was the case, the code should have lived in `~/.bashrc`
  directly). Uses of content in common/ can be found by simply grepping for
  `common/` in the root files and the `src/` files described in the next
  bullets.
- `host.*` - a file to be automatically sourced by `pick` when the host matches the
  current machine (eg `host.voyager` for my personal laptop that has the
  `$HOSTNAME` `voyager`).
- `distro.*` - like host files, but for distributions. Currently detected
  distrubtions are `arch` (for Arch Linux), `debian`, or any distribution that
  sets a `DISTRIB_ID` in their `/etc/lsb-release` file (eg `ubuntu`). Simply
  update `pick` logic to support new ones.
- `deskenv.*` - like host files, but for desktop environments (eg `gnome`) that
  set declare themselves via `$DE` or `$DESKTOP_SESSON`.
- `domain.*` - like host, but the domain of the machine - usually doesn't come
  up on personal devices, but does on corporate devices.
- `src/` - Unlike `common/` which holds arbitrary scripts and sources, this tree
  mostly contains override files that are different than the standard ones in
  the main outer repo. Eg: if a specific host (`$HOSTNAME=voyager`) has a
  radically differnet `~/.vimrc`. a voyager-specific vimrc should be stored in
  src nad the `host.voyager` script can than it's symlinked.
  `common/hostforest` contains helpers on the matter of symlinking (and
  sourcing, and more) in a reliable, and hopefully omnipotent manner.
- `src/host.*/` - files created for no other purpose than to be a host-specific
  fork of some dotfile. eg `src/host.voyager/.vimrc` might get symlinked by
  `./host.voyager`.
- `src/distro.*/` - Same as `src/host.*/` files but for domains.

Paths in this table are relative to this repo, which is typically `$HOSTFOREST_ROOT`,
but _most_ code in this repo tries not to hardcode references to `$HOSTFOREST_ROOT` so
that this can be changed easily in the future.

## Caching

TODO: Explain very briefly how caching of executions works and point out
`hostforest_clear_cache` helper.

## Corporate Environments

For proprietary environments where dotfiles are more sensitive, I keep a
corporate subset of the content described above not in `"$HOSTFOREST_ROOT` but in
`$HOME/.config/dotforest.corp/`. If that corp directory exists, then `pick` will run each of
the following, if they exist:

1. `source ~/.config/dotforest.corp/pre-pick`
1. During the distro,host,domain,etc. `pick` runs, if there's a match in
   `~/.config/dotforest.corp/` directory than that match will be picked and any
   matches in `$HOSTFOREST_ROOT` will be ignored.

   - beware the ignoring part puts a small extra burden on corp files to make an
     explicit opt-in where necessary. For example, if the
     `$HOSTFOREST_ROOT/distro.debian` script is acceptable and should be run, despite a
     corp-specific debian file existing, then something in the
     `~/.config/dotforest.corp/distro.debian` should source the non-corp
     version itself.
1. `source ~/.config/dotforest.corp/post-pick`

This means that _if_ you have a corp repo, it should be laid out very similarly
to a regular dotforest repo (per [above layout](#Layout)).
