#!/usr/bin/env bash
#
# Aliases and functions that are specific Debian distros. Intended to run on every
# ~/.bashrc invocation.

# Specifically on debian-based distros, we install golang manually so need to
# install its hand-crafted path ourselves. On rolling distros like arch we can
# get the latest golang automatically installed like any other binary.
export PATH="$PATH":"/usr/local/go/bin"

export DEBFULLNAME='Jonathan Zacsh'
export DEBEMAIL='j@zac.sh'
export CLOSURE_COMPILER="$HOME/usr/lib/js/closure-compiler"

alias pman='pydoc3.1'
alias pdb='pdb3.1'
