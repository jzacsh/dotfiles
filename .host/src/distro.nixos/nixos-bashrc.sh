#!/usr/bin/env bash

# Both ensure our config is updated (installed) and our systems' packages are
# updated (upgraded).
#
# this is the most convenient command I'm running repeatedly, and it even
# calls the _other_ command (nixforest-update). so it's a safe bet.
function nixforest() {
  if nixforest-install -q; then
    logfInfo 'configs already installed. running updater...\n'
    nixforest-update
    return $?
  fi

  # when -install is done with its work it offers to do an update
  nixforest-install
}

export HOSTFOREST_NVIM_HOSTADD_COMPAT_VIM="$HOSTFOREST_DISTRO_SRC"/.vimrc

# per https://nixos.wiki/wiki/Fzf#bash
# shellcheck disable=1091
#   disabled shellcheck as it's just overreaching/false-positive
if haveCallable fzf-share; then
  source "$(fzf-share)/key-bindings.bash"
  source "$(fzf-share)/completion.bash"
fi

# TODO delete this when upstream gets fzf-tmux completion
yblib.cloneCompletion fzf fzf-tmux

# WARNING: this must be kept in sync with my ~/.bash_aliases file where I
# define fzf-related helpers
yblib.cloneCompletion fzf fz
yblib.cloneCompletion fzf pz
yblib.cloneCompletion fzf fze
yblib.cloneCompletion fzf pze

# https://nixos.org/manual/nix/stable/command-ref/env-common#env-IN_NIX_SHELL
function isInNexShell() { [[ "${IN_NIX_SHELL:-pure}" = impure ]]; }

# Define our own prompt so we still get the benefit of ours without the
# disorientation of forgetting we're in a nix-shell.
#
# Setting NIX_SHELL_PRESERVE_PROMPT is the API for telling `nix-shell` not to
# override our PS1, per https://github.com/NixOS/nix/issues/1268
export NIX_SHELL_PRESERVE_PROMPT=1
function maybeNixshellPrompt() {
  isInNexShell || {
    echo -n '$'
    return 0
  }

  local msg=NIXSHELL
#TODO fix this feature; something about printing escape codes here causes my
#terminal to go wonky.
# if yblib.hasColor; then
#   local col_end='\033[0m' col_red='\e[1;31m'
#   printf -v msg -- "${col_red}%s${col_end}" "$msg"
# fi
  echo -en "[$msg]"
}
dotforest_setPrompts maybeNixshellPrompt

# Performs a broad search for nix packages available packages (not necessarily
# installed on current profile).
#
# $1=package substr
# $@ optionally any extra args to pass to nix-env
function nxpk.findInstalled() ( # subshell for set -x
  local query pkg_substr="$1"; shift
  printf -v query -- '.*%s.*' "$pkg_substr"

  set -x
  nix-env --available --attr-path --query --out-path "$@" "$query"
)

# $1=exact package name (as printed by `nxpk.findInstalled`'s first column) or
#    query in which case if multiple package matches exist the output from this
#    function will likely be less useful
function nxpk.findPath() ( #  subshell so requireCallable can `exit` safely
  requireCallable jq

  nxpk.findInstalled "$1" --json |
   jq --raw-output 'to_entries[] | .value.outputs.out'
)

if haveCallable rlwrap; then
  alias replnix='rlwrap --no-children --always-readline nix repl'
else
  logfError 'missing dependency "rlwrap" on this system?\n'
  alias replnix='nix repl' # :( no rlwrap
fi
