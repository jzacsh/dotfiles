# nixos sources common to all hosts

- `./nix-common-configs/` - `*.nix` files linked to by host-specific sources'
  `nixos/system/*` directories (eg: `$HOSTFOREST_ROOT/src/host.bar/nixos/system`
- `./bin/*` scripts that try to help keep the above configs in sync with
  `/etc/nixos/` files.
