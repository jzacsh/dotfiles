# Edit this configuration file to define what should be installed on
# personal computers (eg: desktops and laptops you'll probably have a graphical
# environment on, listen to music on, etc).
#
# Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{
  nixpkgs,
  config,
  pkgs,
  ...
}:

{
  # per https://nixos.wiki/wiki/Thunderbolt this might help (though I use gnome
  # and am still having troubles on a thinkpad x1 nano).
  services.hardware.bolt.enable = true;

  # Enable the X11 windowing system.
  services.xserver.enable = true;

  # Enable https://uhk.io clientside program to run and manage its keyboard
  hardware.keyboard.uhk.enable = true;

  # Enables non-root access per https://wiki.nixos.org/w/index.php?title=Qmk
  hardware.keyboard.qmk.enable = true;

  services.udev.packages = with pkgs; [
    # tells udev to let `via` app access hardware; found at:
    # https://www.reddit.com/r/olkb/comments/roipbw/comment/ivnmc6a/
    via

    # per https://wiki.nixos.org/wiki/Yubikey#GPG_and_SSH
    yubikey-personalization
  ];

  # per https://wiki.nixos.org/wiki/Yubikey#Smartcard_mode and per yubico docs,
  # required for yubioath-flutter
  services.pcscd.enable = true;

  # Enable the GNOME Desktop Environment.
  services.xserver.displayManager.gdm.enable = true;
  services.xserver.desktopManager.gnome.enable = true;

  # Personal computers are almost always laptops; a laptop receiving charge
  # _and_ actively plugged into an external monitor (eg: a thunderbolt 4
  # power/video cable) shouldn't go to sleep when the lid is shut, as the desk
  # space where it lives might just be getting tidied.
  services.logind.lidSwitchDocked = "lock";

  # Enable touchpad support (enabled default in most desktopManager).
  services.libinput.enable = true;
  #20221024 jzacsh uncommented for framework v1 laptop debugging

  # Enable fingerprint support
  #   Utilize by running `fprintd-enroll` as normal user
  #services.fprintd.enable = false;
  #20230916 jzacsh confirmed this works, but is unnecessary since nixos-hardware
  #does this for us for the "framework" config we depend on (elsewhere in our
  #configs)
  #20221024 jzacsh uncommented for framework v1 laptop debugging
  #
  #20240205 jzacsh explicitly disabled because this is so buggy it often locks
  #out normal text-password entry
  services.fprintd.enable = false;

  # Enable thermal daemon
  services.thermald.enable = true;
  #20221024 jzacsh uncommented for framework v1 laptop debugging

  programs.ssh.startAgent = true;

  # Enable CUPS to print documents.
  services.printing.enable = true;
  # brother laser printers
  services.printing.drivers = [
    pkgs.brlaser
    pkgs.brgenml1lpr
    pkgs.brgenml1cupswrapper
  ];
  services.avahi.enable = true;
  services.avahi.nssmdns4 = true;
  services.avahi.openFirewall = true;

  # Enable sound with pipewire, per https://nixos.wiki/wiki/PipeWire#Enabling_PipeWire
  hardware.pulseaudio.enable = false;
  security.rtkit.enable = true;
  services.pipewire = {
    enable = true;
    alsa.enable = true;
    alsa.support32Bit = true;
    pulse.enable = true;
    # If you want to use JACK applications, uncomment this
    #jack.enable = true;

    # use the example session manager (no others are packaged yet so this is enabled by default,
    # no need to redefine it in your config for now)
    #media-session.enable = true;
  };

  # enable SANE system of drives scanner support per
  # https://nixos.wiki/wiki/Scanners
  hardware.sane.enable = true;

  fonts = {
    enableDefaultPackages = true;

    # Fonts have to be turned on (not _just_ installed over in our packages
    # listing), per https://nixos.wiki/wiki/Fonts#Installing_fonts_on_NixOS
    packages = with pkgs; [
      fira-code
      fira-code-symbols

      nerd-fonts.go-mono
      nerd-fonts.fira-code
      nerd-fonts.noto
      nerd-fonts.ubuntu
      nerd-fonts.ubuntu-mono
      nerd-fonts.ubuntu-sans
      nerd-fonts.droid-sans-mono
    ];

    fontDir.enable = true;
    fontconfig = {
      enable = true;
      defaultFonts = {
        monospace = [ "FiraCode Nerd Font" ];
      };
    };
  };

  # TODO unsure if this works; cobbled together from:
  # - popup from firefox (while using discord) that linked me to:
  #   https://support.mozilla.org/en-US/kb/speechd-setup?as=u&utm_source=inproduct
  # - web search on that^ leading me to: https://github.com/NixOS/nixpkgs/issues/78880
  # - https://discourse.nixos.org/t/no-tts-in-reader-mode-in-firefox/19475/4
  # - https://nixos.wiki/wiki/Firefox
  # nixpkgs.config.firefox.speechSynthesisSupport = true;

  nixpkgs.config.permittedInsecurePackages = [
    # TODO find out if there's a way to say "logseq" is allowed to have insecure
    # dependencies, but not "electron-x.y.z" _generally_ (that is: we don't want
    # to be blanket-approving any toher users of this package).
    "electron-27.3.11"
  ];
}
