{ config, pkgs, ... }:

{
  # Remember to add critical packages you expect, eg: an editor, common posix
  # utils, etc.
  environment.systemPackages = with pkgs; [
    #
    # system administration
    #
    neovim
    binutils # things like `strings` command
    alacritty
    ghostty
    borgmatic
    nano
    gnupg
    curl
    wget
    tmux
    less
    sourceHighlight
    ascii
    # bug in https://github.com/NixOS/nixpkgs/blob/22.05/nixos/modules/programs/less.nix
    # lesspipe is the default choice of less package, but less package doesn't
    # install it as a dependency, so we do it manually here:
    lesspipe
    lynx
    procs
    htop
    btop # better htop
    bash
    bashInteractive
    bash-completion
    nix-bash-completions
    file
    memtest86plus
    zip
    unzip
    _7zz # 7zip (nix config files don't allow starting with digits)
    nix-index # enables nix-locate (like older `locate`)
    nix-tree # enables robust introspection about packages
    bc
    # Easier disk-space utilization analyzers
    dua # --interactive here is an amaazing TUI
    du-dust # du but with a nicer ux
    rlwrap
    detox

    # Hardware management
    libinput
    usbutils
    pciutils
    # like xev, but for wayland; helpful when I'm debugging my own desktop
    # experience.
    wev

    #
    # PIM and other core utilities for people-things
    #
    # TODO(jzacsh) a lot of this file should be in
    # system-personal-computer.nix; see if we can append systemPackages from
    # _any_ .nix (eg via `++`).
    #
    # expliictly disabling as its been replaced by yubioath-flutter; do NOT
    # install it, because having it available is just going to cause confusion
    # (and re-discovery of its deprecation).
    #yubikey-manager-qt
    #
    # note yubioath-flutter requires steps from
    # https://nixos.wiki/wiki/Yubikey like enabling pcscd via config elsewhere
    yubioath-flutter

    bitwarden-cli
    bitwarden
    signal-desktop
    keepassxc
    libsecret

    #
    # basics expected of a desktop
    #
    #graphics utilities (think of Photoshop)
    inkscape
    gimp
    drawio
    imagemagick
    firefox
    firefox-wayland
    thunderbird
    google-chrome
    #office suite (text editor, spreadsheets, etc.)
    libreoffice-fresh
    #webcam desktop-app
    cheese
    wordbook
    gnome-tweaks
    gnome-decoder
    vlc
    mpv
    ffmpeg

    # TODO(feature) find new dictionary provider; dictd (and the reliant
    # gnome-dictionary) are dead, according to the author of gnome-dictionary.
    # Maybe libreoffice has one built in (that can be reused more easily than
    # opening libreoffice)? Or maaybe:
    # - https://github.com/TheOpenDictionary/odict
    # - https://www.babelnet.org clients?

    # basic spell-checking stuff
    aspell
    aspellDicts.en

    # help manage nix files
    nixfmt-rfc-style

    nix-output-monitor
  ];
}
