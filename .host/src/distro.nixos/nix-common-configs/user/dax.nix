{ config, pkgs, ... }:

{
  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.dax = {
    isNormalUser = true;
    description = "dax";
    extraGroups = [
      "networkmanager"
      "wheel"
      "scanner"
      "lp"
    ];
    packages = with pkgs; [
      #  thunderbird
    ];
  };
}
