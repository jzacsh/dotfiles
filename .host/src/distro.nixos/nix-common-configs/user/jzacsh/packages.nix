{ config, pkgs, ... }:

{
  packages = with pkgs; [
    firefox-wayland
    #
    # TUI-oriented things
    #
    vcprompt
    jq
    qprint
    beep
    colordiff
    diff-so-fancy
    wdiff
    fzf

    # TODO investigate using ssh-ident and updating dotfiles (eg
    # .bashrc and $DOTFOREST_BIN/dotforest/setup-ssh-agent.sh) accordingly.
    ssh-ident

    #
    # general desktop-usage preferences
    #
    graphviz
    inotify-tools
    grc
    syncthing
    strace
    rclone
    zathura
    rsnapshot
    xsel
    dconf-editor
    # fd=gitignore respecting grep-alternative written in rust: github.com/sharkdp/fd
    fd
    ripgrep
    ripgrep-all
    # does all the things I used to get from pdfjam on debian releases
    # (merging, splitting into component images, turning images into PDFs,
    # etc).
    pdfcpu
    # for notify-send commandline tool
    libnotify

    gossip

    # TODO jzacsh found this installer broken as of 2024-10-17 17:08:37-05:00
    #calibre

    # for xmllint CLI
    libxml2

    #
    # image/video things
    #
    libwebp
    yt-dlp
    exiftool
    feh
    # for rsvg-convert CLI
    librsvg
    # 2024-10-26 16:20:17-05:00 uncomment when perceptualdiff package no
    # longer relies on vulnerable "freeimage-unstable-2021-11-01" package
    #perceptualdiff

    #
    # Keyboard configuration apps
    #
    # https://uhk.io is the programmable keyboard
    uhk-agent
    # https://usevia.app - for nuphy60v2 keyboard
    via
    # qmk is a keyboard config app and qmk_hid and -udev-rules allow for
    # seamless `qmk setup` runs, which in turn allows for `qmk flash` command
    # to become available. eg: if you want to flash Nuphy's Air60v2's new .bin
    # files to your keyboard.
    qmk
    qmk_hid
    qmk-udev-rules

    #
    # PKMish things
    #
    # Consistently breaking my builds because electron is constantly beng
    # marked as EOL before this package has been updated:
    shellcheck
    logseq
    tutanota-desktop
    weechat
    sirikali
    gocryptfs
    protonmail-bridge
    protonmail-bridge-gui
    protonmail-desktop
    joplin-desktop

    #
    # Games
    #
    nethack
    prboom-plus
    minetest
    classicube # minecraft re-implementation
    vitetris # TUI of tetris
    zeroad
    zeroadPackages.zeroad-data
    zeroadPackages.zeroad-unwrapped

    #
    # programming things
    #

    python3
    sqlite
    diffstat
    ctags
    zeal # GUI for offline docs of _many_ SDKs (primarily care about JDKs).
    wiki-tui # wikipedia TUI
    #
    # Dev. toolchains/CI things (buildtools, linters, VCSs, etc).
    #
    git
    mercurial
    jujutsu
    clang
    lldb
    gnumake
    autoconf
    # gcc # if needed, then use a dev env. via a nix-shell.
    valgrind
    gdb
    podman

    #
    # Java things
    #
    jdk
    google-java-format
    gradle
    gradle-completion

    android-studio

    # TODO: fix broken android-studio-full once hashmismatch is resolved:
    # ```
    #    checking for references to /build/ in /nix/store/mkg0iwp48d3ihz0cw30lmyyqdwnxp1rp-android-sdk-platforms-33...
    #    error: hash mismatch in fixed-output derivation '/nix/store/zsfxdbsw1grq8hyvkhhw75yfbndxygac-platform-35_r02.zip.drv':
    #             specified: sha1-KgsGocibUqPXDOXUBVSwlMHB0Kw=
    #                got:    sha1-C7VgqQp6LL0N2DSCJNUYtjj+eUk=
    #    error: 1 dependencies of derivation '/nix/store/q6rl93q6nln9sbrlwhvl1nr728z7w6fz-android-sdk-platforms-35.drv' failed to build
    # ```
    #android-studio-full

    zig
    go
    # provides 'staticcheck' binary
    go-tools
    # 22.05 channel uses this...
    #go-langserver
    # ... while _beyond_ 22.05, we replace 'go-langserver' with 'gopls'
    gopls
    sassc
    android-tools
    deno
    # Rust SDK that I'd expect if I'd have used rustup:
    rustc
    rusty-man
    rustfmt
    # Language Server (LSP) for Rust
    #rls
    # beyond the 22.05 channel the 'rls' package is replaced by rust-analyzer:
    rust-analyzer
    cargo
    clippy
    cargo-watch
    llvmPackages.bintools
    rustc.llvmPackages.llvm
    cargo-llvm-cov
    cargo-tarpaulin

    nixd
    nixfmt-rfc-style

    # AWS CLI
    #awscli2 # TODO figure out what's going on with nixos packages for awscli
    #2024-03-23 awscli2 keeps breaking with "cannot unlink" errors like:
    # """
    #     copying path '/nix/store/4ndywwxb09cr0dan563jq0s1m933b34h-freetype-2.13.2-dev' from 'https://cache.nixos.org'...
    #     error: 1 dependencies of derivation '/nix/store/ljs3czcx80ypkf7lxs7wb7a05lc1xryq-etc.drv' failed to build
    #     copying path '/nix/store/4n1lm01waj5sd1y3k7m3qhllaiqcrfz2-libXcursor-1.2.2-dev' from 'https://cache.nixos.org'...
    #     copying path '/nix/store/s8pq32k9hrw2w0djkll3zl3214aqdrr7-cups-2.4.7-dev' from 'https://cache.nixos.org'...
    #     copying path '/nix/store/5qm9d2a7j2m3w4nj2dd4wl5sdgq77x1s-libXinerama-1.1.5-dev' from 'https://cache.nixos.org'...
    #     copying path '/nix/store/s5vd8gs8ixzyzi61wg2sb2137yg5f9i2-libayatana-indicator-0.9.4' from 'https://cache.nixos.org'...
    #     copying path '/nix/store/x4qm5fbzchx752fs6sn7h4csx66jr619-harfbuzz-8.3.0-dev' from 'https://cache.nixos.org'...
    #     copying path '/nix/store/p06vyjkxkwrghz0j80c12ic6kwfygnyz-libXfixes-6.0.1-dev' from 'https://cache.nixos.org'...
    #     error: 1 dependencies of derivation '/nix/store/4mk9arb8l3jkld19z4wi0fb173sbx6nv-nixos-system-oankali-24.05pre601756.44d0940ea560.drv' failed to build
    # """

    nodejs
    nodePackages.typescript
    nodePackages.typescript-language-server

    tree-sitter
    # TODO re-evaluate having vim plugins mangaed by nixos like this
    vimPlugins.vim-shellcheck
    vimPlugins.rust-vim
    vimPlugins.nvim-lspconfig

    lua
    luajitPackages.lua-lsp
    luajitPackages.luarocks
    stylua

    # random language servers
    # jzacsh@ commented out 2023-02-23 10:26:12-06:00 as it broke with a
    # warning to use the newer python-lsp-server, but I can't find a generic
    # packge by that name.
    #python-language-server
    nodePackages.bash-language-server
    nodePackages.vim-language-server

    # TODO find an alternative otherwise we hit
    # build-time errors from maven like "has not been downloaded from it
    # before" which appear to be a packaging-design problem and not an actual
    # problem with my system.
    #java-language-server

    #
    # markdown tooling
    #
    # static-side generator and local dev-webserver; used for my personal website's development
    hugo
    # markdown parser; used for my personal website's development
    asciidoctor-with-extensions
    markdownlint-cli
  ];
}
