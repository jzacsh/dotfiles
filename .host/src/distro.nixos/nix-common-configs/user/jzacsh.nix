{ config, pkgs, ... }@args:

{
  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.jzacsh = {
    isNormalUser = true;
    description = "jzacsh";
    extraGroups = [
      "networkmanager"
      "wheel"
      "scanner"
      "lp"
      # for android SDK, per https://wiki.nixos.org/w/index.php?title=Android
      "kvm"
      "adbusers"
    ];

    # allows processes I start (like tmux, local servers, etc) to stick around
    # so long as I've not powered off (eg: if Desktop Environment restarts: no
    # impact to my processes).
    linger = true;

    #
    # personal desktop things
    #
    packages = (import ./jzacsh/packages.nix args).packages;
  };

  #
  # OCI/podman things per https://nixos.wiki/wiki/Podman
  #
  # Enable common container config files in /etc/containers
  virtualisation.containers.enable = true;
  virtualisation = {
    podman = {
      enable = true;

      # Create a `docker` alias for podman, to use it as a drop-in replacement
      dockerCompat = true;

      # Required for containers under podman-compose to be able to talk to each other.
      defaultNetwork.settings.dns_enabled = true;
    };
  };

  # TODO: figure out how to set this
  # args.pkgs.config.android_sdk.accept_license = true;

  # Useful other development tools
  environment.systemPackages = with pkgs; [
    dive # look into docker image layers
    podman-tui # status of containers in the terminal
    podman-compose # start group of containers for dev
  ];
  virtualisation.oci-containers.backend = "podman";
}
