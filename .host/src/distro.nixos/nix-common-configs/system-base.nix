# Edit this configuration file to define what should be installed on
# all machines (eg: servers, desktops, VMs, etc).
#
# Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{
  nixpkgs,
  config,
  lib,
  pkgs,
  ...
}:

{
  # Enable networking
  networking.networkmanager.enable = true;

  # Set your time zone.
  time.timeZone = "America/Chicago";

  # Select internationalisation properties.
  i18n.defaultLocale = "en_US.utf8";

  # try to make tty{1..6} a usably-high resolution
  console = {
    earlySetup = true;
    packages = with pkgs; [ terminus_font ];
    keyMap = "us";
  };
  console.font = lib.mkDefault "${pkgs.terminus_font}/share/consolefonts/ter-d18n.psf.gz";

  # Keep manufacturers' firmware updated
  services.fwupd.enable = true;

  # Configure keymap in X11
  services.xserver = {
    xkb.layout = "us";
    xkb.variant = "";
  };

  security.rtkit.enable = true;
  security.sudo.extraConfig = ''
    # jzacsh@ inserted per
    # https://gitlab.com/jzacsh/dotfiles/-/blob/13e3586f134a298/.host/common/umask
    # so that admins' own personal umasks can be restrictive without messing
    # up `sudo` administrative tasks.
    #
    # For more: https://wiki.archlinux.org/title/Sudo#Permissive_umask
    Defaults umask = 0022
    Defaults umask_override
  '';

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  # programs.mtr.enable = true;
  # programs.gnupg.agent = {
  #
  #   # jzacsh@ determined we _don't_ want this line enabled because it's an
  #   #   override of an already functioning default behavior, per this
  #   #   comment:
  #   #   https://github.com/NixOS/nixpkgs/blob/nixos-22.05/nixos/modules/programs/gnupg.nix#L96
  #   enable = true;
  #   enableSSHSupport = true;
  # };

  # Get helpful tips and messages about missing programs.
  programs.command-not-found.enable = true;

  # List services that you want to enable:

  # Provides the `locate(1)` file-searching command.
  services.locate.enable = true;

  # Every week, automatically delete configs that are older than 30 days
  # per https://nixos.wiki/wiki/Storage_optimization#Automation
  nix.gc = {
    automatic = true;
    dates = "weekly";
    options = "--delete-older-than 30d";
  };

  # nixos-rebuild switch --upgrade will run periodically for us
  system.autoUpgrade.enable = true;

  # Runs hardlink optimization more often, so I don't have to do really
  # long-running calls on my own (eg: after a couple weeks with nixios I had
  # 9GB optimized and it took an hour to run).
  nix.settings.auto-optimise-store = true;

  # Overlays are generally explained here: https://nixos.wiki/wiki/Overlays#In_NixOS
  # - breakpad's overlay is from https://github.com/NixOS/nixpkgs/issues/368994#issuecomment-2565049412
  # For a working example see dotfiles commit aa4de06976a333025387dffc325cf971a8464cb7
}
