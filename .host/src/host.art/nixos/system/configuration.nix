# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:

{
  imports = [
    # Include the results of the hardware scan.
    ./nix-common-configs/system-base.nix

    # NOTE: this assumes we've followed nixos-hardware instructions[1]
    # and added the nixos-hardware repo's tarball as a nix-channel.
    #
    # [1]: https://github.com/NixOS/nixos-hardware/tree/master/#using-channels
    <nixos-hardware/framework/13-inch/intel-core-ultra-series1>

    ./hardware-configuration.nix
    ./nix-common-configs/system-packages.nix

    # Define a user account. Don't forget to set a password with ‘passwd’.
    ./nix-common-configs/user/jzacsh.nix

    ./nix-common-configs/system-personal-computer.nix
  ];

  # Bootloader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  networking.hostName = "art"; # Asshole Research Transport
  # networking.wireless.enable = true;  # Enables wireless support via wpa_supplicant.

  # Configure network proxy if necessary
  # networking.proxy.default = "http://user:password@proxy:port/";
  # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";

  i18n.extraLocaleSettings = {
    LC_ADDRESS = "en_US.UTF-8";
    LC_IDENTIFICATION = "en_US.UTF-8";
    LC_MEASUREMENT = "en_US.UTF-8";
    LC_MONETARY = "en_US.UTF-8";
    LC_NAME = "en_US.UTF-8";
    LC_NUMERIC = "en_US.UTF-8";
    LC_PAPER = "en_US.UTF-8";
    LC_TELEPHONE = "en_US.UTF-8";
    LC_TIME = "en_US.UTF-8";
  };

  # Allow unfree packages
  nixpkgs.config.allowUnfree = true;

  # List services that you want to enable:

  # Enable the OpenSSH daemon.
  # services.openssh.enable = true;

  # Open ports in the firewall.
  # networking.firewall.allowedTCPPorts = [ ... ];
  # networking.firewall.allowedUDPPorts = [ ... ];
  # Or disable the firewall altogether.
  # networking.firewall.enable = false;

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "24.11"; # Did you read the comment?

  # Specifically have 'nixos-unstable" rather than nix "nixos-22.05" so that
  # packages are more rolling-release style, per:
  # - 1) https://nixos.wiki/wiki/Nix_channels
  # - 2) https://nixos.org/manual/nix/stable/package-management/channels.html
  #
  # NOTE: this line below isn't enough - also need to manually do:
  #   sudo nix-channel --add https://nixos.org/channels/nixos-unstable nixos
  #   sudo nix-channel --list # output should confirm unstable is present!
  system.autoUpgrade.channel = "https://nixos.org/channels/nixos-unstable";
}
