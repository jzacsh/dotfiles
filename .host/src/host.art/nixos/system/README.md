in-progress migration from old machine setup; this config needs to be updated
to match host.oankali's corresponding files.

- [x] mechanical/dumb migration of files from .../host.oankali/ dir
  - simply copy/pasted the whole dir
  - simply find/replace `oankali` with new hostname `art` (except a few places).
- [x] migration of nixos calamari's configs
- [x] hand-merge of calamari config with oankali nixos configs
