#!/usr/bin/env bash

function hack_checkBrokenTouchDisplay() {
  local broken_dev_id='0018:04F3:2BB5.0002'
  local hid_mtouch_dir='/sys/bus/hid/drivers/hid-multitouch'
  
  [[ -e "$hid_mtouch_dir"/"$broken_dev_id" ]] || return 0 # our job's done
  if [[ "$1" = -n ]]; then return 1; fi

  dotforest_log error \
    'Broken touchscreen device "%s" is ENABLED!!!\n\t\n' \
    "$broken_dev_id"

  term_confirm_optout "Disable '$broken_dev_id' now?" || {
    dotforest_log warn \
      'on your own with touch inputs! run `%s` to reconsider!\n' \
      "${FUNCNAME[0]}"
    return 1
  }

  (
    set -x
    echo "$broken_dev_id" | sudo tee "$hid_mtouch_dir"/unbind
  )
}

if hack_checkBrokenTouchDisplay -n; then
  dotforest_log info \
    'GOOD: broken touch-display appears disabled (`hack_checkBrokenTouchDisplay`)\n'
else
  dotforest_log warn \
    'SUSPICIOUS: broken touch-display appears bound; `hack_checkBrokenTouchDisplay` to unbind\n'
fi
