# Thinkpad X1 Nano, Gen 1
#
# Do not modify this file!  It was generated by ‘nixos-generate-config’
# and may be overwritten by future invocations.  Please make changes
# to /etc/nixos/configuration.nix instead.
{
  config,
  lib,
  pkgs,
  modulesPath,
  ...
}:

{
  imports = [
    (modulesPath + "/installer/scan/not-detected.nix")
  ];

  boot.initrd.availableKernelModules = [
    "xhci_pci"
    "thunderbolt"
    "nvme"
    "usb_storage"
    "sd_mod"
  ];
  boot.initrd.kernelModules = [ ];
  boot.kernelModules = [ ];
  boot.extraModulePackages = [ ];

  fileSystems."/" = {
    device = "/dev/disk/by-uuid/a7a11f8e-2e94-4f50-bad2-547b5fa70258";
    fsType = "ext4";
  };

  boot.initrd.luks.devices."luks-21d4f42f-9c13-4a93-827f-1020cc411220".device =
    "/dev/disk/by-uuid/21d4f42f-9c13-4a93-827f-1020cc411220";

  fileSystems."/boot/efi" = {
    device = "/dev/disk/by-uuid/F288-4C10";
    fsType = "vfat";
  };

  swapDevices = [
    { device = "/dev/disk/by-uuid/0ac55bfa-e0c8-415e-8913-14b600c3c9dd"; }
  ];

  # Enables DHCP on each ethernet and wireless interface. In case of scripted networking
  # (the default) this is the recommended approach. When using systemd-networkd it's
  # still possible to use this option, but it's recommended to use it in conjunction
  # with explicit per-interface declarations with `networking.interfaces.<interface>.useDHCP`.
  networking.useDHCP = lib.mkDefault true;
  # networking.interfaces.wlp0s20f3.useDHCP = lib.mkDefault true;

  powerManagement.cpuFreqGovernor = lib.mkDefault "powersave";
  hardware.cpu.intel.updateMicrocode = lib.mkDefault config.hardware.enableRedistributableFirmware;

  # Thinkpad's touchscreen display is cracked and fires random events input, so
  # we disable it with these rules.
  #
  # ## Manual: toggle input by binding/unbinding via "hid-multitouch" driver:
  #
  # ```sh
  # $ echo 0018:04F3:2BB5.0002 | sudo tee /sys/bus/hid/drivers/hid-multitouch/unbind
  # ```
  #
  # ## Automatic: the udev rules below automatically hide its inputs instead
  #
  # Hide the device's inputs by disabling this tree of touch devices from being
  # accepted by libinput:
  #     /devices/pci0000:00/0000:00:15.1/i2c_designware.1/i2c-2/i2c-ELAN901C:00/0018:04F3:2BB5.0002
  #
  # See also:
  # - https://wayland.freedesktop.org/libinput/doc/latest/ignoring-devices.html#ignoring-devices
  # - udevadm monitor # useful to run when manually enabling a device
  # - udevadm info --path /devices/pci0000:00/0000:00:15.1/i2c_designware.1/i2c-2/i2c-ELAN901C:00/0018:04F3:2BB5.0002
  # - udevadm info --tree | less +/2BB5.0002
  # - sudo libinput list-devices | less +/2BB5
  # - udevadm test --action add /devices/pci0000:00/0000:00:15.1/i2c_designware.1/i2c-2/i2c-ELAN901C:00/0018:04F3:2BB5.0002
  services.udev.extraRules = ''
    ACTION!="unbind", ENV{ID_PATH}=="pci-0000:00:15.1-platform-i2c_designware.1", ENV{ID_INPUT_TOUCHSCREEN}=="1", ENV{LIBINPUT_IGNORE_DEVICE}="1"
  '';

  #2023-03-26 jzacsh@ commented out the below two lines due to:
  # ```
  # $ sudo nixos-rebuild switch --show-trace
  #   # ...snipped output...
  #          error:
  #   Failed assertions:
  #          - The option definition `hardware.video.hidpi.enable' in `/etc/nixos/hardware-configuration.nix' no longer has any effect; please remove it.
  #                 Consider manually configuring fonts.fontconfig according to personal preference.

  # ```
  # # high-resolution display
  # hardware.video.hidpi.enable = lib.mkDefault true;
}
