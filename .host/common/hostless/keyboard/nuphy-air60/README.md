`via-conf_nuphy60v2.json` was originally downloaded (and commited as-is in
commit `7475da650c3af5`) directly from
https://nuphy.com/pages/json-files-for-nuphy-keyboards (it was
`https://cdn.shopify.com/s/files/1/0268/7297/1373/files/nuphy-air60-v2-via.json.zip?v=1701237799`)
following their guide at https://nuphy.com/pages/via-usage-guide-for-nuphy-keyboards.
