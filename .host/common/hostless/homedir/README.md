# scripts and docs for `$HOME` setup

TODO: move much of https://gitlab.com/jzacsh/desktop-setup here into this
directory.

1.  [ ] move directory construction here
    -   [ ] *then* ensure usr-local-README.md always lands (via symlink) over
        into ~/usr/local/README.md
2.  [ ] provide seldom-run hostforest-bootstrap *check* (dryrun-mode) variant of
    dotfiles symlink fanout. ie: catch if new symlinks land in this dotfiles
    repo that aren't (on *the current host*) installed just yet.
3.  [ ] move the distro package installations here
4.  [ ] make desktop-setup scripts depend on *this* repo's (bullets above) new
    logic, instead of holding the logic itself.
