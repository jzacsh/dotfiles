this is mostly a structure meant for non-nixos distros to install user-specific
codebases.

**exception**: ./source/ is specific tgo my hostforest (it's meant for things
that should be sourced from my shell.

My hostforest already exports `PATH=` to some directories here.

## usage

### example with neovim

```sh
$ git clone https://github.com/neovim/neovim ~/pub/src/neovim
$ git checkout stable
$ time make CMAKE_BUILD_TYPE=Release CMAKE_EXTRA_FLAGS="-DCMAKE_INSTALL_PREFIX=$HOME/usr/local"
$ make install
```

now `type nvim` should show a path in *this* directory.

It *should* be possible to uninstall by just removing all `./*/nvim` glob
results. to be *extra* safe/easy you could also just set install prefixes under
a subpath of `./.custom/` directory. eg:

```sh
# ...snipped...
$ time make CMAKE_BUILD_TYPE=Release CMAKE_EXTRA_FLAGS="-DCMAKE_INSTALL_PREFIX=$HOME/usr/local/.custom/nvim"
# ...snipped...
```
