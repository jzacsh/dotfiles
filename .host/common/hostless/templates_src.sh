#!/usr/bin/env bash
#
# Provides helpers to easily print boilerplate for use elsehwere.

# Shellcheck disabled because if these fail, something's super wrong (eg: we're
# on a mac without a gnu/linux readlink).
# shellcheck disable=SC2155
_hostless_tmpls_srcDir="$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")/templates"

function _hostless_tmpls_listTmpls() {
  find "$_hostless_tmpls_srcDir" \
    -mindepth 1 \
    -maxdepth 1 \
    -type f \
    -printf '%P\n' \
    2>/dev/null
}
readarray -t _hostless_tmpls < <(_hostless_tmpls_listTmpls)
(( "${#_hostless_tmpls[@]}" > 0 )) || {
  dotforest_log err \
    'BUG: templates sourcer still exists but no templates found (expected at %s)\n' \
    "$_hostless_tmpls_srcDir"
  return 1
}


# Function intended to tell bash how to autocomplete `templ` func.
#
# For more see bash(1) man page entries:
# - "complete" builtin
# - "compgen" builtin
# - "Programmable Completion"
function _hostless_tmpls_templ_bashCompletion() {
  (( COMP_CWORD == 1 )) || return 0 # only one arg is taken by `templ`

  local tmpls; readarray -t tmpls < <(_hostless_tmpls_listTmpls)

  readarray -t COMPREPLY < <(compgen -W "${tmpls[*]}" -- "${COMP_WORDS[$COMP_CWORD]}")
}
complete -F _hostless_tmpls_templ_bashCompletion templ

# Boilerplate printer based on private collection of templates
# at $HOSTFOREST_ROOT/common/hostless/templates/ dir.
#
# $1=basename of a file in said templates dir.
function templ() {
  [[ "$#" -ne 0 && "$1" != '-h' ]] || {
    # shellcheck disable=SC2016
    printf -- \
    'Boilerplate printer, based on private template collection.

    usage: TEMPLATE_NAME

    TEMPLATE_NAME
      These are basenames of $HOSTFOREST_ROOT/common/hostless/templates/ files.
      Tab completion also works for TEMPLATE_NAME values.

      Current possible values:
          %s
' \
    "$(find "$_hostless_tmpls_srcDir" -mindepth 1 -maxdepth 1 -type f -printf '%f, ')" \
    >&2
    return 1
  }
  local tmpl_basename="$1"
  local tmpl="$_hostless_tmpls_srcDir"/"$tmpl_basename"
  [[ -e "$tmpl" && -r "$tmpl" ]] || {
    printf -- \
      'ERROR: no such readable template "%s" exists\n' \
      "$tmpl_basename" >&2
    return 1
  }
  cat "$tmpl"
}
