# Non-Host Configs

This directory kind of violates the basic premise of all my dotfiles.
My dotfiles (and my hostforest) are management files for the _linux environments
I have login creds_ for. Hence "host"forest: a way for me to detect which host
I'm logged into via distro or hostname, etc.

However all dotfiles below this directory are very much  configuration for
something _else_ that I manage when _just so happen_ to be logged into a host,
therefore it's handy to have their configurations at hand in this same
repository. That means this directory should be just as useful from say an OSX
host or BSD hosts (both of which I'd expect the entire rest of this repo to be
broken for).

- `*_src.sh` will automatically be sourced on EVERY host, per ../allhosts
  eg: see `./templates_src.sh`
- `keyboard/` holds fancy keyboards' configurations:
   - `keyboard/uhk/` holds my uhk v1 and v2 configs, managed by
     https://uhk.io/agent desktop app. v1 is from the orginal crowdsupply
     crowd-funding campaign, and v2 is from subsequent purchases.
   - `keyboard/nuphy-air60/` holds my [Nuphy Air60
     V2](https://nuphy.com/products/air60-v2)'s configs, managed by VIA
     (https://usevia.app) desktop app.
- `templates/` holds boilerplate I maintain to avoid typing it over and over.
  Anything in that dir can be printed to stdout (our into a new script) via
  `templ NAME` (eg: `templ bash-script`)
