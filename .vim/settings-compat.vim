"
" General settings & configuration, forward-compatible with neovim

""""""""""""""""""""""""""""""""""
" enable autocommands: command executed automatically in response to some
" event, such as a file being read or written or a buffer change.
autocmd!

set cmdheight=2
" config for simple (LSP-less) completions
set wildmode=longest,list
set foldlevel=4

" Minimal number of screen lines to keep above and below the cursor.
"set scrolloff=10
"Too annoying... just not used to this

set nolist eol

" set which wraps
set whichwrap+=<,>,h,l

" for filetype event-based configs
filetype plugin on
filetype indent on

" read in live edits on opened files
set autoread

" show results while typing
set incsearch
" pfftt.. who needs the real vi anyway?
set nocompatible

" textwidth:
set tw=80

" always showtab:
set showtabline=2

" spaces for tabs: `expandtab'
set et

" case-insensitive search (/)
"   unless \C appears anywhere in the query
set ignorecase
set smartcase " allow case-sensitive by simply including mixed case in queries

" default spaces in tabs/indents:
set ts=2
set sw=2
set tabstop=2

" syntax hilighting
syntax on
set ff=unix
set autoindent
set history=400
set ruler
set number
set laststatus=2
set statusline=%t%(\ [buf:\ %n%M]%)%(\ %H%R%W%)\ %L\ lines\ \|\ scroll:\ %P\ \|\ [cursor]\ %(col:\ %c,\ byte:\ #%o,\ val:\ %b=0x%B%)
set hidden

" enable spellchecking; `:setlocal spell` to trigger
set spelllang=en_us
hi clear SpellBad
hi SpellBad cterm=underline
"set spell

" change the direction of new splits to be what I'd naturally expect
"   from tip at https://vimtricks.com/p/open-splits-more-naturally
set splitbelow
set splitright

" Milliseconds to wait before saving state for :crash-recover
" default is 4000
set updatetime=250

" allow a vertical gutter (a la VCS-state symbols) beside line-numbers
"    yes means "always"
set signcolumn=yes

" too annoying:
""""""""""""""
" enables mouse for any of mode:
"   [n]ormal,[v]isual,[i]nsert,[c]ommandline,[h]elp-file,[a]ll
"set mouse=i

" for some reason nvim *still* scrolls w/trackpad
"   last resort, per http://unix.stackexchange.com/a/44516
autocmd BufEnter * set mouse=
" Also disable mouse scrollwheels
set mousescroll=ver:0,hor:0
" Also disable scrollwheel indirectly (terminal emulators that rebroadcast
" scrolls as arrow-up/down events)
noremap <up>, <nop>
inoremap <up>, <nop>
noremap <down>, <nop>
inoremap <down>, <nop>


" must be after colorscheme
set cursorline
hi  cursorline   cterm=underline ctermbg=none
hi  cursorlinenr term=bold ctermfg=white

" summarized-live-preview of mid-progress comamnds
"   eg: %s/a/b/ shows b live, before hitting enter in the buffer, but this
"   summarizes all the effected lines in a new quickfix-looking pane
set inccommand = "split"

if v:version >= 703
  " creates *constant* long-line marker with columns color
  " set colorcolumn=+1,+2,+3

  augroup color_tweak
    autocmd!
    autocmd ColorScheme * highlight clear ColorColumn
    autocmd ColorScheme * highlight ColorColumn guifg=red ctermfg=red gui=bold
  augroup END

  if v:version >= 704
    " When we're in X11
    if &term !=# "linux"
      " set background=dark
      set list listchars=tab:\»\ ,extends:›,precedes:‹
    endif
  endif
endif

" always jump to last position in file, see :help last-position-jump
autocmd BufReadPost * if line("'\"") > 0 && line("'\"") <= line("$") | exe "normal g'\"" | endif

"highlight redundant whitespace.
highlight RedundantSpaces ctermbg=red guibg=red
match RedundantSpaces /\s\+$\| \+\ze\t\|\t/

" highlight variable on hover stackoverflow.com/questions/1551231
" NOTE: inorder to work, this *must* come after RainbowParentheses plugin conf
autocmd CursorMoved * exe printf('match Visual /\V\<%s\>/', escape(expand('<cword>'), '/\'))

" custom colorscheme logic for the Visual group's matches
highlight Visual cterm=standout

set modeline
