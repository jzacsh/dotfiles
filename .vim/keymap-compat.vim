"
" General keymap-related stuff, forward-compatible with neovim

" "Leader" is a concept for hotkey mapping, used to sort of namespace all
" customizations. All user-customizable hotkeys in vim prefixed by one
" dedicated triggering key that tell vim "I'm about to type one of my custom
" hostkeys". Whatever it's set to (eg: the "\" key) will mean that a custom
" shortcut (like 'f' for triggering a search prompt UI) will require it to be
" typed first (in the running example: "\" and then "f" will open your find
" UI). This is possible because nvim ecosystem expects hotkeys to be set by
" specifying the generic <Leader> before a hotkey (eg: "<Leader>f
" :some_search_ui()")
"
let mapleader = ","
let maplocalleader = "," " buffer-scoped version of mapleader


" no need to 'q:![shell stuff]' anymore, just ![shell stuff]
nnoremap ! :!

" no need to ':' anymore
nnoremap ; :

"
" Jon's custom vimscript functions
""""""""""""""""""""""""""""""""""


" Append some annotation reminding you a particular line or set of lines are
" for debugging only and should not be committed to your VCS.
function! AppendDoNotSubmit()
  " Add mark
  normal! mx

  " Append comment to current line
  normal! A  DO NOT SUBMIT
  normal! bhbhbh
  call NERDComment('n', 'toEOL')

  "Go back to mark
  normal! 'x
endfunction


"
" Keymappings for jon's plain-old-vimisms
"""""""""""""""""""""""""""""""""""""""""

" toggle :set (no)paste
nnoremap <Leader>P  <Cmd>se invpaste paste?<return>

" [t]imestamp insertion
nnoremap <Leader>t  <Cmd>r!date --rfc-3339=seconds<Enter>

" Begins a [S]ubstitution for the highlighted word with & to put it back, if I
" want (otherwise just a single backspace is needed). Adapted from:
" https://github.com/ThePrimeagen/init.lua/blob/97c039bb88d8/lua/theprimeagen/remap.lua#L42C34-L42C88
nnoremap <Leader>S  :%s/\<<C-r><C-w>\>/&/gI<Left><Left><Left>

" clear trailing whitespace
nnoremap <Leader>c  <Cmd>%s/[[:space:]]*$//g<Enter>

" quickly append [T]ODO-notes to catch printf/debugging code before it gets submitted
nnoremap <Leader>T  <Cmd>call AppendDoNotSubmit()<Enter>

" buffer next
nnoremap <Leader>p  <Cmd>bnext<Enter>
" buffer previous
nnoremap <Leader>n  <Cmd>bprevious<Enter>

" buffer previous
nnoremap <Leader>r  <Cmd>registers<Enter>

" [H]istory: Undo-tree; relies on plugin github.com/mbbill/undotree (installed
" both in vim-classic vendoring and in Lazy config in newer nveovim world).
nnoremap <Leader>h  <Cmd>UndotreeToggle<Enter>
