" Single set of vim/neovim default configs that is sourced by both neovim (via
" lua configs) AND by ~/.vimrc via classic vimscript startup.
"
" WARNING: when changing this file, ALWAYS test by temporarily setting
" verbose=9 (either via 'vim -V9' or 'set verbose=9') to make sure nothing is
" newly broken by your change!

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

source $HOME/.vim/keymap-compat.vim " order matters!
source $HOME/.vim/settings-compat.vim

if !empty($HOSTFOREST_NVIM_HOSTADD_COMPAT_VIM)
  source $HOSTFOREST_NVIM_HOSTADD_COMPAT_VIM
endif
