#!/usr/bin/env bash

# shellcheck disable=SC1091
source yabashlib || {
  printf 'error: failed to find yabashlib\n' >&2
  exit 1
}
this_script="$(readlink -f "${BASH_SOURCE[0]}")" ||
  logfFatal 'failed basic BASH_SOURCE introspection\n'
declare -r this_script
# shellcheck disable=SC2016
[[ -e "$this_script" ]] ||
  logfFatal 'bug? $this_script unresolved: "%s"\n' "$this_script"

script_name="$(basename "$this_script")" ||
  logfFatal 'basename fail: %s\n' "$this_script"
declare -r script_name
setLogPrefixTo "$script_name"

vendor_dir="$(dirname "$this_script")" ||
  logfFatal 'dirname fail: %s\n' "$this_script"
declare -r vendor_dir

function usage() {
  cat <<EOF_HELP
usage init|download|update|remove|refresh [...]

  Setup a new repo one-time with the "init" action followed by an audit and
  commit of the gitmap change, and then a "download" action, followed.

  Other than one-time setup: use the "update" action to pull the latest code to
  the subtree, and audit the single commit that landed in your repo.

  As a convenience the zero-argument "refresh" action will cause an "update" to
  run automatically in a loop against all previously recorded repositories
  (recording happens via the "init" action documented below).

  - "init" action:
    $script_name init LOCAL_DIR GIT_REMOTE_URL GIT_REFBRANCH

    Calling init will create a record in the locally tracked, home-spun gitmap
    so you can always update a git subtree in the future, even if the subtree
    commit doesn't specify the origin URL of that code.

    Once you're happy with the changes made to the gitmap (git diff) commit then
    and then run the download action against the same exact LOCAL_DIR value.

  - "download" action:
    $script_name download LOCAL_DIR

    Reads the corresponding entry from gitmap - added by "init" action - and
    adds a git subtree.

    This is a thin wrapper for 'git subtree add --squash ...'

  - "update" action:
    $script_name update LOCAL_DIR [GIT_REFBRANCH}

    Reads the corresponding entry from gitmap - added by "init" action (possibly
    a long time ago) - and pulls down the latset code.

    This is a thin wrapper for 'git subtree pull --squash ...'

    Optioanlly pass GIT_REFBRANCH as a one-time override of whatever branch (eg
    "stable") you might have previously recorded to the gitmap via the "init"
    action.

  - "remove" action:
    $script_name remove LOCAL_DIR

    Removes the corresponding entry from gitmap - added by "init" action
    (possibly a long time ago) - and deletes the local files tracked by git.
    Follow this command up with your own commit of the results, if you're happy
    with what you see.

  - "refresh" action:
    $script_name refresh

    Walk through every entry in the gitmap - index file updated by the "init"
    action - and run the "update" action against that entry.

    If any updates fail this command exits immediately to avoid compounding
    issues due to the unexpected and unknown failure state your repo might have
    fallen into. Once you've fixed the root cause of the failure, you can just
    re-run this command again and and the updates on already successfully
    updated  repos will have no effect (this is a property of "git subtree"
    command we're really wrapping here).

  Variables mentioned above are all documented in the README.md - beside this
  script - in the section describing the "gitmap" file. Here's a quick recap:

  - LOCAL_DIR: the path, relative to the vendor dir where _this_ script lives.

    Papercut-Note: gitmap values are currently a subset of a valid LOCAL_DIR,
    relative to src/ dir. This means you have to prefix with "src/" when pasting
    a value out of the gitmap file and passing it back to this script as an
    argument. This is hopefully a non-issue if you're just using the "refresh"
    action anyway.

  - GIT_REMOTE_URL: the git path or URL from which you're vendoring source code.

  - GIT_REFBRANCH: the git ref or branch you want to pull from when
    cloning/pulling out of GIT_REMOTE_URL.
EOF_HELP
}

function bad_usage() { usage >&2; exit 1; }
declare -r usage_regexp='^(init|download|update|remove|refresh)$'

if (( $# == 0 )); then
  bad_usage
elif is_help_like "$1"; then
  usage
  exit
elif [[ ! "${1,,}" =~ $usage_regexp ]];then
  bad_usage
fi
declare -r subtree_action="$1"
shift

declare -r src_dir="$vendor_dir"/src
[[ -d "$src_dir" && -r "$src_dir" ]] || {
  if [[ "$subtree_action" = init ||
    "$subtree_action" = remove ||
    "$subtree_action" = download ]]; then
    logfWarning \
      'expected vendoring src dir at "%s", assuming this is a first run(action="%s") and carrying on\n' \
      "$src_dir" "$subtree_action"
  else
    logfFatal 'expected vendored sources at: %s\n' "$src_dir"
  fi
}

declare -r git_map="$vendor_dir"/gitmap
[[ -s "$git_map" && -r "$git_map" ]] || {
  if [[ "$subtree_action" = init || "$subtree_action" = remove ]]; then
    logfWarning \
      'expected vendoring map at: "%s", assuming this is a first run(action="%s") and carrying on\n' \
      "$git_map" "$subtree_action"
  else
    logfFatal 'expected vendoring map at: %s\n' "$git_map"
  fi
}

outer_repo_root="$(cd "$vendor_dir" && git rev-parse --show-toplevel)" ||
  logfFatal \
  'Failed determining outer-repo root via git rev-parse on: %s\n' \
  "$vendor_dir"
declare -r outer_repo_root

function have_localdir_record() {
  [[ -s "$git_map" ]] && [[ -n "$(get_record_linenum "$1")"  ]]
}

function list_vendored_src_dirs() {
  awk -F '\t' '{ print $1 }' "$git_map"
}

# $1=LOCAL_DIR
function get_record_linenum() {
  awk -F '\t' '{ if ($1 == "'"$1"'" ) { printf FNR }}' "$git_map"
}

function get_record_remote() {
  awk -F '\t' '{ if ($1 == "'"$1"'" ) { printf $2 }}' "$git_map"
}

function get_record_refbranch() {
  awk -F '\t' '{ if ($1 == "'"$1"'" ) { printf $3 }}' "$git_map"
}

# Asserts (or exits) based on dirpath "$1" intended as LOCAL_DIR, and then
# returns a consistently cleaned version of "$1", regardless of the various
# forms "$1" might come in.
#
# Assertion: this function asserts that LOCAL_DIR describes a sub directory of
# "$src_dir"; valid ways that can happen:
# - 1. "$1" exists --> analyze where it is, according to above^
# - 2. "$1" is theoretical --> consider if it's relative to outer-repo [root]
#      eg: "vendor/src/foo"
# - 3. "$1" is theoretical --> consider if it's relative to outer-repo vendor/
#      eg: "src/foo"
#
# Return value: LOCAL_DIR will be echoed back as a relative path to vendor/src;
# for example:
#   "$1=~/back/dots/vendor/src/some/path/ --> echo "some/path"
function check_localdir() {
  local dir_path="$1"

  # case 1 of 3: dir might exist
  if [[ -d "$dir_path" ]]; then
    {
      isDirectoryParentOfFile "$vendor_dir" "$dir_path"
    } || logfFatal \
      'LOCAL_DIR must be a descendent of:\n\t%s\n...but got:\n\t%s\n' \
      "$src_dir" "$dir_path"

    # Clean "$1" and echo relative to vendor/src; eg:
    #   "$1=~/back/dots/vendor/src/some/path/ --> echo "some/path"
    local target_dir; target_dir="$(readlink -f "$dir_path")"
    strTrimLeft "$target_dir" "$outer_repo_root/vendor/src/"
    return
  fi

  # Cases below this line are for a $dir_path that's theoretical...

  if strStartsWith  "$dir_path" /; then
    logfFatal 'LOCAL_DIR must be a relative path (see -h); got: "%s"\n' "$dir_path"
  fi

  # case 2 of 3: relative to outer-repo [root]
  if strStartsWith "$dir_path" vendor/src/; then
    strTrimLeft "$dir_path" vendor/src/
  else
    # case 3 of 3: relative to outer-repo vendor/
    strStartsWith "$dir_path" src/ ||
      logfFatal 'LOCAL_DIR must be a relative path (see -h); got: "%s"\n' "$dir_path"

    strTrimLeft "$dir_path" src/
  fi
}

vcs_git_is_clean || logfFatal 'git repo must be clean\n'

# Most of the remainder of this script doesn't apply to the refresh case, so
# handle it now and always exit now.
if [[ "$subtree_action" = refresh ]];then
  cd "$vendor_dir" ||
    logfFatal 'bug? failed cd to preestablished vendor_dir=%s\n' "$vendor_dir"
  [[ -e "$this_script" && -x "$this_script" ]] ||
    logfFatal 'no executable "this" ref: %s\n' "$this_script"

  task_count="$(list_vendored_src_dirs | wc -l)" ||
    logfFatal 'failed listing vendored src subdirs; gitmap file broken?\n'

  [[ "$task_count" -gt 0 ]] ||
    logfFatal 'vendored src subdirs is empty; gitmap file broken?\n'

  srcd_linenum=1
  # shellcheck disable=SC2016
  logfInfo \
    'REFRESH action: manually calling `%s` now across all %s vendored src dirs...\n' \
    "$script_name update" \
    "$task_count"
  time {
    while read -r entry_dir; do
      remote_dir="src/$entry_dir"

      # shellcheck disable=SC2016
      logfInfo \
        'REFRESH sub-step %s of %s: `%s`...\n' \
        "$srcd_linenum" "$task_count" \
        "$script_name update $remote_dir"

      "$this_script" update "$remote_dir" || logfFatal \
        'updating for entry %s of %s "%s" failed; see error output above

        \r  TIP: once fixed you can safely just run the same command again\n' \
        "$srcd_linenum" "$task_count" \
        "$entry_dir"

      srcd_linenum=$(( srcd_linenum + 1 ))
    done < <(list_vendored_src_dirs)
  }
  exit 0
fi

# Asserts arg (label "$1", value "$2") is non-empty.
function assert_decent_arg() {
  local label="$1" value="$2"
  if ! strIsEmptyish "$value"; then return 0; fi
  logfFatal '%s cannot be empty; got "%s"\n' "$label" "$value"
}

# LOCAL_DIR, but relative to ./src/ dir
local_dir="$(assert_decent_arg 'LOCAL_DIR' "$1" && check_localdir "$1")" ||
  logfFatal 'usage: LOCAL_DIR invalid; got: "%s"\n' "$1"
declare -r local_dir

function localdir_absolute() { echo "$src_dir"/"$local_dir"; }
function is_localdir_present() { [[ -d "$(localdir_absolute)" ]]; }
function assert_not_yet_made() {
  is_localdir_present || return 0
  local error_fmt='LOCAL_DIR already exists (want "remove" action first?); its full path:\n\t%s\n'
  logfFatal "$error_fmt" "$(localdir_absolute)"
}
function assert_already_made() {
  local error_fmt='LOCAL_DIR "%s" not found. Hint: did you add by hand? (maybe re-create via "remove" and "init" actions first?); full path:\n\t%s\n'
  is_localdir_present || logfFatal \
    "$error_fmt" \
    "$local_dir" \
    "$(localdir_absolute)"

}

# WARNING: purposely leaks internal variables to global scope for utilization by
# git_subtree_cli function.
function run_git_subtree() {
  have_localdir_record "$local_dir" || logfFatal \
    'no record found for "%s" (want "init" action first?)\n' \
    "$local_dir"

  git_remote="$(get_record_remote "$local_dir")"
  assert_decent_arg 'GIT_REMOTE_URL[from record]' "$git_remote"

  git_ref="$(get_record_refbranch "$local_dir")"
  assert_decent_arg 'GIT_REFBRANCH[from record]' "$git_ref"

  prefix_relative_repo="$(strTrimLeft "$(strTrimLeft "$(localdir_absolute)" "$outer_repo_root")" /)"

  if [[ "$subtree_action" = download ]];then
    subtree_subcmd=add
  else
    subtree_subcmd=pull
  fi

  local this_repo_ref; this_repo_ref="$(git rev-parse HEAD)" ||
    logfFatal 'git does not know its HEAD (PWD=%s)\n' "$PWD"

  local brief_message="
This is an auto-generated merge commit of a squashed upstream source being
vendored into this git repo. Both this and said squashed commit are generated
via 'git subtree $subtree_subcmd ...' commandline, the full execution of
which is inlined below. Note that this _message_ and the _call_ to said
'git subtree' CLI below are generated via the ['$script_name'][thisrepo] script
in this repo (as of this repo's ref '$this_repo_ref'):
"

  local this_repo_pull_url
  # shellcheck disable=SC2016
  this_repo_pull_url="$(git remote get-url origin)" ||
    logfFatal 'internal error: `git remote` failed\n'

  local git_subtree_message
  # shellcheck disable=SC2016
  printf -v git_subtree_message \
    '%s\n%s\n```shell\n%s\n```\n\n%s\n' \
    "vendoring: $subtree_action subtree 'src/$local_dir'" \
    "$brief_message" \
    "$(git_subtree_cli)" \
    '[thisrepo]: '"$this_repo_pull_url"
  cd "$outer_repo_root" ||
    logfFatal 'could not CD to repo root: %s\n' "$outer_repo_root"
  git_subtree_cli "$git_subtree_message" || logfFatal \
    "git subtree $subtree_subcmd failed; see its error output above\n"
}

function git_subtree_cli() (
  local pre_cmd=echo; [[ $# -eq 0 ]] || pre_cmd=''
  if [[ $# -ne 0 ]]; then set -x; fi
  $pre_cmd git subtree "$subtree_subcmd" --squash \
     --message "${1:-'this commit message here'}" \
     --prefix "$prefix_relative_repo" \
     "$git_remote" "$git_ref"
)

case "$subtree_action" in
  init)
    (( $# == 3 )) || logfFatal \
      'expected 3 args (got %d) after init: LOCAL_DIR GIT_REMOTE_URL GIT_REFBRANCH see -h\n' \
      "$#"
    declare -r git_remote="$2" git_ref="$3"
    if have_localdir_record "$local_dir"; then
      logfFatal \
        'record already exists for "%s" (want "remove" action first?)\n' \
        "$local_dir"
    fi
    assert_not_yet_made

    #
    # Special MS-Clippy hadling for vim-pack additions (because that's >90% of
    # what we vendor as of 2022-07)
    #
    if strContains "$local_dir" vim-pack/; then
      strContains "$local_dir" /start/ || logfFatal \
        'common vim-pack usage error: vim needs "start" somewhere
         but got "%s"
         see other entries in gitmap file for good examples:
           %s\n' \
        "$local_dir" \
        "$git_map"
    fi

    # Append a record in our TSV (Tab Seperated Value) gitmap file
    (
      # utilize printf overloading
      printf '%s\t' \
        "$local_dir" \
        "$git_remote" \
        "$git_ref"
      echo # ensure we have a linebreak
    ) >> "$git_map"
    ;;
  download)
    assert_not_yet_made
    mkdir -p "$src_dir"
    run_git_subtree
    ;;
  update)
    assert_already_made
    run_git_subtree
    ;;
  remove)
    if have_localdir_record "$local_dir"; then
      record_number="$(get_record_linenum "$local_dir")" ||
        logfFatal 'internal bug: failed to get record for entry "%s"\n' "$local_dir"
      sed --in-place "$record_number"' d' "$git_map"
    else
      logfWarning \
        'No record for "%s"; assuming was manually deleted and carrying on to delete git content...\n' \
        "$local_dir"
    fi

    if is_localdir_present "$local_dir"; then
      rm -rf "$(localdir_absolute)" || logfFatal \
        'failed deleting git content at: %s\n' "$(localdir_absolute)"
    else
      logfWarning \
      'No git content to cleanup at src/%s\n' \
      "$(localdir_absolute)"
    fi
    ;;
esac
