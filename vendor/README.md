# vendor/ folder

This doc tries to explain what this `vendor/` folder is doing in my dotfiles.

This folder is intended to hold vendored source code that I've placed here
utilizing git subtree ([snapshot][subtreedoc]). This initially primarily
started for the .vim/ file files of this repository but I already have plans to
vendor in some scripts form my own personal repositories (entirely unrelated to
vim).

Since `git submodule` actions are relative to the outer repo (this repo) root,
this directory shouldn't be assumed to operate only for vim's purposes.

## Usage

As a **tl;dr** here's a recap of what's already documented elsewhere:

- `vendor/subtree help`: tool to install and manage individual vendored source trees
- `vendor/update refresh`: download the latest from **all** vendored source; **and**
  refresh tooling dependent on said sources (eg: vim doc). The latter is
  unrelated to vendoring but is _dependent_ on vendoring, hence its living here.
  - NOTE: this is _probably_ what should be called more than any other custom
    logic this directory provides.
- `$DOTFOREST_ROOT/bin/dotforest/sideload`: primary example usage of the _above_ script.

## Layout of this subdirectory:

- vendor/README.md - this file you're reading now.
- vendor/poke-at-vimdir - temporary script to help identify old hand-vendored
  code. See mention of it in step 2.a.v of section "Migration Status" below.
- vendor/subtree - a script to assist in the process of subtree management
  that's described as a manual-process by this readme.
- vendor/gitmap - a simple text file indexing basic data as tab separated values
    per row. those values ("columns") are:
    - 1. folder [string]: local folder path of the repository being "subtree"d by
      gitsubtree. Path is relative to this directory (so `vendor/foo/bar` will be
      written to this column as `foo/bar`)
    - 2. git remote [string]: URL of the upstream git repository being pulled or
      added from.
    - 3. ref [string]: the ref/branch of the repository in (2) that should be
      pulled or added from.
- vendor/src/vim-pack/ - root for all vendored source code for vim plugins. This
  directory is 1:1 with `$XDG_DATA_HOME/nvim/site/pack`. For example a
  repository for the vim plugin files for "syntastic" might end up in
  `src/vim-pack/syntastic/` (eg `src/vim-pack/syntastic/start/syntastic/...`
  `src/vim-pack/syntastic/opt/syntastic/...).

For the doc writing convenient below I'll refer to the `vendor/gitmap` file as
`$VENDORED_URLS`.

[subtreedoc]: https://github.com/git/git/blob/88d915a634b449147855041d44875322de2b286d/contrib/subtree/git-subtree.txt

## Example .vim/ file management

Thsi section describes what the `./subtree` script is doing - that is: there's
just a handful of small commands its running, and you can replicate the work
it's doing by just following the instructions in this section (instead of
trusting the script).

WARNING: statements about `.vim/` files _specifically_ are likely partly
hypothetical while "#migratino status" sectino below disappears.

Upstream vim plugins are managed in `$VENDORED_VIM`, using `git subtree
--squash` vendoring strategy. Subtree repos that were utilized are listed in
`$VENDORED_URLS`(DO NOT SUBMIT). As far as vim is concerned, the plugins are
detected because they're laid out per the vim "package" layout.

### Vim "Packages" - Successor to vim-plugin

According to [nvim's documentation][nvimpack] each immediate subdir under
`pack/` dir is representative of a single plugin. Each plugin can itself have
its own subdir `start` or `opt` - the former for code to be loaded immediately,
the latter to be loaded only if I call it (vi vimrc, or manually during
runtime).

The only work to do to utilize these is:
1. simply **have** them around - see "vim package locations" and "adding a new package" below
2. having a [`packloadall`][packloaddoc] call in your vimrc

[nvimpack]: https://neovim.io/doc/user/usr_05.html#05.5
[packloaddoc]: https://neovim.io/doc/user/repeat.html#packages

### vim package locations: `$VENDORED_VIM`

`$VENDORED_VIM` for our purposes, in this repo (per "Layout of this
subdirectory" section above) is at `vendor/src/vim-pack`. Vim and/or neovim will
each think:

- vim: plugins are in: `.vim/pack`
- neovim: plugins are in `$XDG_DATA_HOME/nvim/site/pack`

Both of which are achieved via symlink, automatically maintained via
`../bin/dotforest/vim-setup.sh` script.

### Adding a new package

1. save the URL to the upstream repo for yourself in `$VENDORED_URLS` in a
   tab-separated-format to make a 3-tuple of (local-dir, upstream-url, ref)

   ```sh
   ( printf '%s\t' "$VENDORED_VIM/$WHATEVER_PROJECT" "$WHATEVER_PROJECT_URL" master; echo;) | tee -a $VENDORED_URLS
   ```

2. use `git subtree` to actually vendor the content:

   ```sh
   $ git subtree add --squash \
       --prefix .vim/$VENDORED_VIM/$WHATEVER_PROJECT \
       "$WHATEVER_PROJECT_URL" master
    ```

### Updating a package

1. Identify the repo you're updating; see `$VENDORED_VIM` dir contents and
   `$VENDORED_URLS` mapping
2. use `git subtree` to pull in the latest content

   ```sh
   $ git subtree pull --squash \
       --prefix .vim/$VENDORED_VIM/$WHATEVER_PROJECT \
       "$WHATEVER_PROJECT_URL" master
    ```

### Migration Status

last updated 2021-11-06

starting just after `8b928069de1f9df952b584f37a107eb552dbda89` this repo is
working to cleanup its raw vendoring strategy in favor of the new `git subtree`.

the strategy taken here is to:

1. **optional**: blow away all local vendored vim content
2. convert all old vendored content to subtree; continue until step (2.a) yields
   no results (or no results you care about):
   - a. identify the `$NEXT_THING` to vendor the "new" way (step b)
        - i. `git checkout 8b928069de1f`
        - ii. `ls -aFH .vim/*/`
        - iii. identify a project-name looking string (among the files or
            directories); let's refer to this as `$NEXT_THING`
        - iv. go find the latest upstream repo for that codebase
            (`git lg path/to/$NEXT_THING` might help here). let's refer to this
            as `$NEXT_THING_URL`
        - v. if step (1) above wasn't done, then delete *just* the files that
            have to do with your stale old vendoring of `$NEXT_THING`; Figuring
            out which files might be helped by `./poke-at-vimdir` script (and
            source) in this directory.
   - b. vendor the new way with `git subtree` ie:
        ```sh
        $ git rev-parse --show-toplevel
        $ git subtree add --squash \
            --prefix .vim/$VENDORED_VIM/$NEXT_THING \
            "$NEXT_THING_URL" master
        ```
   - c. test that `$NEXT_THING` in vim
3. merge this branch back into master
