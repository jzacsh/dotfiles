#!/usr/bin/env bash

[[ "$BASH_VERSINFO" -ge 4 ]] || {
  printf 'zOMG,omg this is below bashv4\n' >&2
  exit 1
}

# aliases #####################
alias ls='ls --color'
alias la='ls -aFH'
alias  l='la -lha'
alias sl='ls -lFHh'
alias ca='clear; l'
alias ipt='sudo iptraf'
alias pbcopy='xsel --clipboard --input'
alias pbpaste='xsel --clipboard --output'
alias eclimd='"$ECLIPSE_HOME"/eclimd'
if type -p python >/dev/null 2>&1 ||
   type -p python3 >/dev/null 2>&1; then
  if type -p python3 >/dev/null 2>&1; then
    alias python=python3
  fi
  alias json='python -mjson.tool'
else
  printf 'no python in $PATH somehow? json alias will be msising...\n' >&2
fi
# most commonly I'd like to convert: decimal <=> hex
alias tohex="printf '0x%x\n'"
alias fromhex="printf '%0.0f\n'"
alias beep=bell
alias p='${PAGER:-less}'
alias sue='sudo "$EDITOR"'
alias rund=run-dir-change
alias fehw='feh --image-bg white'
function cl() { cd "$1" && l; }
complete -o nospace -F _cd cl # clone of `complete -p cd`


if ! type view >/dev/null 2>&1 && [[ "$EDITOR" = nvim ]]; then
  alias view='nvim -R'
fi

alias csvpretty='column -t -s,' # eg: csvpretty < my.csv
if type pip3 >/dev/null 2>&1; then
  alias pip=pip3
fi
alias annoy='beep $(( ( 6 * RANDOM / 32767 ) + 1 ))'

function e() { "${EDITOR:-vi}" "$@"; }

# Fuzzy-finder workflow-setup
#
# fze = fuzzy-finding to $EDITOR
# pze = fuzzy-finding, with preview, to $EDITOR
#
# TODO(zacsh) replace this all with upstream tooling that's way more robust
# (considerations for `kill` autocompletion, `cd` autocompletion, etc, etc.
#
# TODO force fzf to use my inputrc (eg: right now ESC doesn't put me into edit
# mode). per https://github.com/junegunn/fzf/issues/2365 this is going to have
# to be done manually.
#
# WARNING: this must be kept in sync with my nixos-bashrc.sh file that runs
# later, where I define completions for the helpers below.
if command -v fzf-tmux >/dev/null; then
  # TODO(zacsh) fix fzf-tmux causing new terminal size, but then tmux not
  # getting a chance to recalculate; for details, see:
  # https://github.com/junegunn/fzf/discussions/2876
  function fz() { fzf-tmux "$@"; }
elif command -v fzf >/dev/null; then
  function fz() { fzf "$@"; }
fi
if command -v fz >/dev/null; then
  alias pz="fz --preview='head --lines="$LINES" {}'"
  function fze() { local f; f="$(fz "$@")" || return 1; "$EDITOR" "$f"; }
  function pze() { local f; f="$(pz "$@")" || return 1; "$EDITOR" "$f"; }
fi

# TODO if you figure completion of fzf and fzf-tmux,, then setup such
# completion for _all_ aliases and funcs in this file
#
# use yblib.cloneCompletion

###################################
# start muscle-memory retraining...

# I'm never on machines where I have real vi anymore; ensure I'm using one call
# under the hood (thus vi or vim calls don't workaround my one true editor
# preference).
function vi() { printf 'ERROR[WRIST-SLAP]: use `e` (editor) instead!\n' >&2; }
function vim() { vi; }

function pagebin() { printf 'ERROR[WRIST-SLAP]: use `pagecmd` (or pagecmd -e) instead!\n' >&2; }
function descbin() { printf 'ERROR[WRIST-SLAP]: use `descmd` (or descmd -e) instead!\n' >&2; }

if command -v btop >/dev/null 2>&1; then
  function htop() { printf 'ERROR[WRIST-SLAP]: use `btop` instead!\n' >&2; }
fi

# end of muscle-memory retraining...
####################################

if type httpd >/dev/null 2>&1;then
  alias httpserve='httpd'
elif type go >/dev/null 2>&1;then
  alias httpserve='go run "$HOME"/bin/share/httpd.go'
else
  alias httpserve='python3 -m http.server'
fi

######################################
# preferred args/modes for given tools
alias diff='colordiff' # better diff ouput
alias grep='grep --color=auto' # better grep ouput
alias mutt='pgrep mutt && mutt -R || mutt' # single mutt PID management
type -p grc > /dev/null 2>&1 &&
  alias log='grc tail -F' # better tail
type -p node > /dev/null 2>&1 &&
  alias replnode='NODE_NO_READLINE=1 rlwrap node' # better node repl
type -p deno &&
  alias repldeno='rlwrap --always-readline  deno'

# x env #######################
alias fixcaps='setxkbmap -option ctrl:swapcaps' # swaps caps with right-control

######################################
## typos; common spelling mistakes ###
alias les=less
yblib.cloneCompletion less les
alias duff='echo "no beer here, try \`diff\`." >&2; [[ 1 -eq 0 ]]'
alias :w='echo "yeahh... this is not [n]vim." >&2; [[ 1 -eq 0 ]]'
alias :q=':w'
alias :e=':w'
alias :x=':w'
alias gt=git
alias it='echo "did you mean Git?" >&2; [[ 1 -eq 0 ]]'
# prefixes to display-sensitive commands (tmux/ssh/console considerations)
alias xf='DISPLAY=localhost:10.0 '
alias xl='DISPLAY=:0.0 ' #eg: `xl xdg-open ./my.pdf`

# grdle is a thin gradle wrapper that tries to detect if --offline should be
# used, to avoid needless cache-misses folloewd by network errors.
alias grdl=grdle
if yblib.haveCompletion gradle; then
  yblib.cloneCompletion gradle grdle
  yblib.cloneCompletion gradle grdl
fi

# taking notes in a meeting or lecture, instead of futtzing around with my
# gnome popdown calendar and navigating through months, or sleeping my notes and
# running `cal -3`
alias notes='"$EDITOR" +":Calendar -view=year -width=27 -split=vertical"'

# I can *never* remember this commandline argument (I always look for some other
# variant like "no image" or "no display" or "audio only" .. none of which help
# me find this magical --no-video in the documentation).
alias listen='mpv --no-video -- '
alias listn=listen

# Make use of autocompletion I wrote for doom in
# $XDG_CONFIG_HOME/bash_completion.d/doom-completion.sh
haveCallable doom || {
  if haveCallable prboom-plus; then
    alias doom=prboom-plus
  elif haveCallable prboom; then
    alias doom=prboom
  elif haveCallable chocolate-doom; then
    alias doom=chocolate-doom
  fi
}

function xev() (
  if [[ -n "$WAYLAND_DISPLAY" ]]; then
    printf \
      'this is wayland ($WAYLAND_DISPLAY=%s); calling `wev` instead...\n' \
      "$WAYLAND_DISPLAY" >&2
    (set -x; wev "$@"; )
  else
    command xev "$@"
  fi
)

function detox() {
  # work around https://github.com/dharple/detox/issues/12
  # cuased me a _lot_ of confusion and debugging before I realized some earlier
  # command (detox) hadn't actually worked (sending me down a rabbit hole
  # assuming something _else_ must've been wrong).
  local version; version="$(command detox -v)"
  local hasBugFixRegex='\b1.(4|5)\b' # v1.4 has the fix
  if [[ "$version" =~ $hasBugFixRegex ]]; then
    printf 'WARNING: delete `detox` override from ~/.bash_alias, you now have a version that has the bugfix you want\n' >&2
    command detox "$@"
  fi

  # doesn't handle complex args (like `detox -v -r . /foo/ ./ /bar`)
  if (( $# > 0 )) && [[ "${@: -1}" = '.' || "${@: -1}" = './' ]]; then
    command detox "$@" "$(pwd)"
  else
    command detox "$@"
  fi
}

# Lists all ports being listened to by PIDs on your machine.
function ports() {
  # set -x so I can always learn by osmosis what's going on
  if type ss >/dev/null 2>&1; then
    (
      set -x
      sudo ss --processes --all --tcp --numeric  --listening
    ) | grep --extended-regexp \
        '^|Local Address:Port|:[[:digit:]]+[[:space:]]+'
  else
    (
      set -x
      sudo netstat --tcp --numeric --listening --program
    ) | grep --extended-regexp \
        '^|[[:space:]]+Local Address[[:space:]]+|:[[:digit:]]+\s+'
  fi
}

function dmsg() {
  set -x
  sudo dmesg --force-prefix --userspace --kernel --follow --human --time-format=iso
}

# TODO consider moving this to "$HOSTFOREST_ROOT"/distro.arch
function dif() { diff -u "$1" "$2" | diff-so-fancy; }
type 'diff-so-fancy' >/dev/null 2>&1 || {
  echo 'warning: diff-so-fancy missing on this system; go find a copy' >&2
  unset dif
}

############
# one liners
chars() ( sed -e 's/\(.\)/\1\n/g'; )
tarl() ( printf 'use `unpack` instead\n' >&2; return 1; )
ident() ( identify -verbose $1 | grep modify; )
alias geo='identify -format "%[w]x%[h]\n"'
favipng() (
  set -euo pipefail
  local faviUrl
  faviUrl="$(curl -Ls "$@" | pup 'link[rel="icon"] attr{href}')"
  if [[ "${faviUrl:-x}" = x ]];then
    printf "Could not parse favicon from site's markup\n" >&2
    return 1
  fi
  local ext; ext="$(basename "$faviUrl")"; ext="${ext##*.}"
  local out; out="$(mktemp --tmpdir=. "favipng_XXXXX.${ext}")"

  ( set -x; curl -Ls "$faviUrl" > "$out"; )
  echo "$out"
)
mdown() ( markdown_py < /dev/stdin | html; )  # depends on html alias above
clock() ( while true; do printf '\r%s ' "$(date --iso-8601=ns)";done; ) # watch a running clock
tree() ( find -O3 $@ | sort; )
pcLog() (
  local t=tail
  if type grc >/dev/null 2>&1;then t='grc tail';fi
  $t -F -n 50 /var/log/{dmesg,udev,{sys,ufw.,kern.auth.}log} ~/usr/log/*.log
)
zpdf() ( zathura "$1" >/dev/null 2>&1 & disown; )

# bump font on the fly; from https://bbs.archlinux.org/viewtopic.php?id=44121
urxvtc_font() { printf '\33]50;%s%d\007' "xft:Terminus:pixelsize=" $1; }

xdg-query() (
  set -x
  xdg-mime query default "$(xdg-mime query filetype "$1")"
)

# (nice defaults) silversearcher-style; assume i want to grep all local files
type ag >/dev/null 2>&1 || ag() (
  (( $# )) || { printf 'usage: REGEXP [FILE|DIR]\n' >&2; return 1; }
  \grep --color=auto --recursive --line-number "$@"
  # grep(1): "Note that if no file operand is given, grep searches the working directory."
)

# poor man's asciinema. helpful for pasting commands used and their output in a single pipe
#
# Usage: COMMAND_LINE
#
# Example, assuming `you@machine` is your real shell prompt:
#   you@machine$ cliMock lsb_release --all
#
#   $ lsb_release --all
#   No LSB modules are available.
#   Distributor ID: Debian
#   Description:    Debian GNU/Linux testing-updates (sid)
#   Release:        testing-updates
#   Codename:       sid
#
# Example, more likely:
#   $ lsb_release --all | pastie # win
cliMock() ( printf '$ %s\n%s\n\n' "$*" "$(bash -c "$*" 2>&1)"; );

# Set wallpaper in gnome
#
# $1=wallpaper image
# $2=picture-options; any of:
#     none|wallpaper|centered|scaled|stretched|zoom|spanned
# $3=optional bg color as 6 digits of a hex code, else black is used
#
# zero arguments reset all gsettings values
gnome-background() (
  set -euo pipefail

  if ! (( $# ));then
    set -x
    gsettings reset org.gnome.desktop.background picture-uri
    gsettings reset org.gnome.desktop.background picture-options
    return
  fi

  local fileUri picOpt="$2" bgColor="${3:-'000000'}"

  printf -v fileUri 'file://%s' "$(readlink -f "$1")"
  local validPicOptRegexp='^(wallpaper|centered|scaled|stretched|zoom|spanned)$'
  if ! [[ "$picOpt" =~ $validPicOptRegexp ]];then
    printf 'error: got picture-option "%s" not matching "%s"\n' \
      "$picOpt" "$validPicOptRegexp" >&2
    return 1
  fi

  if (( $# > 2 ));then
    local validHex='^([[:alnum:]][[:alnum:]][[:alnum:]][[:alnum:]][[:alnum:]][[:alnum:]])$'
    if ! [[ "$bgColor" =~ $validHex ]];then
      printf 'error: got optional bg color "%s" not matching regexp for 6 digits\n' \
        "$bgColor" >&2
      return 1
    fi
  fi

  set -x
  gsettings set org.gnome.desktop.background picture-uri "$fileUri"
  gsettings set org.gnome.desktop.background picture-options "$picOpt"
  gsettings set org.gnome.desktop.background primary-color "#$bgColor"
  gsettings set org.gnome.desktop.background secondary-color "#$bgColor"
  set +x
  echo 'done; for more options, see `gsettings list-recursively org.gnome.desktop.background`'
)

alias gnome_wallpaper='gnome-background'

keyboard() (
# NOTE: step #1 might not be necessary, perhaps bluez just expects a PIN typed
# identically in both places. will try to clarify next time.
  cat <<EO_INSTRUCTION
assuming:
  a) \`bluez\` and \`hidd\` exists; respectively: apt-get install bluez bluez-compat
  b) a "DisableSecurity" line lives in /etc/bluetooth/network.conf
  c) KEYBOARD_ID is of format XX:XX:XX:XX:XX:XX (eg: "08:B7:38:12:B6:CA")
     find via: \`bluez-test-discovery\`

1) disable security, via /etc/bluetooth/network.conf
2) pair: \`bt-device --connect=KEYBOARD_ID\`
   a) type "0000⏎" at prompt (ie: here, on host)
   b) enter "0000⏎" on bluetooth keyboard (and step #2 should return 0)
3) connect: \`sudo hidd --connect KEYBOARD_ID\`
4) cleanup step #1
EO_INSTRUCTION
)

#`hg shelve` extension is broken for some reason.
hgunshelve () (
  local patch

  if [[ $1 = -l ]];then
    ls -la "$(hg root)/.hg/shelves/"
  else
    patch="$(hg root)/.hg/shelves/$1"

    if [[ -f $patch && -w "$patch" ]];then
      pushd "$(hg root)" #run patch cmd. from top-level
      patch -p1 < "$patch"
      if (( $? == 0 ));then
        read -p 'Patch successfull; Remove patch file? [Y/n] ' answ
        [[ "${answ/,,}" == y ]] && rm -v "$patch"
      fi
      popd
    else
      printf 'Error: not a writeable patch file: "%s".\n' "" >&2
      return 1
    fi
  fi
)

# `dict`, but treat args just as `echo` does, and add highlighting
define() {
  dict "$*" |
    grep --ignore-case --color=always --extended-regexp '^|'"$*" |
    less -LINE-NUMBERS
}

#dictionary look ups
lu() (
  local lang=en; # default
  if [[ "${LANG:-x}" != x ]] && [[ -n "${LANG/_*/}" ]];then
    lang="${LANG/_*/}"
  fi

  [[ "$#" -eq 1 ]] || { printf 'usage: WORD\n' >&2; return 1; }

  local target="$1"
  curl --silent --location "https://${lang}.wiktionary.org/wiki/$target" |
    w3m -dump -T text/html |
    grep --color=always --extended-regexp '^|\b'"$target"'\b' |
    "${PAGER:-less}"
)

#translate
trans() (
  local orig="$1"
  local targ="$2"
  shift;shift
  local text="$*"
  local google_api='http://ajax.googleapis.com/ajax/services/language/translate?v=1.0&q='
  local url="${google_api}${text// /+}&langpair=${orig}|${targ}"
  curl ${url} 2>/dev/null #| sed 's/.*"translatedText":"\([^"]*\)".*}/\1\n/'
)

# List the newest N(="$2")-files in directory "$1"
rlatest() (
  local count=${2:-1}

  find "${1:-.}" -type f -printf '%T@ %p\0' | sort -znr | {
    while (( count-- )); do
      read -rd ' ' _
      IFS= read -rd '' file
      printf '%s\n' "$file"
    done
  }
)

#eg.: notify me when a tarball is finally uploaded to dropox
# usage: URL [ READY_MESSAGE ]
notifyhttp() (
  local msg url retry
  retry=2

  url="$1"
  if [[ -z $url ]];then
    return 1
  fi

  msg="${2:-ready!}"
  while true; do
    curl -fI "$url" && {
      printf '%s\n' "$msg" ; break
    } || {
      printf '... URL failed, retrying in %s seconds.\n' "$retry"
      sleep "$retry"
    }
  done
)

whiteboardify() (
  local default_levels='60%,91%,0.1'
  local levels="${3:-"$default_levels"}"
  { [[ "$#" -eq 2 ]] || [[ "$#" -eq 3 ]]; } || {
    echo "usage: IN_FILE OUT_FILE [LEVELS]
      to generate a whiteboardified version of INFILE
      and write it to OUTFILE

      LEVELS: defaults to $default_levels
    " >&2
    return 1
  }
  convert "$1" \
    -morphology Convolve DoG:15,100,0 \
    -negate \
    -normalize \
    -blur 0x1 \
    -channel RBG \
    -level "$levels" \
    "$2"
)

baseFromTo() (
  local quiet=0
  if [[ "$#" = 4 ]];then
    [[ "$1" = -q ]] || {
      printf 'usage: [-q] FROM TO FROM_VALUE\n' >&2
      return 1;
    }
    shift; quiet=1;
  fi
  local fro="$1"
  local to="$2"
  shift 2
  (( quiet )) || set -x
  printf -- 'obase=%d; ibase=%d; %s\n' "$to" "$fro" "${*^^}"  | bc
)

# 'cargo watch' but shorter; intended to be my hacky variant of this discussion:
# https://github.com/rust-lang/rust/issues/27189 so I don't need to
# re-investigate this everytime I hit this issue.
function crgwtch() {
  local def_height=50 height="$def_height"
  if [[ "$(tput lines 2>/dev/null | wc -l)" -gt 0 ]]; then
    if tty_height="$(tput lines)"; then
      if [[ -n "$tty_height" && "$tty_height" -gt 0 ]]; then
        height=$(( 10#$tty_height - 20 ))
      fi
    else
      printf 'WARN: `tput` issue? falling back to height=%s\n' "$def_height" >&2
    fi
  fi

  local sh_cmds; declare -a sh_cmds=()
  local cwatch_shell_fmt='set -x; cargo %s |& head -n '"$height"

  local testRunBuildRegex='^\s*(test|build|run)\s*$'
  # $1=cmd
  local appendCmd
  function appendCmd() {
    local cmd="$1"
    if yblib.hasColor && [[ "$cmd" =~ $testRunBuildRegex ]]; then
      cmd+=" --color=always "
    fi
    sh_cmds+=( "$(printf -- '--shell='"$cwatch_shell_fmt" "$cmd")" )
  }

  local isShFlag
  function isShFlag() { [[ "$1" = -s ]] || [[ "$1" = --shell ]]; }

  local usr_cmd
  if [[ "$#" -gt 0 ]] && isShFlag "$1"; then
    usr_cmd=''
    local cmd_chunk
    for cmd_chunk in "$@"; do
      if isShFlag "$cmd_chunk"; then 
        [[ -z "$usr_cmd" ]] || appendCmd "$usr_cmd"
        usr_cmd=''
        continue
      fi
      usr_cmd+=' '"$cmd_chunk"
    done
    [[ -z "$usr_cmd" ]] || appendCmd "$usr_cmd"
  else
    usr_cmd=test; [[ "$#" -eq 0 ]] || usr_cmd="$1"
    [[ "$#" -le 1 ]] || {
      printf 'usage: either pass one command or multiple via -s|--shell\n' >&2
      return 1
    }

    appendCmd "$usr_cmd"
  fi

  (
    set -x
    cargo watch \
      --clear \
      "${sh_cmds[@]}"
  )
}

#########################################
# `mktemp` wrappers/workflows ###########

_assertEnvVariable() {
  [ -n ${!1} ] && return 0
  printf 'Error: $%s env. variable not set\n' "$1" >&2
  return 1
}

_collectWithEditor() (
  _assertEnvVariable EDITOR || return 1

  local contentName="$1"
  local tdir="$2"

  local when; when="$(
    date --iso-8601=minutes |
      sed -e 's|:|.|g' -e 's|-||g'
  )"
  local tmpl; tmpl="$(printf '%s_%s-from-EDITOR_XXXXXXX.txt' "$when" "$contentName")"
  local tmpfile; tmpfile="$(
    if [ -z "${2/ */}" ];then
      mktemp --tmpdir      "$tmpl"
    else
      mktemp --tmpdir="$tdir" "$tmpl"
    fi
  )"

  [ -w "$tmpfile" ] || {
    printf 'failed to open a writeable temporary file\n' >&2
    return 1
  }

  {
    "$EDITOR" "$tmpfile" < /proc/"$$"/fd/0 > /proc/"$$"/fd/1 &&
      [ "$(stat --printf='%s' "$tmpfile")" != 0 ];
  } || return $?

  printf '%s' "$tmpfile"
  return 0
)

tmp_to_pastie() (
  local contentF; contentF="$(_collectWithEditor pastie)"
  [ $? -ne 0 ] && return 1

  local pastieExit=0
  if [ -n "$1" ] && { [ "$1" = c ] || [ "$1" = -c ]; } then
    _assertEnvVariable BROWSER || return 1
    "$BROWSER" "$contentF"; pastieExit=$?      # paste with clipboard/x11
  else
    _assertEnvVariable PASTIE || return 1
    "$PASTIE" $@ < "$contentF"; pastieExit=$?  # use proper pastebin
  fi

  if [ $pastieExit -ne 0 ];then
    printf 'ERROR: failed to pastie contents of:\n\t%s\n' "$contentF" >&2
    return $pastieExit
  fi

  rm "$contentF"
)

vmail() (
  local contentF; contentF="$(_collectWithEditor vmail)"
  [ $? -ne 0 ] && return 1

  if \mail $@ < "$contentF";then
    printf 'ERROR: failed to mail contents of:\n\t%s\n' "$contentF" >&2
    return 1
  fi

  rm "$contentF"
)

tmp_encrypt_mail() (
  _assertEnvVariable EMAIL || {
    printf 'Error: need your key uid to sign with' >&2
    return 1
  }

  { [ $# -ne 0 ] && [ "$1" != "$EMAIL" ]; } || {
    printf 'Error: expected emails whose public keys should be signed with\n' >&2
    return 1
  }

  local msgF; msgF="$(_collectWithEditor unencrypted-msg-for-gpg ./)"
  [ $? -ne 0 ] && return 1

  local recips; recips=($@); recips+=("$EMAIL")
  local recipArgs; recipArgs="$(printf ' --recipient %s ' ${recips[@]})"

  printf \
    'Clear message composed in:\n\t%s\nEncrypting its contents for:\t%s\n\n' \
    "$msgF" "${recips[*]}"
  set -x
  gpg --sign --armor $recipArgs --encrypt "$msgF"
)

# Thin wrapper for mktemp(1) to quickly create a directory with a memorable
# phrase and prefixed with the current date and time, so the likelihood of
# identifying what the directory is _much later_ is greater.
#
# $1 = keyword
#    Some human-readable phrase that will be memorable/identifiable later.
# $2 = type
#    Either "build" to indicate these are potentially very large files we
#    _likely_ don't want backed up, or any other string (e.g. "scrap") to
#    indicate it's just discard files, but we're not necessarily creating an
#    explosion in disk space usage.
function mkScratchDir() (
  local keyword="${1:-scratch}"
  local scratchType="${2:-build}"

  local tmpDirRoot="$HOME"/.local/build/
  [[ "$scratchType" = build ]] ||
    tmpDirRoot="$(readlink -f "$(xdg-user-dir DOCUMENTS)"/../scraps)"
  mkdir -vp "$tmpDirRoot" || {
    printf 'mkScratchDir[%s]: creating tmp dir: %s\n' \
      "$scratchType" "$tmpDirRoot" >&2
    return 1
  }

  if [[ -d "$tmpDirRoot" ]];then
    TMPDIR="$tmpDirRoot" yblib.mkHumanTempDir "$keyword"
  else
    yblib.mkHumanTempDir "$keyword"
  fi
)
scrap_dir() { pushd "$(mkScratchDir "${1:-nolabel}" scraps)"; }
# TODO cleanup build_dir; never use it anymore (and then cleanup its codepath in
# mkScratchDir
build_dir() { pushd "$(mkScratchDir "${1:-nolabel}" build)"; }

vid_get_duration() (
  avconv -i "$1" 2>&1 |
      grep Duration |
      sed -e 's|.*Duration:\ \(.*$\)|\1|' |
      cut -f 1 -d ' ' |
      sed -e 's|,$||g'
)

# Args:
# - $pics = $0...$N-1
# - $gif_out_file = $N
#
# Eg: pics_to_gif cat_sitting.png cat_tail_swish.png cat.gif
function pics_to_gif() { convert -loop 0 "$@"; }

function vid_to_frames() (
  set -e

  if [[ "$#" -ne 2 && "$#" -ne 3 ]]; then
    printf 'usage: [-p] SRC_VIDEO OUT_DIR\n' >&2
    printf '  -p: use PNG format, instead of webp\n' >&2
    return 1
  fi

  local useWebp=1
  if [[ "$#" -eq 3 ]];then
    [[ "$1" = -p ]] || {
      printf 'unexpected second arg in 3-arg call; try zero-arg for help\n' >&2
      return 1
    }
    useWebp=0
    shift
  fi

  local inVid="$(readlink -f "$1")";
  [[ -n "$inVid" && -r "$inVid" && -f "$inVid" ]] || {
    printf 'SRC_VIDEO must be a regular, readable file\n' >&2
    return 1
  }

  local framesDir="$2"
  # see yabashlib's isDirectoryEmpty impl
  [[ "$(find "$framesDir")" = "$framesDir" ]] || {
    printf 'OUT_DIR must be empty; got dir: %s\n' "$framesDir" >&2
    return 1
  }

  # Figure out *which* utility to use
  local vidExec vfArgs
  local scale=900
  if type avconv > /dev/null 2>&1;then
    vidExec=avconv
    vfArgs="scale=${scale}:-1:flags=lanczos -r 10"
  elif type ffmpeg > /dev/null 2>&1;then
    vidExec=ffmpeg
    vfArgs="scale=${scale}:-1:flags=lanczos,fps=10"
  else
    printf 'ERROR: could not find `ffmpeg` or `avconv` utilities\n' >&2
    return 1
  fi

  #######################
  # real work starts here

  local framePrefix=ffout
  time { "$vidExec" -loglevel quiet -i "$inVid" -vf $vfArgs "$framesDir"/"$framePrefix"%03d.png; }

  if (( useWebp )); then
    while read -r pngFrame; do
      if cwebp "$pngFrame" -o "$(dirname "$pngFrame")"/"$(basename "${pngFrame/.png}")".webp; then
        rm "$pngFrame"
      fi
    done < <(find "$framesDir" -type f -regex '.*'"$framePrefix"'.*\.png$' -print)
  fi

  printf \
    'DONE: %s frames extracted from "%s" into dir "%s", listed below:\n' \
    "$(find "$framesDir" -type f -regex '.*'"$framePrefix"'.*' -print | wc -l)" \
    "$inVid" \
    "$framesDir"

  find "$framesDir" -type f -regex '.*'"$framePrefix"'.*' -print
)

function vid_to_gif() (
  set -e

  if [[ "$#" -ne 2 && "$#" -ne 3 ]]; then
    printf 'usage: [-k] SRC_VIDEO OUT_GIF\n' >&2
    printf '  -k: keep temporary files used during construction\n' >&2
    return 1
  fi

  local keepScraps=0
  if [[ "$#" -eq 3 ]];then
    [[ "$1" = -k ]] || {
      printf 'unexpected second arg in 3-arg call; try zero-arg for help\n' >&2
      return 1
    }
    keepScraps=1
    shift
  fi

  local inVid="$(readlink -f "$1")";
  [[ -n "$inVid" && -r "$inVid" && -f "$inVid" ]] || {
    printf 'SRC_VIDEO must be a regular, readable file\n' >&2
    return 1
  }

  local outGif="$2"
  if [[ -z $outGif ]];then outGif="$(dirname "$inVid")"/out.gif; fi

  local framesDir="$(mktemp --directory --tmpdir 'vid-to-gif_frames_XXXXXXX')"
  [[ -n "$framesDir"  && -d "$framesDir" ]] || {
    printf 'FAILED: failed setting up temporary output directory to collect frames to\n' >&2
    return 1
  }

  #######################
  # real work starts here

  printf '[STEP 1 of 3]\tExporting frames from "%s"\n' "$inVid"
  time { vid_to_frames -p "$inVid" "$framesDir"; } || {
    printf 'FATAL: failed to extract frames in step 1; see errors above\n' >&2
    return 1
  }

  printf '[STEP 2 of 3]\tCompiling frames into single GIF in "%s"\n' "$outGif"
  time { pics_to_gif "$framesDir"/ffout*.png "$outGif"; } || {
    printf 'FATAL: failed to compile frames in step 2; see errors above\n' >&2
    return 1
  }

  if (( keepScraps ));then
    printf '[STEP 3 of 3]\tLeaving frames and temp dir intact:\n\t"%s"\n' "$framesDir"
  else
    printf '[STEP 3 of 3]\tCleaning up frames and temp dir:\n\t"%s"\n' "$framesDir"
    rm "$framesDir"/*.png
    rmdir "$framesDir"
  fi
)

function gif_to_vid() {
  local src_gif="$1"
  local out_vid="$2"

  # mp4-generation adapted from https://unix.stackexchange.com/a/294892
  ffmpeg \
    -i "$src_gif" \
    -movflags faststart \
    -pix_fmt yuv420p \
    -vf 'scale=trunc(iw/2)*2:trunc(ih/2)*2' \
    "$out_vid"
}

alias strip_location_vid=vid_strip_location
function vid_strip_location() {
  local src_vid="$1" out_vid="$2"
  [[ "$#" -eq 2 ]] || {
    printf 'error: usage: %s SRC_VID OUT_VID\n' "$FUNCNAME" >&2
    return 1
  }

  [[ -e "$src_vid" && -r "$src_vid" ]] || {
    printf \
      'SRC_VID must be a readable file; got: "%s"\n' \
      "$src_vid" >&2
    return 1
  }

  if [[ -e "$out_vid" ]]; then
    printf \
      'ERROR: OUT_VID already exists; too cautious to overwrite: "%s"\n' \
      "$out_vid" >&2
    return 1
  fi

  time (
    set -x

    ffmpeg \
      -i "$src_vid" \
      -metadata location='' \
      -metadata location-eng='' \
      -acodec copy \
      -vcodec copy \
      "$out_vid"
  )
}

# list all things you could call.
# Defaults to only showing executables in $PATH, unless 'all' is passed, then
# functions, aliases, bash builtins, etc. are all shown.
#
# Really helpful if there's a command you remember, but you're not sure the name
# of and want to do some creative grepping.
list_bins() {
  while read thing; do
    if (( $# == 0 )); then # default behavior
      [[ -n "$(type -p "$thing")" ]] || continue
    fi
    echo "$thing"
  done < <(compgen -c | sort | uniq)
}

secret() (
  set -euo pipefail
  # adapted from https://github.com/drduh/YubiKey-Guide#using-keys about
  # 20dd0687cdd70a3b8072f379157e1fac7fa711d6
  local output="$1".enc
  if [[ -s "$output" ]]; then
    echo 'output file already exists' >&2
    return 1
  fi

  (
    set -x
    gpg \
      --output "$output" -r "$2" \
      --encrypt --armor "$1"
  ) && echo "$1 -> $output"
)

reveal() (
  set -euo pipefail
  # adapted from https://github.com/drduh/YubiKey-Guide#using-keys about
  # 20dd0687cdd70a3b8072f379157e1fac7fa711d6
  local input="$1"
  local output; # strip ".TIMESTAMP.enc" file suffix
  output="$(echo "$input" | sed --expression 's,\.[[:digit:]]*\.enc$,,g')"
  ( set -x; gpg --decrypt --output "$output" "$input"; ) &&
      echo "$input -> $output"
)

function ttyListFonts() {
  find \
    -L \
    /etc/kbd/consolefonts \
    -type f \
    -name '*.gz' \
    -printf '%P\n' |
      sort
}

function ttySetFont() {
  local gzBaseName="$1"
  [[ -n "$gzBaseName" ]] || {
    cat >&2 <<EOF_USAGE
usage: ${FUNCNAME[0]} FONT_BASENAME

Params
    FONT_BASENAME should be one of the gzipped files that 'ttyListFonts' shows.

Description
    This is useful when in a Virtual TTY (eg: ctrl+alt+1) and the resolution is
    painfully low (the font-size is huge) and you'd like to make the TTY
    realestate more usable.
EOF_USAGE
    return 1
  }
  [[ -e /etc/kbd/consolefonts/"$1" ]] ||
    printf \
      '"%s" not a font in ttyListFonts would output? ... running anyway...\n' \
      "$gzBaseName" >&2
  (
    set -x
    sudo setfont "$gzBaseName"
  )
}

if haveCallable dua; then
  function du() {
    local isPwd=1
    if [[ "$#" -eq 1 ]] && [[ -d "$1" ]]; then
      isPwd=0
      pushd "$1"
    fi
    ( set -x; dua interactive; )
    (( isPwd )) || popd
  }
elif haveCallable dust; then
  function du() ( set -x; dust; )
fi

function lgsq() (
  haveCallable wordq || {
    printf 'wordq dependency missing\n' >&2
    return 1
  }

  local queryRegexp="$1"
  local lgsqRootD="$HOME"/sync/dweb/syncthing/content/default/logseq
  [[ -n "$queryRegexp" ]] || {
    cat >&2 <<EOF_USAGE
usage: ${FUNCNAME[0]} QUERY_REGEXP

Queries your logseq content for files or files' contents matching QUERY_REGEXP.
Just a thin wrapper for 'wordq' program.

Note: logseq content is assumed to be located at:
  $lgsqRootD
EOF_USAGE
    return 1
  }
  cd "$lgsqRootD" || {
    printf 'could not CD to %s\n' "$lgsqRootD" >&2
    return 1
  }

  # we use '-d .' (and cd first) because we cannot get around the bug where:
  #   $ grep --exclude-dir=foo expression foo/'
  # interprets this^ as a "do nothing" because it considers the root-dir
  # excluded (as oppsoed to interpreting this as "foo/foo/" being excluded)
  local logseqInternalDir="$lgsqRootD"/logseq
  wordq \
    "$queryRegexp" \
    -d . \
    --grep_args \
    --color=always \
    --exclude-dir="logseq" \
    --exclude='hls__*.md' \
    --find_args \
    -not -regex "$logseqInternalDir"'/.*' \
    -not -regex "$lgsqRootD"'.*\.edn$' \
    -not -regex "$lgsqRootD"'.*/hls__.*' |
    "${PAGER:-less}"
)

# vim: et:ts=2:sw=2
