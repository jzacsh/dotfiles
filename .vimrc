" Classic-vim only; Only reachable via:
"    vim -u ~/.vimrc
" However this isn't the default for neovim which thanksfully uses
" $XDG_CONFIG_HOME (and thus $XDG_CONFIG_HOME/nvim/init.lua is the start
" script instead).
"
" Thus the contents of this file are old/quirky settings I don't think are
" applicable in the newer neovim/easy-LSP universe any longer. Eg: in neovim
" init script I use Lazy plugin manager, whereas here I use packloadall for my
" vendored vim plugins (see $DOTFOREST_ROOT/vendor/).
"
" The ONLY shared logic is the stuff that's most trivially shared:
" ~/.vim/init-compat.vim
"
" WARNING: ONLY write stuff in this file that should NOT be used by
" neovim ($XDG_CONFIG_HOME/nvim/init.lua), otherwise new stuff should go into
" either that init lua or into one of the shared files: ~/.vim/*-compat.vim.
"
" WARNING: when changing this file, ALWAYS test by temporarily setting
" verbose=9 (either via 'vim -V9' or 'set verbose=9') to make sure nothing is
" newly broken by your change!
"
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

source $HOME/.vim/init-compat.vim

" Colorschemes loaded by neovim and its Lazy echosystem are might nicer.
"enjoyed: slate, onedark
colorscheme slate
highlight Normal ctermbg=none
highlight Comment ctermfg=grey

"
" Keymaps which are better defined in the newer nvim universe (with which-key
" plugin running) and thus shouldn't be put in forward-compatable
" (keymap-compat.vim)
"
" Unfortunately this means inevitable drift between this block and the
" init.lua block.
nnoremap <Leader>f  <Cmd>echo("<Leader>f: OBSOLETE! use s[f] or s[c]")<Enter>
nnoremap <Leader>s  <Cmd>echo("<Leader>s: OBSOLETE! use s[f] or s[c]")<Enter>
" [S]earch the [f]iles in $PWD
nnoremap <Leader>sf  <Cmd>Telescope find_files<Enter>
" [S]earch the live [g]rep
nnoremap <Leader>sg  <Cmd>Telescope live_grep<Enter>
" [S]earch the [c]odebase; TODO: make this VCS-agnostic
nnoremap <Leader>sc  <Cmd>Telescope git_files<Enter>
" [S]earch the [s]ources of this codebase; TODO: make this VCS-agnostic
nnoremap <Leader>ss  <Cmd>Telescope git_files<Enter>
" buffer list
nnoremap <Leader>b  <Cmd>Telescope buffers<Enter>
" [s]earch [b]uffer list
nnoremap <Leader>sb  <Cmd>Telescope buffers<Enter>

"[S]earch current [W]ord
nnoremap <leader>sw <Cmd>Telescope grep_string<Enter>
"[S]earch by [G]rep
nnoremap <leader>sg <Cmd>Telescope live_grep<Enter>
"[S]earch [D]iagnostics
nnoremap <leader>sd <Cmd>Telescope diagnostics<Enter>
"[S]earch [R]esume
nnoremap <leader>sr <Cmd>Telescope resume<Enter>
"[S]earch Recent Files ("." for repeat)
nnoremap <leader>s. <Cmd>Telescope oldfiles<Enter>
"[S]earch [H]elp
nnoremap <leader>sh <Cmd>Telescope help_tags<Enter>
"[S]earch [K]eymaps
nnoremap <leader>sk <Cmd>Telescope keymaps<Enter>

"
" Language-specific settings
" TODO(jzacsh) aren't these handled by syntax files? maybe delete all of this?
""""""""""""""""""""""""""
if has("autocmd")
  " make files
  autocmd FileType make set noexpandtab

  "js
  autocmd FileType js set ts=2
  autocmd FileType js set sw=2
  autocmd FileType js set sts=2

  "php
  autocmd FileType php set ts=2
  autocmd FileType php set sw=2
  autocmd FileType php set sts=2

  "python
  autocmd FileType py set ts=2
  autocmd FileType py set sw=2
  autocmd FileType py set sts=2

  " ... but different width when reading mail
  autocmd BufRead ~/tmp/mutt-* set tw=72
  autocmd FileType java set tw=100

  " lesscss.org
  au BufNewFile,BufRead *.less set filetype less

  " haml-lang.org
  au! BufRead,BufNewFile *.haml set filetype haml

  " ledger-cli.org
  au BufNewFile,BufRead *.ldg,*.ledger setf ledger | comp ledger

  " Drupal *.module and *.install files.
  augroup drupal
    autocmd!
    autocmd BufRead,BufNewFile *.php set filetype=php
    autocmd BufRead,BufNewFile *.module set filetype=php
    autocmd BufRead,BufNewFile *.install set filetype=php
    autocmd BufRead,BufNewFile *.theme.inc set filetype=php
    autocmd BufRead,BufNewFile *.theme set filetype=php
    autocmd BufRead,BufNewFile *.admin.inc set filetype=php
    autocmd BufRead,BufNewFile *.test set filetype=php
  augroup END
endif



"
" Keymappings for Tools & Plugins
""""""""""""""""""""""""""""""""""""""""""""""

nnoremap <Leader>r  <Cmd>MRU<Enter>
" [v]ariables in this file; LSP-less logic
nnoremap <Leader>v  <Cmd>TlistToggle<Enter>
" [w]riting mode: binding for Goyo plugin
nnoremap <Leader>w <Cmd>Goyo<Enter>



"
" Settings for Tools & Plugins
""""""""""""""""""""""""""""""""""""""""""""""

" per output from `opam install otp-indent`
set runtimepath^=~/.opam/system/share/ocp-indent/vim

" Neovim/modern lua: MRU is replaced by ':Telescope oldfiles'
"let MRU_File = "$HOME/.vim/plugin/mru.vim"
let MRU_Exclude_Files = '$HOME/tmp/.*'
let MRU_Auto_Close = 0
let MRU_Max_Entries = 10

" gundo requires to be _told_ to utilize python3, per:
" https://github.com/sjl/gundo.vim/blob/c5efef192b975b8e/doc/gundo.txt#L239-L243
let g:gundo_prefer_python3 = 1

" syntastic:
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 1
let g:syntastic_auto_loc_list = 1
"let g:syntastic_c_checkers = ['clang-3.6']
let g:syntastic_cpp_checkers = ['clang-check-3.6']
let g:syntastic_cpp_compiler = 'clang-3.6'
let g:syntastic_cpp_compiler_options = '-std=c++11 '
let g:syntastic_css_checkers = ['prettycss']
let g:syntastic_html_checkers = ['jshint']
let g:syntastic_javascript_checkers = ['gjslint']
let g:syntastic_java_checkers = ['javac']
let g:syntastic_java_javac_config_file_enabled = 1
let g:syntastic_debug_file = '~/usr/log/syntastic.log'

" disable default mappings; we only use nerdcomment for our Append script under
" ~/.vim/init-compat.vim codebase.
let g:NERDCreateDefaultMappings = 0

" disable syntastic for bats, but not syntax; *sort* of per:
"    https://groups.google.com/d/msg/vim-syntastic/D80n65Fgj1w/y5OIJUQWY4wJ
let g:syntastic_mode_map = {'mode': 'active', 'passive_filetypes': ['bats', 'java', 'typescript']}

" xdebug
let g:debuggerPort = 9000

" TODO delete drupal stuff
" TODO factor out the most common stuff to a "$HOSTFOREST_ROOT"/common/.vimrc and have each ost source that instead.

let php_sql_query = 1
let php_baselib = 1
let php_htmlInStrings = 1
let php_oldStyle = 1
let php_parent_error_close = 1
let php_parent_error_open = 1
let php_folding = 1
let php_sync_method = 0

" TODO figure out how to conditionally load vim-go; specifically:
"if has('nvim')
"  TODO manually load github.com/ray-x/go.nvim
"else
"  TODO manually load vimgo here (rather than rely on "start/" pack dir it lives now
"endif
"
" golang: fix imports *as well* as format, per:
"   https://github.com/fatih/vim-go/issues/207
let g:go_fmt_command = "goimports"

" per https://github.com/rust-lang/rust.vim#formatting-with-rustfmt
let g:rustfmt_autosave = 1

" set to 0 to only enable manually later via :RainbowToggle
let g:rainbow_active = 1

" enable syntax highlighting for markdown's codeblocks
" found at https://github.com/tpope/vim-markdown
" (at 9d87cc19179496db50fb6d902987737c4b522e38)
let g:markdown_fenced_languages = ['html', 'css', 'python', 'bash=sh', 'java', 'go', 'rust']

" See 'Vim "Packages" - Successor to vim-plugin' in vendor/README.md of this repo (at gitlab.com/jzacsh/dotfiles)
set runtimepath^=~/.vim runtimepath+=~/.vim/after
let &packpath = &runtimepath
packloadall

lua <<EOF
require('telescope').setup {
  pickers = {
    find_files = {
      -- Shockingly default is to hide files starting with a '.'
      hidden = true,
    }
  },
}
EOF
