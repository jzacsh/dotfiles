#!/usr/bin/env bash
# ~/.bash_logout: executed by bash(1) when login shell exits.

if [[ "$SHLVL" -eq 1 ]]; then
  clear # when leaving the console clear the screen to increase privacy

  # It might be empty, if we're in a pristine shell (say we skipped most of our
  # bashrc for debugging purposes.
  if [[ -n "$HOSTFOREST_ROOT" ]]; then
    source "$HOSTFOREST_ROOT"/common/hostforest
    hostforest_clear_cache # Clear out dotfile runtime indicators
  fi
fi
