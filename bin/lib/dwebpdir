#!/usr/bin/env bash
#
# Calls dwebp in a loop over all files in a directory, dumping everything safely
# into a new location. Mostly useful for dumping files to a USB thumb drive
# before running off to kinkos or some-such.

# shellcheck disable=SC1091
source yabashlib || {
  printf 'failed to load yabashlib\n\n' >&2
  exit 1
}

###############################################################################
# Common Bash~esque Lib #######################################################
self="$(basename "$(readlink -f "${BASH_SOURCE[0]}")")"
setLogPrefixTo "$self"

declare -r acceptedOpts='ho:i:'
function usage() {
  cat  <<EOF_HELP
$self: Turn webp files in DIR into more generally-friendly jpegs.

usage: $self -i DIR [-o OUT_DIR]
usage: $self [$acceptedOpts]

Roughly runs dwebp in a loop over all direct .webp children under DIR, dumping everything safely

If -o isn't specified then a tempdir is generated and printed for reference.
EOF_HELP
}

function usagef() {
  local fmt="$1"; shift
  logfFatal \
    'usage error (see -h for help): '"$fmt" \
    "$@"
}

###############################################################################
# General Setup (arg parsing, etc) ############################################

# TODO put this into yabashlib; I write this way too often
# $1=slug/program name
function mktempd() {
  local slug="$1" tmpl
  tmpl="$(date "+${slug}_%FT%s_.d.XXXXXXXXX")" || return 1
  mktemp --directory --tmpdir=  "$tmpl" || return 2
}

# $OPTARG will hold args for "opt" that had a colon after it; $OPTIND might also
# be useful; for more see bash man page:
# https://manpages.debian.org/bookworm/bash/bash.1.en.html#getopts
declare -A opts=( [e]=png )
while getopts "$acceptedOpts" actual_opt; do
  case "$actual_opt" in
    h) usage; exit;;
    i) opts["$actual_opt"]="$OPTARG";;
    ?) usagef 'bad arg passed, see err above\n';;
  esac
done
shift $(( OPTIND - 1 ))

function checkReadableDir() {
  [[ -d "$1" && -r "$1" ]] ||
    usagef \
      '%s must be a readable dir; got "%s"\n' \
      "$2" "$1"
}

strIsContentful "${opts[i]}" || usagef '-i DIR is required'
checkReadableDir "${opts[i]}" DIR

if strIsContentful "${opts[o]}"; then
  checkReadableDir "${opts[o]}" OUT_DIR
else
  opts[o]="$(mktempd "$self")" ||
    logfFatal 'failed generating tmpdir; out of space?\n'
fi

(( "$#" == 0 )) ||
  usagef 'got %d extra args: "%s"\n' "$#" "$*"

###############################################################################
# main: actuall do work we advertised #########################################

function listWebpFiles() {
  find "${opts[i]}" -mindepth 1 -maxdepth 1 -type f -name '*.webp' -print
}
[[ "$(listWebpFiles | wc -l)" -gt 0 ]] || usagef 'failed to find webp files\n'

# TODO move this into yabashlib
function hashOfStr() { echo -n "$1" | md5sum  | cut -f 1 -d  ' '; }

# $1="$input file"
# $2=outdir
function decodeToPng() {
  local webpFile="$1" outDir="$2" hash bname

  hash="$(hashOfStr "$(readlink -f "$webpFile")")" ||
    logfFatal 'failed to hash webpFile="%s"\n' "$webpFile"

  bname="$(basename "$webpFile")"
  outb="${hash}_${bname%.webp}.${opts[e]}"

  printf \
    '%s -> outb="%s"\t%s\n' \
    "$hash" "$outb" "$webpFile"
  dwebp "$webpFile" -o "$outDir"/"$outb"
}

declare okCount=0
declare -a failedFiles=()
while read -r webpFile; do
  if decodeToPng "$webpFile" "${opts[o]}"; then
    okCount=$(( okCount + 1 ))
  else
    failedFiles+=( "$webpFile" )
  fi
done < <(listWebpFiles)

convertStatus=all
if (( "${#failedFiles[@]}" )); then
  convertStatus=''
  logfError \
    'conversion FAILed for %d files\n' \
    "${#failedFiles[@]}" $okCount
  printf '\t%s\n' "${failedFiles[@]}" >&2
fi

logfInfo \
  'done: converted %s %d files successfully at:\n%s\n' \
  "$convertStatus" $okCount "${opts[o]}"
(( "${#failedFiles[@]}" == 0 )) || exit 1
