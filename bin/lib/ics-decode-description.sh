#!/usr/bin/env bash

# shellcheck disable=SC1091
source yabashlib || {
  printf 'failed to find yabashlib\n' >&2
  exit 1
}

declare -r icsFile="$1"
[[ -r "$icsFile" ]] || logfFatal 'require a readable ICS file as only arg\n'

# get function: decodeQuotedPrintable
source "$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")"/decode-quoted-printable.sh ||
  logfFatal 'failed to dependency: find sibling script for decoding: decode-quoted-printable.sh\n'

function print_encoded_description() {
  sed --silent --regexp-extended --expression \
    '/^DESCRIPTION\b/p' < "$1"
}

# header looks like:
#    DESCRIPTION;ENCODING=QUOTED-PRINTABLE:
function strip_ics_description_header() {
  # TODO why can't I directly match the colon (:) after the "QUOTED-PRINTABLE"?
  sed --regexp-extended --expression \
    's,^(DESCRIPTION;ENCODING=QUOTED-PRINTABLE.?)(=)\b,\2,g'
}

print_encoded_description "$icsFile" |
  strip_ics_description_header |
  decodeQuotedPrintable
