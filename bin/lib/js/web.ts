/**
 * Returns `content` string with the following first line removed:
 *   )]}'
 */
export function stripXsrfTokens(content: string): string {
  return content.replace(new RegExp(`^\\s*\\)]}'\\s*`), '');
}
