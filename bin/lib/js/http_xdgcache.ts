import { deno_RuntimeCacheUrl } from './http_fscache.ts';
import { assert } from './assert.ts';
const { env } = Deno;

export class deno_XdgRuntimeCacheUrl {
  private readonly runtimeCacheUrl: deno_RuntimeCacheUrl = new deno_RuntimeCacheUrl(
      assert(env.get('XDG_RUNTIME_DIR'), `failed getting $XDG_RUNTIME_DIR`),
      this.url,
      this.runtimePath,
      this.ttlMs);
  constructor(
    private readonly url: string,
    private readonly runtimePath: string,
    private readonly ttlMs: number,
  ) {}

  async fetch(): Promise<string> {
    return await this.runtimeCacheUrl.fetch();
  }
}
