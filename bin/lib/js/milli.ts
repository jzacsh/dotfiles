export const SEC_MS = 1000;
export const MIN_MS = 60 * SEC_MS;
export const HRS_MS = 60 * MIN_MS;
export const DAY_MS = 24 * HRS_MS;
