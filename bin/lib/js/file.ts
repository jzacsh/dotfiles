const { openSync } = Deno;

export function deno_isReadableDir(absPath: string): boolean {
  let isReadableDir = false;
  let f;
  try {
    f = openSync(absPath, {read: true});
    const stat = f.statSync();
    isReadableDir = stat.isDirectory;
  } finally {
    if (f) f.close();
  }
  return isReadableDir;
}
