export enum Comparison {
  A_Larger = -1,
  AB_Equal = 0,
  B_Larger = 1,
}

export function comparatorString(result: Comparison): string {
  switch(result) {
    case Comparison.A_Larger:
      return 'A larger';
    case Comparison.AB_Equal:
      return 'equal';
    case Comparison.B_Larger:
      return 'B larger';
  }
}

export function compareNumbers(a: number, b: number): Comparison {
  if (a === b) return Comparison.AB_Equal;
  return a < b ? Comparison.B_Larger : Comparison.A_Larger;
}
