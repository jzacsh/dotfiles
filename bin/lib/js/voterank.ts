import { assert, assertGet, assertDef, countDupes, getAnyKeyWithVal, UniqArray} from './assert.ts';
import { toPlainOldObject } from './debug.ts';

export type Candidate = string;

/* Canonical machine form: sortable numeric value of the rank. */
export type RankOrder = Positif;

/** String of a `RankOrder`. */
export type RankLabel = string;

/** A non-zero positive integer; aka a "counting number" */
// TODO: typescript-way to encode this `number`'s type-constraint?
export type Positif = number;

/** A Non-negative integer */
// TODO: typescript-way to encode this `number`'s type-constraint?
export type Integer = number;

/* A float greater than zero, less than one.  */
// TODO: typescript-way to encode this `number`'s type-constraint?
export type KnobZeroOne = number;

export interface Rank {
  // Human-readable stringification of the rank.
  label: RankLabel,

  value: RankOrder,

  // The scoring multiplier used to value this rank above others. Only sensible
  // if all ranks are known.
  weight: Positif,
}

/* A single voter's casted votes, showing how they rank each candidate. */
export type UserBallot = { [keyof: Candidate]: RankLabel };

export interface Ballot {
  userBallot: UserBallot;

  // Whether an attempt was made to count this ballot (even if not used,eg: due
  // to isMalformed).
  isAccountedFor: boolean;

  isMalformed: boolean;
};

/** The non-negative integer of votes a candidate got for each ranking. */
export type CandidateVotes = Map<RankLabel, Integer>;

export interface Tally {
  candidate: Candidate;

  votes: CandidateVotes;
}

export interface TallySummary {

  // All fields below are duplicated from other sources, and so not canonical.
  // They're placed here purely for transparency as this struct iself is
  // intended as the final stage (isnpection) after all work has been done.

  // Length of `candidates` (and of `ranks`).
  candidateCount: Integer;

  ranks: Map<RankLabel, Rank>;

  // Number of ballots encountered (including `badBallotCount`).
  voterCount: Integer;

  // Length of `badBallots`.
  badBallotCount: Integer;

  badBallots: Array<Ballot>;
}

export interface VoteAdmin {
  candidates: Array<Candidate>;
  votes: Array<Ballot>;
  badBallots: Array<Ballot>;
  rankings: BallotCounter;
};

export class VoteMachine implements VoteAdmin {
  public badBallots: Array<Ballot> = [];
  public rankings: BallotCounter;
  public votes: Array<Ballot>;

  constructor(
      public candidates: Array<Candidate>,
      votes: Array<UserBallot>) {
    assert(
        countDupes(candidates) == 0,
        `usage: called VoteMachine with ${countDupes(candidates)} duplicate candidates`);
    this.votes = votes.map(userBallot => {
      return {
        isAccountedFor: false,
        isMalformed: false,
        userBallot,
      };
    });
    this.rankings = new BallotCounter(this.candidates);
  }

  public tallyVotes(): Array<BallotCheck> {
    return this.votes.map((v, idx) => this.tallyVotesForBallot(v, idx));
  }

  /**
   * Mutates vote-machine by updating internal tally of candidates votes, given
   * the results of this single ballot
   */
  private tallyVotesForBallot(ballot: Ballot, voteIndex: number): BallotCheck {
    const check = this.rankings.validateBallot(ballot.userBallot);
    ballot.isAccountedFor = true;
    ballot.isMalformed = check.state !== BallotCheckState.Ok;
    if (ballot.isMalformed) {
      this.badBallots.push(ballot);
      return check;
    }

    for (let candidate in ballot.userBallot) {
      const candidateVotes = assert(
          this.rankings.tally.get(candidate),
          `bug! despite prior pre-validations: unrecognized candidate "${
              candidate}" of record #${
              voteIndex}`);

      // The rank into which this ballot casts this candidate
      const castRank = ballot.userBallot[candidate];

      assert(
          candidateVotes.votes.has(castRank),
          `bug! despite prior pre-validations: zero-vote was not set for candidate "${
              candidate}" on rank "${
              castRank}"`);
      // How to avoid this cast? just hide the assret and the cast below in a helper?
      candidateVotes.votes.set(castRank, (candidateVotes.votes.get(castRank) as number) + 1);
    }
    return check;
  }

  private buildVoteSummary(): TallySummary {
    return {
      candidateCount: this.candidates.length,
      ranks: this.rankings.ranks,
      // Number of ballots encountered (including `badBallotCount`).
      voterCount: this.votes.length,
      badBallotCount: this.badBallots.length,
      badBallots: this.badBallots,
    };
  }

  // See https://www.rcvresources.org/types-of-rcv options
  public findWinnerRunoff(sufficientWin: KnobZeroOne = 0.5): RunoffResults {
    assert(
        sufficientWin > 0 && sufficientWin < 1,
        `sufficientWin: expect knob in (0,1), but got "${sufficientWin}"`);

    let winner: Candidate|undefined = undefined;
    let runoffsCount = 0;
    let currentRank: RankOrder = 1;
    let topRankCandidates: Array<RunoffRank> =
      [...this.rankings.tally.values()].map(t => {
        return {
          candidate: t.candidate,
          voteCount: assertGet(
              currentRank.toString(),
              t.votes,
              `bug: missing rank from vote-tally of candidate "${t.candidate}"`),
        };
      });
    const runoffs: Array<RunoffSummary> = [{
      rankings: topRankCandidates,
      votersNextChoice: 0,
      newlyBumped: [],
      prevRunBumped: [],
      prevNotQuiteWinners: [],
      prevNotQuiteWinningVoteCount: 0,
    }];

    const droppedCandidates: Set<Candidate> = new Set();
    // TODO: is there a deepClone() standard now? or just keep this here?
    const votesQueue: Array<Ballot> = JSON.parse(JSON.stringify(this.votes));
    // TODO: or is currentRank being <= maxRank the most sensible while condition?
    while (droppedCandidates.size < this.candidates.length) {
      topRankCandidates.sort((a, b) => b.voteCount - a.voteCount);
      runoffs[runoffs.length - 1].rankings.sort((a, b) => b.voteCount - a.voteCount);
      const winningVoteCount = topRankCandidates[0].voteCount;
      if (winningVoteCount / this.votes.length > sufficientWin) {
        winner = topRankCandidates[0].candidate;
        break;
      }
      runoffsCount++;
      const currentLeads = new Map(topRankCandidates.map(c => [c.candidate, c.voteCount]));
      console.error(`runoffsCount: ${runoffsCount}: currentLeads: ${JSON.stringify(toPlainOldObject(currentLeads))}`);

      // TODO: there _could_ be multiple candidates with the same voteCount at
      // the tail of this array; what should happen then?
      const lowestCandidate = topRankCandidates[topRankCandidates.length - 1].candidate;
      currentLeads.delete(lowestCandidate);
      droppedCandidates.add(lowestCandidate);
      const prevNotQuiteWinners = topRankCandidates.
            filter(c => c.voteCount === winningVoteCount).
            map(c => c.candidate);
      // Non-unique list of candidates that were bumped up out of voters' ballots.
      // TODO: mutate votesQueue so we're working on its own shifting/reallocation of votes.
      const newlyBumpedCandidates: Array<Candidate> = votesQueue.
          filter(b => !b.isMalformed).
          filter(b => assertDef(
                b.userBallot[lowestCandidate],
                `welformed ballot missing candidate "${lowestCandidate}"`
              ) === currentRank.toString()).
          map(b => getAnyKeyWithVal(
              b.userBallot,
              (currentRank + 1).toString(),
              `bug: welformed ballot missing vote for an expected ranking`));
      if (!newlyBumpedCandidates.length) {
        break;
      }
      const bumpedCandidates: Map<Candidate, Positif> =
          newlyBumpedCandidates.reduce((m, candidate) => {
            const prev = m.get(candidate) || 0;
            m.set(candidate, prev + 1);
            return m;
          }, new Map());

      for (let i = 0; i < newlyBumpedCandidates.length; i++) {
        const newlyBumpedCandidate = newlyBumpedCandidates[i];
        const prevVote = currentLeads.get(newlyBumpedCandidate) || 0;
        currentLeads.set(newlyBumpedCandidate, prevVote + 1);;
      }
      topRankCandidates = [...currentLeads.entries()].map(entry => {
        const [candidate, voteCount] = entry;
        return {candidate, voteCount};
      });

      currentRank++;
      runoffs.unshift({
        newlyBumped: [...bumpedCandidates.entries()].map(entry => {
          const [candidate, voteCount] = entry;
          return {candidate, voteCount};
        }),
        rankings: topRankCandidates,
        votersNextChoice: newlyBumpedCandidates.length,
        prevRunBumped: [lowestCandidate],
        prevNotQuiteWinningVoteCount: winningVoteCount,
        prevNotQuiteWinners,
      });
    }
    return {
      winner,
      minSurpassPercentage: sufficientWin,
      runoffs,
      runoffsCount,
      voteSummary: this.buildVoteSummary(),
    };
  }

  /**
   * Returns tally results showing a ranking of candidates
   */
  public findWinByWeight(): WeightedScoreResults {
    const weightSortedCandidates = [...this.rankings.tally.values()].map(t => {
      const score = [...t.votes.entries()].reduce((score, voteEntry, _idx, _arr) => {
        const [rankLabel, voteCount] = voteEntry;
        const weight = this.rankings.lookupWeight(rankLabel);
        return score + (voteCount * weight);
      }, 0);
      return {
        ...t,
        score,
      };
    });

    weightSortedCandidates.sort((a, b) => b.score - a.score);

    return {
      candidates: weightSortedCandidates,

      voteSummary: this.buildVoteSummary(),
    };
  }
}

export interface RunoffRank {
  // Number of voters that puts a given candidate into this ranking.
  voteCount: Positif;
  // Which candidates fall into this ephemeral ranking.
  candidate: Candidate;
}

export interface RunoffSummary {
  // Rankings
  rankings: Array<RunoffRank>;

  // Candidates that got bumped up as "next-choice" due to this run-off.
  //
  // NOTE: Will be zero iff this particular RunoffSummary is of the _very
  // first_ runoff.
  newlyBumped: Array<RunoffRank>;

  // How many voters had to have their previous runoff's top-choice
  // discarded.
  //
  // NOTE: Will be zero iff this particular RunoffSummary is of the _very
  // first_ runoff and it required _zero_ additional runoffs (ie: a rare
  // event where there was a winner right away).
  votersNextChoice: Integer;

  // Which candidates from the previous runoff were dropped for this one.
  //
  // NOTE: will be an empty list iff this is the RunoffSummary of the _very
  // first_ analysis.
  prevRunBumped: Array<Candidate>;

  // Which candidates from the previous runoff were top contenders.
  //
  // NOTE: will be an empty list iff this is the RunoffSummary of the _very
  // first_ analysis.
  prevNotQuiteWinners: Array<Candidate>;

  prevNotQuiteWinningVoteCount: Positif;
}

// Describes a summary of all votes, that expresses a winner, using Ranked
// Choice Voting (RCV).
export interface RunoffResults {
  winner?: Candidate;

  // Minimum percentage above which a candidate needs to have the total votes,
  // in order for winner to be declared. Eg: if '0.5', then a winning candidate
  // won because they achieved something higher like 0.500001 of the votes.
  minSurpassPercentage: KnobZeroOne;

  // Runoffs including the winning one, in reverse order (index=0 is the most
  // recent/winning run and the final entry is the oldest/first run).
  //
  // NOTE: will always be >=1, as the first vote (though not a runoff) will
  // still have relevant stats here.
  runoffs: Array<RunoffSummary>;

  runoffsCount: Integer;

  voteSummary: TallySummary;
}

export interface WeightedScoreResult {
  candidate: Candidate;

  // All fields below are duplicated from other sources, and so not canonical.
  // They're placed here purely for transparency as this struct iself is
  // intended as the final stage (isnpection) after all work has been done.

  score: Positif;

  // Underlying
  votes: CandidateVotes;
}

// Describes a summary of all votes, that expresses a winner.
export interface WeightedScoreResults {
  candidates: Array<WeightedScoreResult>;

  voteSummary: TallySummary;
}

export interface BallotCheck {
  state: BallotCheckState;

  // Any arbitrary content about which we're complaining. 
  subject?: string;
}

export enum BallotCheckState {
  // This is a valid vote, nothing is malformed.
  Ok = 0,
  InvalidCandidate,
  InvalidRank,
  DuplicateCandidate,
  DuplicateRank,
}

class BallotCounter {
  private rankValidator: Set<RankLabel>;
  private candidateValidator: Set<Candidate>;

  public ranks: Map<RankLabel, Rank>;
  public tally: Map<Candidate, Tally>;

  constructor(candidates: UniqArray<string>) {
    this.candidateValidator = new Set(candidates);

    const validRanks: UniqArray<RankLabel> = Array
      .from(new Array(candidates.length))
      .map((_, i) => (i + 1).toString());
    this.ranks = new Map(
        validRanks.
        map((label, i) => {
          const value = i + 1;
          return {
            label,
            value,
            weight: validRanks.length - i,
          };
        }).
        map(r => [r.label, r])
    );
    this.rankValidator = new Set(validRanks);

    this.tally = new Map(
        candidates.
        map(candidate => {
          return {
            candidate,
            votes: BallotCounter.buildNewVoteCounter(validRanks),
          };
        }).
        map(tally => [tally.candidate, tally])
    );
  }

  public lookupWeight(label: RankLabel): Positif {
    const rankDef = assert(
        this.ranks.get(label),
        `weight lookup on missing RankLabel="${label}"`);
    return rankDef.weight;
  }

  public validateBallot(b: UserBallot): BallotCheck {
    const seenCandidates: Set<Candidate> = new Set();
    const seenRanks: Set<RankLabel> = new Set();
    for (let candidate in b) {
      if (!this.isValidCandidate(candidate)) {
        return { subject: candidate, state: BallotCheckState.InvalidCandidate };
      }
      if (!this.isValidRank(b[candidate])) {
        return { subject: b[candidate], state: BallotCheckState.InvalidRank };
      }

      if (seenCandidates.has(candidate)) {
        return { subject: candidate, state: BallotCheckState.DuplicateCandidate };
      }
      if (seenRanks.has(b[candidate])) {
        return { subject: b[candidate], state: BallotCheckState.DuplicateRank };
      }

      seenCandidates.add(candidate);
      seenRanks.add(b[candidate]);
    }
    return { state: BallotCheckState.Ok };
  }

  /* Whether `rank` is of type RankLabel. */
  public isValidRank(rank: string): boolean {
    return this.rankValidator.has(rank);
  }

  /* Whether `cand` is of type Candidate. */
  public isValidCandidate(cand: string): boolean {
    return this.candidateValidator.has(cand);
  }

  private static buildNewVoteCounter(validRanks: Array<RankLabel>): CandidateVotes {
    const zeroVotes = new Map();
    for (let i = 0; i < validRanks.length; i++) {
      zeroVotes.set(validRanks[i], 0 /*zero votes*/);
    }
    return zeroVotes;
  }
}
