import { assert } from './assert.ts';
import { deno_isReadableDir } from './file.ts';

const { lstatSync, errors, readTextFileSync, writeTextFileSync } = Deno;

/**
 * Fetches from a URL unless we're within TTL of /var/run/user/$UID/.. cache
 * file, in which case local file is used.
 */
export class deno_RuntimeCacheUrl {
  private readonly cachePath: string;
  /**
   * @param url remote URL for JSON data
   * @param runtimePath file path, relative to /var/run/user/$UID/, where data
   *    should be cached.
   */
  constructor(
    rootDirPath: string,
    private readonly url: string,
    runtimePath: string,
    private readonly ttlMs: number,
  ) {
    assert(deno_isReadableDir(rootDirPath), 'failed ensuring $XDG_RUNTIME_DIR is available');
    this.cachePath = `${rootDirPath}/${runtimePath}`;
  }

  /** Content of cache, or undefined if too old. */
  private async getFreshCache(): Promise<string|undefined> {
    // Fail early if the file looks old, empty, or we can't access it
    try {
      const stats = lstatSync(this.cachePath);
      if (!stats.mtime) return undefined;
      const diffMs = Date.now() - stats.mtime.getTime();
      if (stats.size <= 1 || diffMs > this.ttlMs) return undefined;
    } catch (error) {
      assert(
        error instanceof errors.NotFound,
        `unexpected error reading runtime file (at ${this.cachePath}): ${error}`);
      return undefined;
    }

    try {
      return readTextFileSync(this.cachePath);
    } catch {
    }
    return undefined;
  }

  async fetch(): Promise<string> {
    const cache = await this.getFreshCache();
    if (cache !== undefined) return cache;

    const content = await fetch(this.url).then(resp => resp.text());
    writeTextFileSync(this.cachePath, content);
    return content;
  }
}
