#!/usr/bin/env bash

# TODO(zacsh) delete __dotforest_isbroken_completion usages when
# yblib.haveCompletion's TODO (wrt better heuristics) is completed
function __dotforest_isbroken_completion() {
  local prog_name="$1"
  # os-name scraping copied from yabashlib unixy.sh script:
  local osprog='$1 == "ID" {print tolower($2)}' # awk script
  local os; os="$(awk -F'=' "$osprog" /etc/os-release)"
  [[ "$os" = nixos && "$prog_name" = gradle ]]
}

function __dotforest_run_custom_completion() {
  local completion="$1"
  local target_base target_program official_completion msg_prefix
  target_base="$(basename "$completion")"
  target_program="${target_base/-completion.sh/}"
  official_completion=/usr/share/bash-completion/completions/"$target_program"

  printf -v msg_prefix -- \
    'foregoing custom bash completion of "%s"' \
    "$target_program"

  # Warn myself so that if this happens on _all_ hosts I know I should maybe
  # just delete the old crappy version I have from before this source came
  # with the package, *OR* if mine is truly custom and intentionally so, then
  # I need to simply rename my own copy
  # (eg "zacshforest-$target_program-completion.sh").
  if __dotforest_isbroken_completion "$target_program"; then
    dotforest_log warn \
      '%s: hardcoded bypass\n' \
      "$msg_prefix"
  elif yblib.haveCompletion "$target_program"; then
    dotforest_log warn \
      '%s: apparently already present completion\n' \
      "$msg_prefix"
  elif [[ -s "$official_completion" ]]; then
    dotforest_log warn \
      '%s: using system-provided one instead\n' \
      "$msg_prefix"
    source "$official_completion"
  else
    source "$completion"
  fi
}

# TODO(zacsh) nixos: figure out how to find the system-wide installed versions
# (or maybe move this entire block/for-loop into hostforest where it can run
# when *not* nixos).
for completion in ~/.config/bash_completion.d/*.sh; do
  __dotforest_run_custom_completion "$completion"
done
