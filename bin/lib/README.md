# Lib for non `$PATH` Logic

Sources and scripts in this directory are _like_ things that I used to keep in
my bin but they're strategic helpers I don't want to have to write again but are
only rarely useful. EG: things I will think of when I'm writing a longer or more
complicated script, but I don't need it muddying my autocomplete day-to-day in
my CLI.

## Usage

You can use `dotforest_extend_path` bash func to extend your `$PATH` to include
this directory. This function is stored in `../path-setup.sh` but should already
be in your environment if your bash login is setup correctly.
