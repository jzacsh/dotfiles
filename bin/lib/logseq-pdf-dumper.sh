#!/usr/bin/env bash
#
# Clones all PDFs out of logseq to a temp directory with SHAs for easy reference
# later. Intended for dumping to eink readers/tablets that are more difficult to
# connect to my logseq ecosystem.

outd="$(mktemp --tmpdir= --directory "$(date +'%F_at-%s')_"PDF-Dump_XXXXXXXXXXXX)" || {
  printf 'failed making dump location\n' >&2
  exit 1
}
while read -r f; do
  sha="$(echo "$f" | shasum  | cut -f 1 -d ' ')" || {
    printf 'WARNING: skipping file; failed getting checksum: %s\n' "$f" >&2
    continue
  }
  b="$(basename "${f/.pdf/}")_dumpedPDF-${sha}.pdf" || {
    printf 'WARNING: skipping file; failed building new PDF name: %s\n' "$f" >&2
    continue
  }
  # printf '%s for\n\t%s\n' "$b" "$f"

  cp -v "$f" "$outd"/"$b"
done < <(
  find \
    ~/sync/dweb/syncthing/content/default/logseq/assets \
    -type f \
    -name '*.pdf'
)

printf 'DONE; all PDFs in:\n\t%s\n' "$outd"
