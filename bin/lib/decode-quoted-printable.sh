#!/usr/bin/env bash

# Decodes "Quoted-Printable"[1] binary-to-text encoding from stdin, often used
# in ICS content, using a core Perl module per https://superuser.com/a/1549021
#
# [1]: https://en.wikipedia.org/wiki/Quoted-printable
function decodeQuotedPrintable() { perl -MMIME::QuotedPrint -0777 -nle 'print decode_qp($_)'; }

# If we were invoked as a script (rather than source).
if [[ "${BASH_SOURCE[0]}" = "$0" ]]; then
  if [[ $# -gt 0 ]]; then
    printf 'usage: %s; pass encoded string from STDIN!\n' \
      "$(basename "$0")" >&2
    exit 1
  fi
  decodeQuotedPrintable
fi
