/**
 * jzacsh@ forked 2023-09-13 from https://gist.github.com/russelldavies/5745528
 *
 * # Instructions
 *
 * Run this code in hterm's JavaScript console.
 *
 * ## Setup: Usage of code in this file
 *
 * - 1) chrome://extensions -> click hterm
 * - 2) under "inspect views" click html/nassh.html
 * - 3) you should see a devtools window now
 * - 4) the js console in said devtools should have `term_` variable available
 * - 5) now paste the below code into said console
 * - 6) done!
 *
 * ## UNDOing work in this file
 *
 * Only do the above setup; you can also *UNDO* your changes to get back to
 * default via: steps 1-4 and then running `term_.prefs.resetAll()`
 *
 * ## Bonus: NerdFonts with hterm
 *
 * per https://mshaugh.github.io/nerdfont-webfonts visit hterm's "options" UI
 * and...
 * 1. under "Custom CSS (URI)" put https://mshaugh.github.io/nerdfont-webfonts/build/firacode-nerd-font.css
 * 2. under "Custom CSS (inline text)"
 *     x-row {
 *        text-rendering: optimizeLegibility;
 *        font-variant-ligatures: normal;
 *      }
 * 3. under "Text font family" prefix the extant
 *    value with `"FiraCode Nerd Font",`
 */

class HtermPrefProfile {
  /* @param {!Object<string, string>} preferences */
  constructor(profileName, preferences) {
    this.profileName = profileName;
    this.prefs = preferences;
  }

  static setPref(htermPrefMgr, prefName, pref) {
    try {
      htermPrefMgr.set(prefName, pref);
    } catch (htermPrefMgrException) {
      throw new Error(
        `buggy pref: "${prefName}" -> "${
          JSON.stringify(pref)}"\n\thterm API reports: ${
          htermPrefMgrException.message}`,
        {case: htermPrefMgrException});
    }
  }

  _setPref(htermPrefMgr, prefName, preference) {
    try {
      HtermPrefProfile.setPref(htermPrefMgr, prefName, preference);
    } catch (e) {
      throw new Error(`[profile:${this.profileName}]: ${e.message}`, {case: e});
    }
  }

  setAsPreferred(htermTerm) {
    htermTerm.setProfile(this.profileName); // TODO what does this do?
    htermTerm.prefs_.resetAll();
    for (const [prefName, preference] of Object.entries(this.prefs)) {
      this._setPref(htermTerm.prefs_, prefName, preference);
    }
  }

  extendWith(profileName, newPrefs) {
    return new HtermPrefProfile(
      profileName,
      Object.assign(HtermPrefProfile._deepClone(this.prefs), newPrefs));
  }

  static _deepClone(item) {
    return JSON.parse(JSON.stringify(item));
  }
}

let SOLARIZED_BASE = new HtermPrefProfile('_solarized', {
  'color-palette-overrides': [
    '#073642', '#dc322f', '#859900', '#b58900', '#268bd2', '#d33682', '#2aa198',
    '#eee8d5', '#002b36', '#cb4b16', '#586e75', '#657b83', '#839496', '#6c71c4',
    '#93a1a1', '#fdf6e3',
  ],
  'font-size': 9,
  'enable-bold': false,
  // 'font-family': 'Menlo',
  'font-smoothing': 'subpixel-antialiased',
});
let SOLARIZED_LIGHT = SOLARIZED_BASE.extendWith('solarized-light', {
  'background-color': '#fdf6e3',
  'foreground-color': '#657b83',
  'cursor-color': 'rgba(101, 123, 131, 0.5)',
});
let SOLARIZED_DARK = SOLARIZED_BASE.extendWith('solarized-dark', {
  'background-color': '#002b36',
  'foreground-color': '#839496',
  'cursor-color': 'rgba(131, 148, 150, 0.5)',
});

SOLARIZED_DARK.setAsPreferred(term_);
