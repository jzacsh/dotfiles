#!/usr/bin/env bash
#
# Rewrites $1 to be relative to PWD (or $2 if provided). If $1 is not contained
# in $2, an very long and ugly relative path will still be generated.

# TODO move this script to yabashlib in file or string folders
source yabashlib || {
  printf 'ERROR: failed to find yabashlib in $PATH\n' >&2
  exit 1
}
#setMaxLogLevelToDebug

# TODO: support stdin via a CLI API of "-" as far arg; eg:
#    cd /a/c
#    find /a/b -type d | relative-path -
#    find /a/b -type d | relative-path - /a/c
[[ "$#" -ge 1 ]] || logfFatal 'usage: PATH [RELATIVE_TO]\n'
declare -r target="$1"
declare -r context="${2:-$PWD}"

isImplicitContext=1; (( $# == 1 )) || isImplicitContext=0

logfDebug 'target vs. context:\n\t%s\n\t%s\n' "$target" "$context"

function stripRightPathPart() { sed --regexp-extended --expression 's,/[^/]*$,,'; }

# Using the delimeter "$1" joins all remaining _non empty_ arguments.
# TODO move this to yabashlib string folder
function strJoinNonempty() {
  local delimeter="$1" usedDelim; shift
  local non_empty_args; non_empty_args=()
  for arg in "$@";do
    [[ -n "$arg" ]] || continue
    non_empty_args+=("$arg")
  done

  local i=0
  local len_args; len_args="${#non_empty_args[@]}"
  for arg in "${non_empty_args[@]}"; do
    if (( i < len_args - 1 )); then
      usedDelim="$delimeter"
    else
      usedDelim=''
    fi
    printf '%s%s' "$arg" "$usedDelim"
    i=$(( i + 1 ))
  done
}

parentPath=''
parentHeight=0
commonPortion="$context"
if ! strStartsWith "$target" "$commonPortion"; then
  while [[ -n "$commonPortion" ]]; do
    commonPortion="$(echo "$commonPortion" | stripRightPathPart)"
    parentHeight=$(( parentHeight + 1 ))
    parentPath="$parentPath"'../'
    if strStartsWith "$target" "$commonPortion"; then
      break
    fi
  done
fi
strStartsWith "$context" "$commonPortion" ||
  logfFatal 'not yet implemented: relative path all the way up to root\n'

relativeDescentToTarget="$(strTrimLeft "$target" "$commonPortion")"

logfDebug \
  'write relative jump (%d dirs up) up to\n%s\n%s\nand then stick the remainder back on\n' \
  $parentHeight "$parentPath" "$commonPortion"

contextPrefix=''; (( isImplicitContext )) || contextPrefix="$context"

relativePath="$(
  # Careful not to trimthe root "/" out of the contextPrefix
  strJoinNonempty / \
    "$(strTrimRight "${contextPrefix:-}" /)" \
    "$(strTrim "$parentPath" /)" \
    "$(strTrim "$relativeDescentToTarget" /)"
)"

logfDebug \
  'context vs. target vs. relative path vs. readlink -f output vs.  file:\n-\t%s\n-\t%s\n-\t%s\n-\t%s\n-\t%s\n' \
  "$context" \
  "$target" \
  "$relativePath" \
  "$(readlink -f "$relativePath" 2>&1)" \
  "$(file "$relativePath" 2>&1)"

printf -- '%s\n' "$relativePath"
