#!/usr/bin/env bash
#
# hunts recursively for git repos and than makes absolutely sure they really
# are.
#
###############################################################################
# Common Bash~esque Lib #######################################################
self="$(basename "$(readlink -f "${BASH_SOURCE[0]}")")"

# like printf, but prefixes current script's basename for easier log
# identification
function _logf() {
  local given_fmt="$1"; shift

  local fmt; printf -v fmt -- \
    '[%s]: %s' \
    "$self" \
    "$given_fmt"

  # shellcheck disable=SC2059
  printf -- "$fmt" "$@"
}

function logf() { _logf "$@"; }
function errf() { _logf "$@" >&2; }
function dief() { errf "$@"; exit 1; }

# taken from yabashlib:
function yblib.vcsq_is_in_git() (
  cd "$1" || dief 'checking repo we cannot enter: %s\n' "$1"
  git rev-parse --show-toplevel > /dev/null 2>&1
)

function assertHaveCallable() {
  local dep="$1"
  type -p "$dep" >/dev/null 2>&1 ||
    dief 'missing dependency: "%s"\n' "$dep"
}
###############################################################################
# General Setup (arg parsing, etc) ############################################

function listHeuristicallyGitRepoLookingDirs() {
  local searchRootDir="$1"
  local subRepoPath=/.git/config
  find \
      "$searchRootDir" \
      -mindepth 1 \
      -type f \
      -wholename '*'"$subRepoPath" \
      -print |
    sed \
      --regexp-extended \
      --expression \
      's,'"$subRepoPath"'$,,g'
}

function looksLikeGitRepo() {
  local gitRepoDir="$1"
  [[
    -d "$gitRepoDir/.git" &&
    -d "$gitRepoDir/.git/hooks" &&
    -f "$gitRepoDir/.git/config" &&
    -f "$gitRepoDir/.git/HEAD"
  ]] &&
    yblib.vcsq_is_in_git "$gitRepoDir"
}

function listGitRepos() {
  local rootDir="$1"
  while read -r repoPath; do
    looksLikeGitRepo "$repoPath" || continue
    printf '%s\n' "$repoPath"
  done < <(listHeuristicallyGitRepoLookingDirs "$rootDir")
}

###############################################################################
# main: actuall do work we advertised #########################################

assertHaveCallable 'git'
assertHaveCallable 'sed'

declare -r rootSearchDir="$1"
[[ -d "$rootSearchDir" && -r "$rootSearchDir" ]] ||
  dief 'usage: need readable dir for ROOT_DIR arg; got: "%s"\n' "$rootSearchDir"
listHeuristicallyGitRepoLookingDirs "$rootSearchDir" >/dev/null ||
  dief 'failed to find any repos in %s\n' "$rootSearchDir"

listGitRepos "$rootSearchDir" ||
  dief 'failed listing repos under: %s\n' "$rootSearchDir"
