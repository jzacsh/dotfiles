#!/usr/bin/env bash

declare -r flags_prodLog=1 # should be 1, otherwise spammy debug logs will print
declare -r flags_quiet=0 # TODO turn into CLI flag
declare -r flags_experimentTimezoneForced=1
declare -r dateFmtDateTool='%FT%H:%M:%S'
declare -r dateFmtExifTool='%Y%m%dT%H:%M:%S'
declare -r MAX_SECONDS_SEARCH_NUDGING=$(( 60 * 60 * 8 ))

# shellcheck disable=SC1091
source yabashlib || {
  printf 'failed to find dependency: yabashlib\n' >&2
  exit 1
}
(( flags_prodLog )) || setMaxLogLevelToDebug
declare -a CP_FLAGS=( )
(( flags_prodLog )) || CP_FLAGS+=( '-v' )

function date_isValidDate() { date --date="$1" >/dev/null; }

# WARNING: assumes date_isValidDate has passed already
function date_normalizeDateStr() { date --iso-8601=ns --date="$1"; }

# Between two dates, a=$1 and b=$2, is a greater than b?
# WARNING: assumes a and b are valid.
function date_isYoungerThan() {
  local a a_raw="$1" a_sec
  local b b_raw="$2" b_sec

  a="$(date_normalizeDateStr "$a_raw")"
  a_sec="$(date --date="$a" +%s)"

  b="$(date_normalizeDateStr "$b_raw")"
  b_sec="$(date --date="$b" +%s)"

  [[ "$a_sec" -gt "$b_sec" ]]
}

# Whether a=$1 is older than or the same age as b=$2
function date_isOlderThanOrEq() { ! date_isYoungerThan "$@"; }

function listInputFiles() { find "$dir" -mindepth 1 -maxdepth 1 -type f "$@" | sort; }

# target="$1" the date to produce
# WARNING: assumes date_isValidDate has passed already
# WARNING: you'll get confusing output if don't normalize your inputs to this
# function using date_normalizeDateStr
function dateStrFromRaw() { date +"$dateFmtDateTool" --date="$1"; }

# assumes file is named "YYYYMMDD_some-stuff-here.ext"
function getIntendedDateStr() {
  local bname="$1" dateStrRaw dateStr

  dateStrRaw="${bname/_*/}"
  strIsContentful "$dateStrRaw" || return 1
  [[ "$dateStrRaw" != "$bname" ]] || return 1

  # should be YYYYMMDD or more (eg: 'T13-06', etc)
  [[ "${#dateStrRaw}" -ge 8 ]] || return 1

  date_isValidDate "$dateStrRaw" || return 1

  dateStr="$(date_normalizeDateStr "$dateStrRaw")" || return 1

  dateStrFromRaw "$dateStr"
}

# $1=dateStr date to nudge
# $2=last_datestr (for logging purposes)
# $3=file for which we're doing this (for logging purposes)
function getNudgedDateStr() {
  local dateStr="$1" last_dateStr="$2" f="$3"
  local nudged nudgeSec=1 nudgedDateTarget

  # TODO something more elegant? we don't want to just do 1sec+previous because
  # we don't want to arbitrarily jump far; we want to know how far forward we're
  # jumping; we could check the diff and assert on that but this is fine too...
  while (( i < MAX_SECONDS_SEARCH_NUDGING )); do
    nudgeSec=$(( nudgeSec + 1 ))
    nudgedDateTarget="$(date_normalizeDateStr "$dateStr")" || return 1
    nudgedDateTarget="$nudgedDateTarget + $nudgeSec seconds" || return 1
    nudged="$(dateStrFromRaw "$nudgedDateTarget")" || return 1

    if date_isYoungerThan "$nudged" "$last_dateStr"; then
      break
    fi

    logfDebug \
      'nudge %3dsec of "%s" insufficient to bypass prev="%s"; continuing seearching...\n' \
      $nudgeSec "$nudged" "$last_dateStr" \
      >&2 # ensure we don't break the API of this func (uses stdout)
  done


  logfDebug \
    'nudged date forward a bit from "%s" to "%s" to bypass previous="%s" for file:\n\t"%s"\n' \
    "$dateStr" "$nudged" \
    "$last_dateStr" "$f" \
    >&2 # ensure we don't break the API of this func (uses stdout)

  date_isYoungerThan "$nudged" "$dateStr" || {
    logfError \
      'BUG: somehow did not nudge date forward (was "%s"; is "%s")\n' \
      "$dateStr" "$nudged"
    return 1
  }
  date_isYoungerThan "$nudged" "$last_dateStr" || {
    logfError \
      'BUG: somehow DID nudge date forward (from "%s"), but not past previous("%s") is now: "%s"\n' \
      "$dateStr" \
      "$last_dateStr" \
      "$nudged"
    return 1
  }

  echo "$nudged"
}

# -SubSecDateTimeOriginal expects a format like this:
#                         '20230805T00:00:08-05:00'
function getSubSecDateTimeFormat() {
  local target="$1"
  date \
    --date="$target" \
    '+%Y%m%dT%H:%M:%S%:z'
}

# $1=dateStr; $2=file
function overWriteExifDatesOfFile() {
  local dateStr="$1" f="$2"
  local subSecDateTimeFormat exifToolArgs=()

  # TODO finish this experiment
  # PROBLEM: we're trying to get Google Photos UI to behave well and respect the
  # timezone of the EXIF data, however we're finding it's a GMT-05:00 in GPhotos
  # UI;
  # https://photo.stackexchange.com/a/126978 suggests there's a long-standing
  # hack to put the time/date EXIF info into _GPS_ fields since Google Photos
  # uses that. However manual experiments aren't verifying that works either, so
  # keep searching.
  if (( flags_experimentTimezoneForced )); then
    subSecDateTimeFormat="$(getSubSecDateTimeFormat "$dateStr")" || {
        logfWarning 'BUG: given bad date by other code? building exiftool format from date="%s" failed\n' \
        "$dateStr"
        return 1
    }

    # NOTE: -SubSecDateTimeOriginal doesn't work if -d is used (despite being
    # set with timezone formatting)
    exifToolArgs+=( -SubSecDateTimeOriginal="$subSecDateTimeFormat" )
  else
    exifToolArgs+=( -d "$dateFmtExifTool" )
    exifToolArgs+=( -AllDates="$dateStr" )
  fi

  (
    (( flags_prodLog )) || set -x
    exiftool \
      -quiet \
      -overwrite_original_in_place \
      "${exifToolArgs[@]}" \
      "$f"
  )
}

# $1=dateStr; $2=file
function fixDatesOnFile() {
  local dateStr="$1" f="$2" subSecDateTimeFormat
  (( flags_quiet )) || logfInfo \
      'setting dates to "%s" for file "%s"\n' \
      "$dateStr" "$f"

  overWriteExifDatesOfFile "$dateStr" "$f" || {
    logfWarning 'FAILURE: failed setting exif dates to "%s" on "%s"\n' "$dateStr" "$f"
    return 1
  }
  (
    (( flags_prodLog )) || set -x
    touch -d "$dateStr" "$f"
  ) || {
    logfWarning 'failed setting filesystem timestamp "%s" on "%s"\n' "$dateStr" "$f"
    return 1
  }
}

function listBasenamesByExifDates() {
  # shellcheck disable=SC2016
  local printOutputFormatSpec='$DateTimeOriginal $FileName'
  exiftool \
    -quiet \
    -p "$printOutputFormatSpec" \
    -d "$dateFmtExifTool" "$1" |
  sort |
  cut -f 2- -d ' '
}

# no idea why the diff command uses stderr for advertising
function compareFiles() { diff "$1" "$2" 2>/dev/null; }

function compareFileOrdering() {
  compareFiles \
    <(listInputFiles -printf '%P\n') \
    <(listBasenamesByExifDates "$outd")
}
function assertSameFileOrdering() { compareFileOrdering >/dev/null; }

# main below this line ########################################################
###############################################################################

[[ "${BASH_SOURCE[0]}" = "$0" ]] || {
  logfError 'seems you sourced this script? returning early so you can debug...\n'
  return 1
}

[[ "$#" -eq 2 ]] || {
  printf 'usage: IMAGE_DIR EMPTY_OUT_DIR\n' >&2
  logfFatal 'missing arguments; see usage\n'
}

declare -r dir="$1"
strIsContentful "$dir" || logfFatal 'expected DIR to be a non-empty string\n'
{ [[ -r "$dir" ]] && [[ -d "$dir" ]]; } ||
  logfFatal 'arg DIR must be a readable directory; got: "%s"\n' "$dir"

outd="$(strTrimRight "$2" /)"; declare -r outd
strIsContentful "$outd" || logfFatal 'expected OUTD to be a non-empty string\n'
{ [[ -r "$outd" ]] && [[ -d "$outd" ]]; } ||
  logfFatal 'arg DIR must be a readable directory; got: "%s"\n' "$outd"
isDirectoryEmpty "$outd" || logfFatal 'OUTD must be an empty directory\n'


i=-1
last_dateStr=''
while read -r f; do
  i=$(( i + 1 ))
  logfDebug \
    'START processing file #%00d: "%s"\n' \
    $(( i + 1 )) "$f"

  bname="$(basename "$f")" || {
    logfWarning 'failed determining basename of file: "%s"\n' "$f"
    continue
  }
  outf="$outd"/"$bname"
  [[ ! -e "$outf" ]] || {
    logfWarning \
      'SKIPPING: would clobber file in target dir! from "%s" to: "%s"\n' \
      "$f" "$outf"
    continue
  }
  dateStr="$(getIntendedDateStr "$bname")" || {
    logfWarning 'SKIPPING: failed to determine date str for "%s"\n' "$f"
    continue
  }

  if (( i > 0 )); then
    # shellcheck disable=SC2016
    strIsContentful "$last_dateStr" || logfFatal 'bug! $last_dateStr lost :(\n'
    date_isOlderThanOrEq "$last_dateStr_orig" "$dateStr" || {
      logfWarning \
      'SKIPPING: bad date ordering: previous(%s) should be older than newly encountered file with stamp (%s):\n\t%s\n' \
        "$last_dateStr" \
        "$dateStr" \
        "$f"
      continue
    }

    # Ensure dates are not equal
    date_isYoungerThan "$dateStr" "$last_dateStr" || {
      dateStr="$(getNudgedDateStr "$dateStr" "$last_dateStr" "$f")" || {
        logfWarning \
          'SKIPPING: failed to increment date in order to bypass previous (%s): %s\n' \
          "$last_dateStr" \
          "$f"
        continue
      }
    }
  fi
  last_dateStr_orig="$(getIntendedDateStr "$bname")" || logfFatal \
    'BUG: somehow getIntendedDateStr is not idempotetnt for bname="%s"\n' \
    "$bname"
  last_dateStr="$dateStr"

  cp "${CP_FLAGS[@]}" "$f" "$outf"
  fixDatesOnFile "$dateStr" "$outf" || {
    # no warn log; fixDatesOnFile already logs heavily
    continue
  }
done < <(listInputFiles)


assertSameFileOrdering || {
  compareFileOrdering
  logfError \
    'failed to produce correct ordering of files by EXIF dates; see diff above (from FROM: original ordering you intended TO: sorted by EXIF date)\n'
}

logfInfo \
  'DONE modifying % 3d files; and verified overall-ordering was preserved in dates.\n' \
  $(( i + 1 ))
