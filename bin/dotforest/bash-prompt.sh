#!/usr/bin/env bash

function dotforest_setSimplePrompt() {
  PS1='\s^$RET \u@\h[\j] LVL=$SHLVL@\t \w\n\$ '
}
dotforest_setSimplePrompt

# Below this line nothing should run but function _definitions_.
###############################################################################

function dotforest_renderPromptVcs() { vcprompt; }
function dotforest_renderPromptIdentity() {
  echo "${col_usr}\u${col_end}@${col_host}\h${col_end}"
}

# Sets the PS1 prompt the way I prefer it.
#
# WARNING: the following render commands are expected to exist:
# - dotforest_renderPromptVcs; prints VCS data about $PWD
# - dotforest_renderPromptIdentity; prints info like user and host (eg: alice@cray).
#
# $1=any command to execute that will print a suffix for you, instead of '$'
function dotforest_setPrompts() {
  local col_end='\[\033[0m\]'
  local col_red='\[\e[1;31m\]'
  local col_grn='\[\e[0;32m\]'
  local col_grnB='\[\e[1;32m\]'
  local col_ylwB='\[\e[1;33m\]'
  local col_ylw='\[\e[0;33m\]'
  local col_blu='\[\e[1;34m\]'

  if yblib.hasColor; then
    function suffix_default() { echo -n "${col_ylw}\$${col_end}"; }
  else
    function suffix_default() { echo -n '$'; }
  fi
  if (( $# == 1 )) && [[ "$(type -t "${1}")" = function ]]; then
    local suffix_fn_name="$1"
    local suffix_fn="$suffix_fn_name"
  else
    local suffix_fn="suffix_default"
  fi
  PS1='\s^$RET  @\t $(dotforest_renderPromptVcs) \w\n\u@\h   $SHLVL: $('"$suffix_fn"') ' # simple version of below

  yblib.hasColor || return 0
  # vcs and color-aware version of bash prompt:

  local col_host="$col_blu"
  if term_is_tty_ssh;then
    local whitebold_on_red='\e[1;48;5;196;39;5;232m'
    local white_on_hotpink='\e[1;48;5;197;39;5;232m'
    col_host="$white_on_hotpink"
  fi

  local col_usr="$col_grn"
  if [[ "$UID" -eq "0" ]];then
    col_usr="$col_red" #root's color
    unset dotforest_renderPromptVcs
    alias dotforest_renderPromptVcs='echo -n' # don't execute `dotforest_renderPromptVcs`
  fi

  RET_VALUE='$(if [[ "$RET" -ne 0 ]];then echo -n ":\[\033[1;31m\]$RET\[\033[0m\]";fi)'
  # starts PS1 prompt with username@host-style identification
  PS1="$(dotforest_renderPromptIdentity)"
  # appends PS1 with job count in brackets
  PS1="$PS1[\j]"
  # appends PS1 with return value of the last command
  PS1="$PS1 ${RET_VALUE}"
  # appends PS1 with VCS information
  PS1="$PS1 ${col_grn}"'$(dotforest_renderPromptVcs)'"${col_end}"
  # appends PS1 with PWD
  PS1="$PS1 ${col_ylwB}\w${col_end}\n\t"
  # appends PS1 with current shell level
  PS1="$PS1 ${col_grnB}${SHLVL}${col_end}:"
  # appends PS1 with custom suffix that caller to this function might provide.
  PS1="$PS1 "'$('"$suffix_fn"') '
  # cleanup: ensure we end any formatting that mayb e stray
  PS1="${PS1}${col_end}"
  PS4='+$BASH_SOURCE:$LINENO:$FUNCNAME: '
}

function dotforest_togglePrompt() {
  if [[ -n "${__dotforest_togglePrompt_orig:-}" ]]; then
    PS1="$__dotforest_togglePrompt_orig"
    unset __dotforest_togglePrompt_orig
    return
  fi
  __dotforest_togglePrompt_orig="$PS1"

  dotforest_setSimplePrompt
}
