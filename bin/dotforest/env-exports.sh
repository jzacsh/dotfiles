#!/usr/bin/env bash
#
# Run all env. `export` statements
#
# NOTE: This is intended to be run from inside ~/.bashrc (yes, nested shells).

# Hacks to somewhat allow "export ENV=...." to be run a million times without
# making a mess.
#
# $1=env var to export (eg: PYTHONPATH)
# $@=strings to colon-delimited prepend (eg: /my/pylib /sys/pylib)

function exportPrependedOnce() {
  set +u
  local env_var="$1";shift

  # TODO make this safe to run in script that has `set -u` -- ie: temporarily
  # unset it if it's set

  local to_prepend="$(
    local -a found=( $(printf "${!env_var}" | tr : '\n') )
    local isFirst=1

    local part
    for part in $@; do
      printf '%s\n' "${found[@]}" | grep "$part" >/dev/null 2>&1 &&
          continue

      (( isFirst )) || printf ':'
      isFirst=0

      printf '%s' "$part"
    done
  )"

  [[ -z "$to_prepend" ]] && return  # nothing new

  local toExport="$to_prepend"
  [[ -n "${!env_var}" ]] && toExport="$toExport:"${!env_var}""

  export "${env_var}=$toExport"
}

# Static/vanilla (not appending) env vars:
export EMAIL=j@zac.sh
export GPG_TTY="$(tty)"
export XDG_DATA_HOME=$HOME/.local/share
export XDG_CONFIG_HOME=$HOME/.config
export CSCOPE_DB=$HOME/.vim/cscope.out
export COWER='cower --color=auto'
# graphical overrides should live in $HOSTFOREST_ROOT/desk.* files
export BROWSER=w3m
export SHOT_PUB='shot'
export SHOT_UPLOAD='ompload'
export TTS_CONFIG=~/.ttskeyrc
export PUNCH_CARD="$(xdg-user-dir DOCUMENTS)/punchcard"

export PASTIE="$DOTFOREST_ROOT/bin/lib/noop-pastie"
alias pastie='$PASTIE'

#TODO(jzacsh) at next boot - set this before starting tmux; experiment:
export TMUX_TMPDIR="$XDG_RUNTIME_DIR/tmux"
mkdir -vp "$TMUX_TMPDIR"

# TODO(jzacsh) figure out how to get rid of all of this LESSOPEN syntax
# highlighting spaghetti.
#
# prefer source highlighting over archive inspection
if haveCallable source-highlight; then
  export LESSOPEN='|source-highlight --data-dir=${TMPDIR:-/tmp/} %s'
elif haveCallable lesspipe; then
  export LESSOPEN='|lesspipe %s'
elif haveCallable lesspipe.sh; then
  export LESSOPEN='|lesspipe.sh %s'
elif haveCallable pygmentize; then
  if echo $TERM | grep 256 >/dev/null 2>&1;then
    # For more, see /usr/share/doc/python-pygments/formatters.html
    termOpt=' -f terminal256 '
  else
    termOpt=''
  fi
  export LESSOPEN="| pygmentize -g $termOpt %s"
  unset termOpt
elif haveCallable zacsh-highlight; then
  export LESSOPEN="| $(type -p zacsh-highlight) %s"
fi

#
# Programming language & SDK stuff below this line
#

# TODO(jzacsh) figure out if nixos and archlinux set these already for us (eg:
# is ANDROID_HOME already available?) and as such maybe these lines belong in
# distro-specific $HOSTFOREST files.
export GOPATH="$HOME/usr/lib/go"
export GRADLE_USER_HOME="$XDG_CONFIG_HOME"/gradle
export ECLIPSE_HOME="$HOME"/back/code/ide/eclipse/installations/java-mars
# $ANDROID_HOME is SDK install dir, per https://developer.android.com/tools/variables#android_home
export ANDROID_HOME="$HOME"/usr/lib/android/adk
# $ANDROID_USER_HOME is prefs dir, per https://developer.android.com/tools/variables#android_home
export ANDROID_USER_HOME="${XDG_CONFIG_HOME:-${HOME}/.config}"/android-dev

# see https://deno.land/#installation
export DENO_INSTALL="$HOME/usr/lib/deno"

# see https://docs.npmjs.com/resolving-eacces-permissions-errors-when-installing-packages-globally
export NPM_CONFIG_PREFIX="$HOME/usr/lib/npm/global"

# rustlang things; see https://sh.rustup.rs script for details
export RUSTUP_HOME="$HOME/usr/lib/rust/rustup"
export CARGO_HOME="$HOME/usr/lib/rust/cargo"

# more generally, see https://github.com/junegunn/fzf/issues/337 for some good discussion.
if command -v fd >/dev/null; then
  # adapted from helpful sample usage github.com/sharkdp/fd#integration-with-other-programs
  export FZF_DEFAULT_COMMAND='fd  --type file --hidden --exclude .git'
elif command -v ag >/dev/null; then
  export FZF_DEFAULT_COMMAND='ag --hidden --ignore .git --files-with-matches -g ""'
else
  # This works, but unlike `ag` and `fd`, it won't dynamically consider any
  # .gitignore files in the current repo:
  export FZF_DEFAULT_COMMAND='find . \! \( -type d -path ./.git -prune \) \! -type d -printf '\''%P\n'\'
fi


#
# Export a single $PATH env. variable
#


# WARNING: "$HOME/bin (clone of gitlab.com/jzacsh/bin) is being slowly replaced
# with "$DOTFOREST_BIN" (a subdir of gitlab.com/jzacsh/dotfiles). Exports of
# "$DOTFOREST_BIN" don't need to be exported here has they're done elsewhere
# ("$DOTFOREST_BIN"/path-setup.sh tiny specialty exports).
#
# Directories we expect to exist on a given machine can be found in `nonXdgDirs`
# of laptop-homedir-setup.sh in https://gitlab.com/jzacsh/desktop-setup repo.
#
# $ANDROID_HOME subdirs per https://developer.android.com/tools/variables
exportPrependedOnce 'PATH' \
  "$HOME/bin" \
  "$HOME/bin/local" \
  "$HOME/bin/share" \
  "$HOME/bin/dist" \
  "$HOME/bin/lib" \
  "$GOPATH/bin" \
  "$HOME/.local/bin/" \
  "$HOME/usr/share/appimage" \
  "$HOME/usr/local/bin" \
  "$ANDROID_HOME/tools" \
  "$ANDROID_HOME/tools/bin" \
  "$ANDROID_HOME/platform-tools" \
  "$HOME/usr/local/bin/valgrind/bin" \
  "$CARGO_HOME/bin" \
  "$DENO_INSTALL/bin" \
  "$NPM_CONFIG_PREFIX/bin" \
  "$HOME/.rbenv/shims"


if [[ -e "$HOME"/usr/local/build ]]; then
  while read -r binDir; do
    export PATH="${binDir}:${PATH}"
  done < <(listBashGlob "$HOME"/usr/local/build/*/install/bin)
fi

exportPrependedOnce 'PYTHONPATH' "$HOME/usr/lib/python"
exportPrependedOnce 'NODE_PATH' \
  "/usr/local/lib/node_modules" \
  "/usr/lib/node"


unset exportPrependedOnce
