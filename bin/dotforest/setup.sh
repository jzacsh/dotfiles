#!/usr/bin/env bash
# shellcheck disable=SC2016

# TODO(jzacsh) replace all `source`rs of yabashlib with the below function
function dotforest_src_yabashlib() {
  # shellcheck disable=SC1091
  source yabashlib || {
    printf 'yabashlib unavailable; hostforest cannot run!\n' >&2
    return 1
  }
  setLogPrefixTo '.forest'
}

dotforest_src_yabashlib || return 1

# Thin wrapper for yabashlib logger.
#
# $1=info|warn|err
function dotforest_log() (
  local level="$1"; shift 1

  # TODO fix yabashlib to check whether color is supported before using it
  case "$level" in
    info) logfInfo "$@";;
    warn) logfWarning "$@";;
    # err; all else should be loud as it's just wrong
    *) logfError "$@";;
  esac
)

# Basic helper meant to print information and orient myself.
function dotforest() {
  pushd "$DOTFOREST_ROOT" || {
      dotforest_log warn \
        '$DOTFOREST_ROOT could not be `pushd`d into: %s\n' \
        "$DOTFOREST_ROOT"
      return 1
  }
  # TODO port to yabashlib vcs.sh
  git st 2>/dev/null ||
    jj diff  -r @ | diffstat -C

  dotforest_log info 'Welcome to your dotforest (`popd` to return)\n'
  if [[ -e "$HOSTFOREST_ROOT" ]]; then
    dotforest_log info '... you also have a `hostforest` at: %s\n' "$HOSTFOREST_ROOT"
  fi
  if [[ -e "$CORPFOREST_ROOT" ]]; then
    dotforest_log info '... you also have a `corpforest` at: %s\n' "$CORPFOREST_ROOT"
  fi
}

# Basic helper meant to print information and orient myself.
function hostforest() {
  [[ -e "$HOSTFOREST_ROOT" ]] || {
    dotforest_log warning '$HOSTFOREST_ROOT missing: there is no hostforest on this machine; were you expecting one?\n'
    return 1
  }
  local envPushd
  if [[ "${1:-}" = host ]]; then
    envPushd="HOSTFOREST_DISTRO_SRC"
  else
    envPushd="HOSTFOREST_ROOT"
  fi
  pushd "${!envPushd}" || {
      dotforest_log warn \
        '$%s could not be `pushd`d into: %s\n' \
        "$envPushd" "${!envPushd}"
      return 1
  }
  # TODO port to yabashlib vcs.sh
  git st 2>/dev/null ||
    jj diff  -r @ | diffstat -C
  dotforest_log info \
    "Welcome to your hostforest's $%s (popd to return)\n" \
    "$envPushd"
}

# Basic helper meant to print information and orient myself.
function corpforest() {
  [[ -e "$CORPFOREST_ROOT" ]] || {
    dotforest_log warning 'there is not corpforest on this machine; were you expecting one?\n'
    return 1
  }
  pushd "$CORPFOREST_ROOT" || {
      dotforest_log warn \
        '$CORPFOREST_ROOT could not be `pushd`d into: %s\n' \
        "$CORPFOREST_ROOT"
      return 1
  }
  # TODO port to yabashlib vcs.sh
  git st 2>/dev/null ||
    jj diff  -r @ | diffstat -C
  dotforest_log info 'Welcome to your corpforest (`popd` to return)\n'
}

function dotforest_must_source() {
  local instruction='sourcing must be of readable, non-empty file'
  [[ -e "$1" ]] || {
    dotforest_log err '%s: non-existent, but %s: %s\n' "${FUNCNAME[0]}" "$instruction" "$1"
    return 1
  }
  [[ -f "$1" ]] || {
    dotforest_log err '%s: not regular file, but %s: %s\n' "${FUNCNAME[0]}" "$instruction" "$1"
    return 1
  }
  [[ -r "$1" ]] || {
    dotforest_log err '%s: not readable, but %s: %s\n' "${FUNCNAME[0]}" "$instruction" "$1"
    return 1
  }
  [[ -s "$1" ]] || {
    dotforest_log err '%s: empty(?), but %s: %s\n' "${FUNCNAME[0]}" "$instruction" "$1"
    return 1
  }

  # shellcheck disable=SC1090
  source "$1"
}

# shellcheck disable=SC1091
source "$DOTFOREST_BIN"/dotforest/sideload # produces AIs I'll typically need a browser for
