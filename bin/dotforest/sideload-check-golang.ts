#!/usr/bin/env -S deno run --allow-write --allow-env --allow-run --allow-net --allow-read
/**
 * Checks whether version of golang is the most up to date.
 *
 * Uses golang source repo's version tags to determine this, comparing to
 * `go version` output.
 */
// TODO(jzacsh) rm '.ts' extension on this file once deno solves this:
// https://github.com/denoland/deno/issues/5088

const { exit } = Deno;

import { exec } from 'https://deno.land/x/execute@v1.1.0/mod.ts';
import { assert } from '../lib/js/assert.ts';
import {
  comparatorString,
  compareNumbers,
  Comparison,
} from '../lib/js/compare.ts';
import { deno_XdgRuntimeCacheUrl } from '../lib/js/http_xdgcache.ts';
import {
  SEC_MS,
  MIN_MS,
  HRS_MS,
  DAY_MS,
} from '../lib/js/milli.ts';
import { stripXsrfTokens } from '../lib/js/web.ts';

// basic tools above this line

const MAX_CACHE_AGE_MS = 7 * DAY_MS;
const DEBUGGING = false;

const EXPERIMENT_JUST_FILTER = true;

/**
 * Fetches results from golang git repo's API, which looks something like:
 *    {
 *      "go1": {
 *        "value": "6174b5e21e73714c63061e66efdbe180e1c5491d"
 *      },
 *      "go1.0.1": {
 *        "value": "2fffba7fe19690e038314d17a117d6b87979c89f"
 *      },
 *      "go1.0.2": {
 *        "value": "cb6c6570b73a1c4d19cad94570ed277f7dae55ac"
 *      }
 *    }
 */
async function golangRefsApi(): Promise<string> {
  const tagsJsonUrl = 'https://go.googlesource.com/go/+refs/tags?format=JSON';
  const cacheFileBasename = `golang-update_702a271a9c9b8ca0c41cff7394613979b59853f6.txt`;
  const cacheTtlMs = DEBUGGING ? 2 * SEC_MS : MAX_CACHE_AGE_MS;

  return new deno_XdgRuntimeCacheUrl(
    tagsJsonUrl,
    cacheFileBasename,
    cacheTtlMs,
  ).fetch();
  
}

function maybeLog(
  logLabel: string,
  a: string,
  b: string,
  result: Comparison,
): Comparison {
  if (DEBUGGING) {
    console.log(`[${logLabel}] comparing a="${a}" vs b="${b}" --> ${
      comparatorString(result)
    }`);
  }
  return result;
}

class SemVerPart {
  private static readonly NumericRegexp = /^(\d+)/;
  public isNumeric: boolean;
  constructor(private readonly part: string) {
    this.isNumeric = !!part.match(SemVerPart.NumericRegexp);
  }
  getNumber(): number {
    const match: string = assert(
      this.part.match(/^(\d+)/),
      `parse error: encountered semver identity "${this.part}" with no numeric component`,
    )![1];
    return Number.parseInt(match, 10 /*radix*/);
  }

  toString(): string {
    return `${this.part}[num=${this.getNumber()}]`
  }

  compare(b: SemVerPart): Comparison {
    return maybeLog(
        "SemVerPart",  // logLabel
        this.toString(),
        b.toString(),
        SemVerPart.comparator(this  /*a*/, b));
  }
  static comparator(a: SemVerPart, b: SemVerPart): Comparison {
    const aNum = a.getNumber();
    const bNum = b.getNumber();
    const naiveResult = compareNumbers(aNum, bNum);
    if (a.isNumeric && b.isNumeric) return naiveResult;

    if (naiveResult !== Comparison.AB_Equal) return naiveResult;
    return b.isNumeric ? Comparison.B_Larger : Comparison.A_Larger;
  }
}

class SemVer {
  public readonly parse: Array<SemVerPart>;
  private static readonly SEM_VER_DELIM: string = '.';
  constructor(private readonly raw: string) {
    assert(raw.trim().length, `parsing an empty string: "${raw}"`);
    const parts: Array<string> = raw.split(SemVer.SEM_VER_DELIM);
    parts.forEach(p => assert(p.trim(), `found empty identifier "${p}"`));
    assert(parts.length, `zero parts found on version tag: "${raw}"`);
    this.parse = parts.map(p => new SemVerPart(p));
  }

  toString(): string {
    return this.raw;
  }

  compare(b: SemVer): Comparison {
    return maybeLog(
        "SemVer",  // logLabel
        this.toString(),
        b.toString(),
        SemVer.comparator(this  /*a*/, b));
  }

  static comparator(a: SemVer, b: SemVer): Comparison {
    for (let i = 0; i < Math.min(a.parse.length, b.parse.length); i++) {
      if (i > a.parse.length - 1 || i > b.parse.length - 1) break; // impossible

      let idResult = a.parse[i].compare(b.parse[i]);
      if (idResult === Comparison.AB_Equal) continue;
      return idResult;
    }
    if (a.parse.length === b.parse.length) return Comparison.AB_Equal;
    return a.parse.length < b.parse.length ? Comparison.B_Larger : Comparison.A_Larger;
  }
}

/**
 * Super crude semver utility. Does not support 90% of the grammar of
 * any semver.org specifications.
 *
 * NOTE: golang does NOT adhere semver.org spec anyway.
 */
class SemVerReader {
  private readonly slugRegexp: RegExp;
  constructor(
    private readonly slug: string = '',
  ) {
    this.slugRegexp = new RegExp(`^${this.slug}`);
  }

  isTag(tag: string): boolean {
    if (!this.slug)
      throw new Error(`unimplemented: SemVer doesn't parse tags to check for semver.org validity`);

    return !!tag.match(this.slugRegexp);
  }

  private tagToSemver(tag: string): SemVer {
    return new SemVer(tag.replace(this.slugRegexp, ''));
  }

  comparator(aTag: string, bTag: string): Comparison {
    const a = this.tagToSemver(aTag);
    const b = this.tagToSemver(bTag);
    const result = a.compare(b);
    return maybeLog(
        "SemVerReader"  /*logLabel*/,
        a.toString(),
        b.toString(),
        result);
  }
}

const GOLANG_SEMVER_READER = new SemVerReader('go');

async function golangTags(): Promise<Array<string>> {
  const resp = await stripXsrfTokens(await golangRefsApi());

  return Object.
    keys(JSON.parse(resp)).

    // Do some simple hygiene of inputs
    map(tag => tag.trim()).
    filter(tag => tag).

    // Interpret semver values as presented as git tags
    filter(tag => GOLANG_SEMVER_READER.isTag(tag));
}

/** Returns _roughly_ semver-sorted golang repository tags for main releases. */
async function golangTagsSorted(): Promise<Array<string>> {
  const tags = await golangTags();

  return tags.sort((a, b) => GOLANG_SEMVER_READER.comparator(a, b));
}

async function latestUpstreamVersion(): Promise<string> {
  return golangTagsSorted().then(tags => tags[0]);
}

async function installedVersion(): Promise<string> {
  // Expecting "go version go1.1.6.4 linux/amd64" output for `go version`
  return exec('go version').then((goline: string) => {
    const parts = assert(goline, 'expected non-empty `go version` output').split(' ');
    assert(
        parts.length === 4,
        `internal bug: 'go version' output changed from usual format; got: "${goline}"`);
    const tag = parts[2];
    assert(
      GOLANG_SEMVER_READER.isTag(tag),
      `internal bug: 'go version' output a version tag we don't understand: "${tag}"`);
    return tag;
  });
}

/// actual main logic
function settled(goodVersion: string) {
  if (DEBUGGING) console.log(`installed and upstream versions match (${goodVersion})`);
  exit(0);
}

const installedVer = await installedVersion();
if (EXPERIMENT_JUST_FILTER) {
  const tags = await golangTags();
  let newerUpstreams = tags.find(upstream => GOLANG_SEMVER_READER.comparator(installedVer, upstream) === Comparison.B_Larger);
  if (newerUpstreams === undefined) settled(installedVer);
  console.error(
    `golang is old - visit https://golang.org/dl - installed "${installedVer}" vs. a newer upstream: "${newerUpstreams}"`);
  exit(1);
} else {
  const latestUpstream = await latestUpstreamVersion();
  if (DEBUGGING) {
    console.log('installed version: "%s"', installedVer);
    console.log('newest upstream: "%s"', latestUpstream);
  }

  const result = GOLANG_SEMVER_READER.comparator(installedVer, latestUpstream);
  if (result === Comparison.AB_Equal) settled(installedVer);
  console.error(`golang is old: installed=${installedVer} vs. upstream=${latestUpstream}`);
  exit(1);
}
