# Meta-Sources

Things that live here don't _add_ to my home configuration other than the fact
that they _assist_ the dotfile configuration _setup_. So they're really _meta_
sources that are used during the initialization of my login, etc.
