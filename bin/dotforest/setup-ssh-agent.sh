#!/usr/bin/env bash
#
# Provides common tooling for hacking my ssh-agent into existence and sometimes
# mucking with my SSH_AUTH_SOCK and any other related, horribly half-understood
# logic I might rely on.

# NOTE: this depends on yabashlib

function __dotforest_sshsetup_haveSockEnv () { strIsContentful "$SSH_AUTH_SOCK"; }

function __dotforest_sshsetup_tmuxSockSymlink() { echo "${TMPDIR:-"$HOME"/.ssh}"/ssh-auth-sock.tmux; }

# Whether an ssh agent appears to be up and running.
function dotforest_sshsetup_isAgentSetup() {
  # Note we do NOT check for the presence of a local ssh agent process; that's
  # because we might be on a machine for which all ssh agent interactions are
  # being forwarded from elsewhere.
  __dotforest_sshsetup_haveSockEnv &&
    [[ -e "$SSH_AUTH_SOCK" ]]
}

# Why is this so complicated? see
# https://blog.testdouble.com/posts/2016-11-18-reconciling-tmux-and-ssh-agent-forwarding/#the-socket-shouldnt-really-move
function dotforest_sshsetup_setupSshAgent() {
  if __dotforest_sshsetup_haveSockEnv;then
    # tmux management
    #TODO: maybe use $XDG_RUNTIME_DIR instead?
    if [[ -e "$SSH_AUTH_SOCK" ]];then
      tmuxSshSock="${TMPDIR:-"$HOME"/.ssh}"/ssh-auth-sock.tmux
      if [[ "$(readlink -f "$SSH_AUTH_SOCK")" != "$(readlink -f "$tmuxSshSock")" ]];then
        was="$SSH_AUTH_SOCK"
        rm -f "$tmuxSshSock"
        ln -sf "$SSH_AUTH_SOCK" "$tmuxSshSock"
        export SSH_AUTH_SOCK="$tmuxSshSock"
        dotforest_log warn \
          'stabilized $SSH_AUTH_SOCK; was "%s", is now (via symlink) "%s"\n' \
          "$was" "$SSH_AUTH_SOCK" >&2
      fi
      unset tmuxSshSock
    else
      dotforest_log warn \
        'found dead file on $SSH_AUTH_SOCK(%s); cleaning its export\n' \
        "$SSH_AUTH_SOCK" >&2
      export SSH_AUTH_SOCK=''
      unset SSH_AUTH_SOCK
    fi
  else
    # NOTE on debugging modern Gnome+Wayland; ie:
    #   [[ -n "$DISPLAY" ]] &&
    #   [[ -n "$DBUS_SESSION_BUS_ADDRESS" ]] &&
    #   [[ "$XDG_SESSION_TYPE" = wayland ]] &&
    #   [[ "${DE:-$DESKTOP_SESSION}" = gnome ]]
    #
    # These two posts are very relevant:
    #   https://unix.stackexchange.com/a/360309
    #   https://bugzilla.gnome.org/show_bug.cgi?id=738205#c40
    #
    # The result is that I've gotten the below (vanilla, very old logic I've
    # always had in my ~/.bashrc) working IFF the following files carefully help
    # orchestrate and fix things:
    # - 1. ~/.config/autostart/sshadd.desktop
    #      This^ file keeps gnome keyring daemon in wayland from trying to handle
    #      SSH keys for me.
    # - 2. ~/.config/systemd/user/sshkeys.service
    #      This^ file *also* keeps gnome keyring daemon in wayland from trying to
    #      handle SSH keys for me by exporting a magic workaround variable.
    # - 3. The below code starts an ssh-agent for bourne shell the old fashioned
    #      way.
    if pgrep -l ssh-agent >/dev/null 2>&1 ;then
      dotforest_log err \
        'ssh-agent already present (`pgrep -l ssh-agent`) but undiscoverable via $SSH_AUTH_SOCK\n'
    fi
    [[ -z "$CHROME_REMOTE_DESKTOP_SESSION" ]] || {
      dotforest_log err \
        'cannot start ssh agent when $CHROME_REMOTE_DESKTOP_SESSION is set (found value "%s")\n' \
        "$CHROME_REMOTE_DESKTOP_SESSION"
      return 1
    }
    local ssh_agent_eval
    if ssh_agent_eval="$(ssh-agent -s)"; then
      eval $ssh_agent_eval
      dotforest_log info \
        'SSH_AUTH_SOCK was empty, started[%s] new agent (now="%s")\n' \
        "$?" "$SSH_AUTH_SOCK"
    else
      dotforest_log err 'SSH_AUTH_SOCK was empty, but failed to start\n'
    fi
  fi
}

function dotforest_sshsetup_impatientListKeys() { timeout "${1:-1.05s}" ssh-add -l; }

function dotforest_sshsetup_autoAddSshKeys() (
  local keys_glob=~/.ssh/key.auto'.*[^pub]'
  isFilledBashGlob "$keys_glob" || {
    dotforest_log warn \
      'SSH keyring: no keys installed (checked: %s)\n' \
      "$keys_glob"
    return 1
  }
  dotforest_log info \
    'SSH keyring: ensuring all %d keys are present...\n' \
    "$(listBashGlob "$keys_glob" | wc -l)"
  local extant_keys

  # Do nothing if we're experiencing hanging `ssh-add -l` behavior
  extant_keys=( "$(dotforest_sshsetup_impatientListKeys 2>/dev/null)" )
  if [[ "$?" -eq 124 ]]; then
  dotforest_log err '`ssh-add -l` listing hanging again; not fixing keys...\n' >&2
    return 1
  fi


  local hasWarned=0 privKey fingerprint
  # Purposely do _not_ quote here, so the glob will be evaluated
  for privKey in $keys_glob;do
    fingerprint="$(ssh-keygen -l -f "$privKey" | cut -f 2 -d ' ')"
    printf '%s\n' "${extant_keys[@]}" | grep --quiet "$fingerprint" || {
      if ! (( hasWarned )); then
        dotforest_log info \
          'found key missing $SSH_AUTH_SOCK="%s" (this one %s)...\n' \
          "$SSH_AUTH_SOCK" "$privKey"
        hasWarned=1
      fi
      ssh-add "$privKey"
    }
  done
  dotforest_log info \
    'SSH keyring ($SSH_AUTH_SOCK=%s) has %d keys\n' \
    "$SSH_AUTH_SOCK" \
    "$(dotforest_sshsetup_impatientListKeys 2>/dev/null | wc -l)"
)

function dotforest_sshsetup_debugState() {
  local tmuxSshSock; tmuxSshSock="$(__dotforest_sshsetup_tmuxSockSymlink)"

  printf 'DEBUGGING: ssh debugging info:\n'
  printf '\t%s="%s"\n' \
    '$SSH_AGENT_PID' "$SSH_AGENT_PID" \
    '$SSH_AUTH_SOCK' "$SSH_AUTH_SOCK" \
    'readlink $SSH_AUTH_SOCK' "$(readlink -f "$SSH_AUTH_SOCK" 2>&1)" \
    "readlink "$(basename "$tmuxSshSock")"" "$(readlink -f "$tmuxSshSock" 2>&1)" \
    'file "$tmuxSshSock"' "$(file "$tmuxSshSock" 2>&1)"

  printf 'DEBUGGING: listing of ~/.ssh/key.* fingerprints...\n'
  for f in ~/.ssh/key.auto.*[^pub]; do
    printf '\tfile="%s"\n\tfingerprint="%s"\n\n' \
      "$(basename "$f")" \
      "$(ssh-keygen -l -f "$f" | cut -f 2 -d ' ')"
  done

  local crd=CHROME_REMOTE_
  printf 'DEBUGGING: seeking $%s... env variables:\n' "$crd"
  env | grep -E '^'"$crd"
  echo

  printf 'DEBUGGING: `ssh-add -l` report:\n'
  ssh-add -l
  echo

  printf 'DEBUGGING: `ssh-agent`s running in your env (per `pgrep`)...\n'
  while read pid; do ( set -x; pstree -ps "$pid"; ); done < <(pgrep ssh-agent)
  echo

  printf DEBUGGING: '`ssh-agent`-looking things on your machine\n'
  ps aux | grep ssh-agent
}

function dotforest_sshsetup_forceResetSshAgents() {
  dotforest_log warn 'force re-installing a new SSH agent...\n'
  if pgrep --uid "$USER" ssh-agent >/dev/null 2>&1; then
    dotforest_log warn 'found existing ssh-agent(s), killing them....\n'
    (
      set -x
      pkill --echo ssh-agent
    ) || {
      printf 'failed to kill all ssh-agents; run dotforest_sshsetup_setupSshAgent if you want to continue anyway\n' >&2
      return 1
    }
  fi
  dotforest_sshsetup_setupSshAgent
}
