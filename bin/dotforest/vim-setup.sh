#!/usr/bin/env bash

# TODO break this dependency by moving hostforest_link up to dotforest (and
# renaming of course).
source "$HOSTFOREST_ROOT"/common/hostforest

nvim_pack="${XDG_DATA_HOME:-$HOME/.local/share}"/nvim/site
mkdir -p "$nvim_pack"
hostforest_link ~/back/dots/vendor/src/vim-pack "$nvim_pack"/pack
hostforest_link ~/back/dots/vendor/src/vim-pack ~/.vim/pack

unset nvim_pack
