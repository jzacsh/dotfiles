# empty direcotry for host-specific symlinks

This directory is a place to drop symlinks or one-off builds of executables that
should be in $PATH.

This means that NOTHING should be committed to this folder aside from this
README you're reading now (and a .gitignore to assist this sentence).
