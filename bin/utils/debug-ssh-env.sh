#!/bin/bash

(
  if source "$DOTFOREST_BIN"/dotforest/setup-ssh-agent.sh; then
    dotforest_sshsetup_debugState
  else
    printf 'failed to find and source ssh setup code\n' >&2
  fi
)
