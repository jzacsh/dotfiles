#!/usr/bin/env python3
"""
Very simple HTTP server in python for logging requests (defaults to 8080).
Usage::
    ./server.py [PORT]

Adapted from https://gist.github.com/mdonkers/63e115cc0c79b4f6b8b3a6b797e485c7
"""
from http.server import BaseHTTPRequestHandler, HTTPServer
import logging

def build_multiline_log(label, content):
    return "'''[start of {}]\n{}\n'''[eof {}]\n".format(label, content, label)

class S(BaseHTTPRequestHandler):
    def _set_response(self):
        self.send_response(200)
        self.send_header('Content-type', 'text/html')
        self.end_headers()

    def do_GET(self):
        logging.info(
                "GET(Start) request being handled,\nPath: %s\n%s\nGET(End)\n",
            str(self.path),
            build_multiline_log("headers", str(self.headers)))
        self._set_response()
        self.wfile.write("httplogger GET response for {}\n".format(self.path).encode('utf-8'))

    def do_POST(self):
        content_length = int(self.headers['Content-Length'] or 0)
        post_data = self.rfile.read(content_length)
        logging.info(
            "POST(Start) request being handled [body length %d],\nPath: %s\n%s\n%s\nPOST(End)\n",
            content_length,
            str(self.path),
            build_multiline_log("headers", str(self.headers)),
            build_multiline_log("body", post_data.decode('utf-8')))

        self._set_response()
        self.wfile.write("httplogger POST response for {}\n".format(self.path).encode('utf-8'))

def run(server_class=HTTPServer, handler_class=S, port=8080):
    logging.basicConfig(level=logging.INFO)
    server_address = ('', port)
    httpd = server_class(server_address, handler_class)
    logging.info('Starting httpd that simply logs requests on localhost:%d...\n', port)
    try:
        httpd.serve_forever()
    except KeyboardInterrupt:
        pass
    httpd.server_close()
    logging.info('Stopping httpd...\n')

if __name__ == '__main__':
    from sys import argv

    if len(argv) == 2:
        run(port=int(argv[1]))
    else:
        run()
