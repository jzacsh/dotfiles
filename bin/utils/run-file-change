#!/usr/bin/env bash
#
# Crappy, single-file-polling version of gitlab.com/jzacsh/runonchange for
# places I can't install that binary.
declare -r pollIntervalSec=1.5

function fatalf() {
  local msg="$1"; shift
  # shellcheck disable=SC2059
  printf 'fatal: '"$msg" "$@" >&2
  exit 1
}
this="$(basename "$0")"

(( $# == 1 )) || fatalf 'usage error. see help doc:

%s Prints a newline when FILE_TO_WATCH bytes change

   FILE_TO_WATCH changes are detected no more frequently than every %s seconds.

Usage: %s FILE_TO_WATCH

Example:
   while read _; do make all;done < <(%s FILE_TO_WATCH)
' "$this" "$this" "$pollIntervalSec" "$this"

targetFile="$1"
function checksumTarget() { md5sum "$targetFile"; }

last_sum_file="$(mktemp --tmpdir= "$(date +%FT%s_runfilechange_XXXXXXX)")" ||
  fatalf 'failed to generate temp file for checksum usage\n'

while true; do
  if diff "$last_sum_file" <(checksumTarget) >/dev/null; then
    printf '.' >&2 # waiting-UI
  else
    printf '\r' >&2 # clear previous waiting-UI
    checksumTarget > "$last_sum_file"

    # linebreak is what's consumed by users of this script
    printf '%s re-running @%s\n' "$this" "$(date --iso-8601=ns)"
  fi
  sleep "$pollIntervalSec"
done
