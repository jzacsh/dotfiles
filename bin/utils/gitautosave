#!/usr/bin/env bash
#
# Usage: DIR|FILE MESSAGE_FORMAT
#
# Automatically commits all changes to DIR|FILE and prints exactly one line to
# stdout as a signal when this happens.
#
# Indefinitely watches FILE (or all descendents of DIR) for changes, and for
# each change: (1) automatically commits the current state of the entire repo
# with a generated git commit message, and printing some CLI UX to stderr and
# printing exactly one line to stdout.
#
# Commits occur in DIR or in dirname(1) of FILE. Commit message is dyamically
# generated, or uses MESSAGE_FORMAT (which must includes a "%s" dynamic summary
# is included).

# TODO port this script to yblib.vcs_autosave in yabashlib
source yabashlib || {
  printf 'fatal: could not source requisite yabashlib\n' >&2
  exit 1
}
declare -r self_tag=gitautosave
setLogPrefixTo "$self_tag"

requireCallable inotifywait
requireCallable git

###############################################################################
# CLI parsing logic ###########################################################

declare -r flag_repotarget="${1}"
strIsContentful "$flag_repotarget" || logfFatal 'DIR|FILE arg required\n'
# TODO replace this with proper flag parsing
if ! isNonemptyFile "$flag_repotarget" && ! isNonemptyDir "$flag_repotarget"; then
  logfFatal 'Target must be an existing file or directory\n'
fi

# TODO impl flag parsing an make this optional via [-m|--message]=?...
flag_message_default='auto saved for watch on %s'
flag_message_desc='Commit message format containing a %s to hold dynamic summary'
flag_message="${2:-$flag_message_default}"

# TODO impl flag parsing and make this overrideable
# TODO rename variabe to clarify this is about low-level filesystem events
flag_fseventsec_default=3 # 3sec
flag_fseventsec_desc='Number of seconds between file-changes should be ignored'
flag_fseventsec=$flag_fseventsec_default
strIsWholenumber $flag_fseventsec || logfFatal 'flag --fseventsec should be a natural number\n'

flag_vcspushsec_default=$(( 5 * 60 )) # 2min
flag_vcspushsec_desc='Number of seconds between VCS pushes should be ignored'
flag_vcspushsec=$flag_vcspushsec_default
strIsWholenumber $flag_vcspushsec || logfFatal 'flag --vcspushsec should be a natural number\n'

# TODO impl flag parsing and make this overrideable
flag_showdiff_default=0
flag_showdiff_desc='Whether to print a diff as FYI before each commit'
flag_showdiff=$flag_showdiff_default

# TODO impl flag parsing and make this overrideable
flag_progressux_default=1
flag_progressux_desc='Whether to show progress on stderr as opposed to hiding all progress indicators'
flag_progressux=$flag_progressux_default

# TODO impl flag parsing and make this overrideable
flag_cleanrun_default=0
flag_cleanrun_desc='Whether to skip VCS mutations'
flag_cleanrun=$flag_cleanrun_default

# TODO impl flag parsing and make this overrideable
flag_vcpush_default=1
flag_vcpush_desc='Whether to share your VCS changes upstream'
flag_vcpush=$flag_vcpush_default

# TODO impl flag parsing and make this overrideable
flag_exclude_default=''
flag_exclude_desc='Optional exclude pattern to pass to underlying inotifywait(1)'
flag_exclude=$flag_exclude_default

###############################################################################

# $@ commandline that would have been run
function logCleanRunCmd() { logfWarning 'cleanrun, skipping: `%s`\n'  "$*"; }

# Whether a sensitive action should be taken according to your own debounce
# limits.
#
# $1=epoch (seconds) of the last action you took
# $1=your own debouncee limit (in seconds)
function _shouldWaitPatiently() {
  local epochSecondsLastActed="$1"
  local debounce_sec"$2"

  if (( epochSecondsLastActed == 0 )); then
    return 1 # act now; you've never acted before
  fi

  local now; now="$(date +%s)"
  local secondsSinceLastEvent
  secondsSinceLastEvent=$(( now - epochSecondsLastActed ))

  # Wait, if you've not yet hit your debounce window
  [[ "$secondsSinceLastEvent" -le $debounce_sec ]]
}

# Negating wrapper for _shouldWaitPatiently.
function _shouldActNow() { ! _shouldWaitPatiently "$@"; }

# TODO port git interactions to be VCS-agnostic, then replace isGitRepo with
# yblib.isVcsRepo
function isGitRepo() ( git rev-parse --is-inside-work-tree >/dev/null 2>&1; )
function _vcsPush() { git push --quiet; }
function _mustGitCommit() {
  local commit_msg="$1"
  local stage="${2:-change}"
  local msgFormat="[${self_tag}:"$stage"]: $flag_message"

  local git_commit_flags; git_commit_flags=(
    --all
    --message="$(printf -- "$msgFormat" "$commit_msg")"
  )
  (( flag_progressux )) || git_commit_flags+=( --quiet )

  if (( flag_cleanrun )); then
    logCleanRunCmd git commit "${git_commit_flags[*]}"
  else
    git commit "${git_commit_flags[@]}" ||
      logfFatal 'auto commit failed\n'
  fi
}
# TODO implement this; we call this "slowly" because we want to remember not to
# run this too much.
function slowlyTryToSync() {
  if yblib.netwIsLikelyOnline; then
    logfWarning 'pushing changes remotely\n'
    _vcsPush || return 1
  else
    logfWarning 'Offline? SKIPPING VCS push\n'
  fi
  return 0
}

last_vcspush_sec=0 # `date +%s` for an example value
function shouldTryPush() {
  (( flag_vcpush )) || return 1 # no, user doesn't want this

  _shouldActNow \
    $last_vcspush_sec $flag_vcspushsec || return 1 # no, ignore
  last_vcspush_sec="$(date +%s)"
  return 0 # no, act now
}

# Saving content means both committing it and pushing it (when network is
# available).
function mustSaveContent() {
  if (( flag_cleanrun )); then
    logCleanRunCmd git add .
  else
    git add . || logfFatal 'failed to stage contents for autocommit\n'
  fi

  if (( flag_progressux )) && (( flag_showdiff ));then
    git diff --cached
  fi

  _mustGitCommit "$@"

  (
    if shouldTryPush; then
      slowlyTryToSync || logfFatal 'failed to sync upstream\n'
    fi
  )
}

# prints a unicode codepoint ($1) or something simpler ($2) if it doesn't appear
# terminal will like it.
function getCodeOrChar() {
  local utf_codepoint="$1" ascii_fallback="$2"
  if [[ "$(locale charmap)" =~ ^UTF ]]; then
    echo -ne '\U'"$utf_codepoint"
  else
    printf -- '%s' "$ascii_fallback"
  fi
}

function getProgressChar() { getCodeOrChar 2591 '|'; }

# Prints a new line for every filesystem change detected in $PWD.
#
# See --format= flag passed in implementation for components of the line.
function listFilesystemChangesInPwd() {
  local inotify_extra_args; inotify_extra_args=()

  if [[ -d "$flag_repotarget" ]]; then
    inotify_extra_args+=( --recursive )
  fi

  local extra_exclude_arg
  if strIsContentful "$flag_exclude"; then
    extra_exclude_arg='--exclude='"$flag_exclude"
  fi

  # these are: when(seconds epoch), what, dirChild, events (comma separated)
  local notify_format='%T %w %f %e'
  local exclude_baseline='(^\.\w*.*\.s[a-w][a-z]$)|^\.(/\.)?git/*'
  # TODO: fix exclude logic by replacing inotifywait's logic with simple sed
  # pipes. in these steps:
  # 1) include %w (in --format)
  # 2) update forloop to concat %w/%f
  # 3) strip vcs root from prefix
  # 4) with said filename start excluding
  # 5) allow for adding _additional_ excludes (again, via sed) via cli flags $flag_exclude

  inotifywait "${inotify_extra_args[@]}" \
    --quiet \
    --monitor \
    -e modify \
    -e attrib \
    -e close_write \
    -e moved_to \
    -e moved_from \
    -e move_self \
    -e create \
    -e delete \
    -e delete_self \
    --exclude="$exclude_baseline" \
    --timefmt='%s' \
    --format="$notify_format" .
}

# TODO fix yabashlib logfFatal (and other `die` methods) so they can break out
# of subshells

last_save_sec=0 # `date +%s` for an example value
# Whether the event should be ignored.
#
# We ignore events if they're too close in succession to one we recently acted
# on (see $flag_fseventsec), or if there's simply nothing to act on (VCS state is
# clean).
function shouldIgnoreFilesystemActivity() {
  vcs_git_is_dirty || return 0 # safe to ignore this event

  _shouldActNow $last_save_sec $flag_fseventsec || return 0
  last_save_sec="$(date +%s)"

  return 1 # do not ignore this event; rather, act on it
}

###############################################################################
# main program execution below this line ######################################

if [[ -d "$flag_repotarget" ]];then
  cd "$flag_repotarget" || logfFatal 'failed `cd`ing into: "%s"\n' "$flag_repotarget"
  isGitRepo ||
    logfFatal 'DIR passed is not a git repo:\n\t%s\n' "$flag_repotarget"
  if (( flag_progressux )); then
    logfInfo \
      'Watching files and commiting to: "%s"\n' \
      "$(readlink -f "$flag_repotarget")" \
      >&2
  fi
else
  cd "$(dirname "$flag_repotarget")" ||
    logfFatal 'failed `cd`ing into "%s"\n' "$(dirname "$flag_repotarget")"
  isGitRepo ||
    logfFatal 'FILE passed is not inside a git repo:\n\t%s\n' "$flag_repotarget"
  if (( flag_progressux )); then
    logfInfo 'Watching file "%s" and commiting to:\n\t%s\n' \
      "$(basename "$flag_repotarget")" \
      "$(readlink -f "$(dirname "$flag_repotarget")")" \
      >&2
  fi
fi

vcs_git_is_clean ||
  mustSaveContent 'dirty repo at start of watch' startup >&2

while read when what dirChild events; do
  if shouldIgnoreFilesystemActivity; then
    if (( flag_progressux )); then
      getProgressChar >&2
    fi
    continue # we're ignoring this event
  fi

  if (( flag_progressux )); then
    echo >&2 # simple line break to cleanup above's progresschars floating about
  fi

  when_iso8601="$(date --iso-8601=s --date="@$when")"
  printf -v commit_message -- 'changes=%s\n\nwhen=%s' \
    "$events" \
    "$when_iso8601"
  mustSaveContent "$commit_message" >&2

  # turn self_tag into a verb by puttign it in past tense
  # specifically on stdout API of this script
  printf -- \
    '%sd:%s\n' \
    "$self_tag" \
    "$when_iso8601"
done < <(listFilesystemChangesInPwd)
