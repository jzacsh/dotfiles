#!/usr/bin/env bash

function printfPrefix() {
  local prefix="$1" tmpl="$2"; shift 2

  local prefixed_tmpl='['"$prefix"'] '"$tmpl"
  printf -- "$prefixed_tmpl" "$@"
}

function die() { printfPrefix fatal "$@" >&2; exit 1; }

slug="$(basename "$(readlink -f "${BASH_SOURCE[0]}")")" ||
    die 'internal bug: failed to figure out own script name\n'

usage_line='BASE_ANCESTOR_REV COMMAND'
function usage() { die 'usage: '"${slug} ${usage_line} ${1:-''}\n"; }

[[ $# -eq 2 ]] || usage '

Description:
  Walks back from BASE_ANCESTOR_REV to the current REV running COMMAND with each
  rev checked out, starting from the oldest rev first
'

function isVcsClean() {
  local lines_of_hgst
  lines_of_hgst="$(hg status | wc -l)" ||
      die 'failed to run hg (nots in a VCS repo?)\n'

  [[ -n "$lines_of_hgst" ]] || die 'internal bug: could not run `wc` count\n'

  [[ "$lines_of_hgst" -eq 0 ]]
}

isVcsClean || die 'vcs is not currently clean; commit or discard and rerun\n'

base_ancestor="$1"
[[ -n "$base_ancestor" ]] || usage 'missing BASE_ANCESTOR_REV'

cmd="$2"
[[ -n "$cmd" ]] || usage 'missing COMMAND'


start_node="$(hg log -r . --template '{node}')" ||
    die 'failed to determine _current_ node\n'

base_node="$(hg log -r "$base_ancestor" --template '{node}')" ||
    die "could not read metadata about ancestor rev ('%s')\n" "$base_ancestor"

function listAncestory() {
  hg log --follow --prune "$base_node" --rev . --template '{node}\n'
}

listAncestory >/dev/null 2>&1 ||
    die 'failed to list ancestors of curent checkout\n'

ancestory_length="$(listAncestory | wc -l)"
[[ "$ancestory_length" -gt 0 ]] || logF 'empty ancestory'

# End of prelim setup #########################################################
###############################################################################

mktmp_tmplt="$(date +"$slug"_logs_%F-%s_XXXXXXX.d)" ||
    die 'internal bug: failed to start logs-dir name\n'
logs_dir="$(mktemp --directory --tmpdir= "$mktmp_tmplt")" ||
    die 'failed to start logs-dir\n'

root_log="$logs_dir"/"$slug".log
touch "$root_log" ||
    die 'failed to write to root log file:\n\t"%s"\n' "$root_log"

function log() { printfPrefix status "$@" | tee -a "$root_log" >&2; }
function logF() {
  printfPrefix warning \
      'leaving you in current node; `hg checkout %s` to go back\n' \
      "$start_node" >&2
  die "$@" |& tee -a "$root_log" >&2

  exit 1
  # must duplicate this else exit of die (being in subshell) would be ignored
}

log 'running command on %s ancestors....\n' \
  "$ancestory_length"

i=1
while read parentNode; do
  log 'running command for node (%d of %s) "%s"....\n' \
    $i "$ancestory_length" \
    "$parentNode"
  hg checkout "$parentNode" ||
     logF 'failed to checkout node "%s"\n' "$parentNode"

  node_log="$logs_dir"/node-"$parentNode".log
  touch "$node_log" ||
      logF 'failed to write to log file for node:\n\t"%s"\n' "$node_log"

  fail_log="$logs_dir"/FAIL-"$parentNode".log
  {
    time bash -c "$cmd" || touch "$fail_log"
  } |& tee -a "$node_log"
  if [[ -e "$fail_log" ]]; then
    logF 'command failed on node "%s"\n' "$parentNode"
  fi

  isVcsClean || logF 'command modified state; commit and rerun\n'

  i=$(( i + 1 ))
done < <(listAncestory | tac)

log \
  'done running command on all %d ancestors; logs in and around:\n\t%s\n' \
  "$ancestory_length" \
  "$root_log"
hg checkout "$start_node"
