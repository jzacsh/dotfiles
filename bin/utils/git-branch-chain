#!/usr/bin/env bash
#
# Manage chains of dependent branches, when you can't use something better like
# jj vcs to manage your clunky git repo.

###############################################################################
# Common Bash~esque Lib #######################################################
self="$(basename "$(readlink -f "${BASH_SOURCE[0]}")")"

# like printf, but prefixes current script's basename for easier log
# identification
function _logf() {
  local given_fmt="$1"; shift

  local fmt; printf -v fmt -- \
    '[%s]: %s' \
    "$self" \
    "$given_fmt"

  # shellcheck disable=SC2059
  printf -- "$fmt" "$@"
}

function logf() { _logf "$@"; }
function errf() { _logf "$@" >&2; }
function dief() { errf "$@"; exit 1; }

source yabashlib || dief 'failed to find dep "yabashlib"\n'

function checkStr() {
  local str="$1" arg_label="$2"
  strIsContentful "$1" || dief \
    '%s must be non-empty string; got: "%s"\n' \
    "$arg_label" "$str"
  echo "$str"
}

requireCallable git

declare -r acceptedOpts='hu'
function usage() {
  cat  <<EOF_HELP
$self: Manage chains of dependent branches.

Only necessary when you can't use something better like jj vcs to manage your
clunky git repo.

usage: $self -u -- BRANCH_1 BRANCH_2 [...BRANCH_N]
usage: $self [$acceptedOpts]

-u Update branch chains

    Given branches BRANCH_1 BRANCH_2 will run a chain of merges to ensure the
    history looks like:

       BRANCH_1 -> BRANCH_2

    This is useful if you have a chain of work that is segmented into branches
    that are dependent on each other (a concept too high-level for first-class
    support in git).

    Example Usage:

        Imagine you've written 2 commits to a new PREREFACTOR branch off main
        and now you have a BUGFIX branch (itself with 3 commits) that depends on
        PREREFACTOR.

        ...--> a --> b (main)
                      \
                       c --> e (PREFACTOR)
                              \
                               f --> g --> h (BUGFIX)

        GOAL: At the end of this example you'll have run '$self' one time and
        your history will represent a logical chain like the above.

        Now suppose you realize you need one more tweak, commit i, on PREFACTOR
        branch, so you 'git switch PREFACTOR'  do your work and commit. Your
        history will look like:

        ...--> a --> b (main)
                      \
                       c --> e --> i (PREFACTOR)
                              \
                               f --> g --> h (BUGFIX)

        Now obviously this is non-ideal as the nature of BUGFIX being
        "dependent" on PREFACTOR is you need BUGFIX to get all of PREFACTOR's
        history. So you can run '$self -u' to run all the annoying merges
        (particularly annoying with longer chains of branches than just the two
        we show here). Suppose it's even worse: upstream has landed commit 'j'
        on 'main' so now PREFACTOR needs a merge commit.

        ...--> a --> b --> j (main)
                      \
                       c --> e --> i (PREFACTOR)
                              \
                               f --> g --> h (BUGFIX)

        Here's how to fix this with '$self': identify which ref is already
        up-to-date and which are the dependencies (in correct order). Pass the
        stable ref as the first arg and the chain in order as remaing args. In
        our example the first graph showed the logical desired chain: main,
        PREFACTOR, BUGFIX. So we fix like this:

          $ $self -u -- main PREFACTOR BUGFUX

        ...--> a --> b --> j (main)
                           \
                            c' --> e --> i (PREFACTOR)
                                          \
                                           f' --> g --> h (BUGFIX)

        Note: c has become c' and f has become f', to indicate the commits will
        be a little more noisy than the above because of merge commits. But
        you'll effectively have re-established the chain we describe here.
EOF_HELP
}

function usagef() {
  local fmt="$1"; shift
  dief \
    'usage error (see -h for help): '"$fmt" \
    "$@"
}

###############################################################################
# General Setup (arg parsing, etc) ############################################

declare -a opts=(
  [u]=0 # -[u]pdate mode
)
function mode_update() { (( opts[u] )); }

# $OPTARG will hold args for "opt" that had a colon after it; $OPTIND might also
# be useful; for more see bash man page:
# https://manpages.debian.org/bookworm/bash/bash.1.en.html#getopts
while getopts "$acceptedOpts" actual_opt; do
  case "$actual_opt" in
    h) usage; exit;;
    u) opts[$actual_opt]=1;;
    '--') break;;
    ?) usagef 'bad arg passed, see err above\n';;
  esac
done
shift $(( OPTIND - 1 ))

mode_update || dief 'only mode -u implemented\n'

[[ "$#" -ge 2 ]] || dief \
  'require root branch and 1+ dependency chain; but got %s branches: %s\n' "$#" "$*"

declare -r rootBranch="$(checkStr "$1" BRANCH_1)"; shift

declare -a chain=()
for branch in "$@";do
  if strStartsWith "$branch" '-'; then
    dief \
      'expected BRANCH_N values after flag terminator "--", but got flag: "%s"\n' \
      "$branch"
  fi

  chain+=( "$(checkStr "$branch" BRANCH_N)" )
done

###############################################################################
# main: actuall do work we advertised #########################################

function gitRun() {
  printf '+ git %s\n' "$*"
  term_confirm 'run above?' ||
    dief 'stopping per user-request\n'
  git "$@" ||
    dief 'git failed running:\n\tgit %s\n' "$*"
}


logf \
  'START: updating %s dependent branches along chain:\n  %s\n' \
  "${#chain[@]}" "$(
      printf -- ' --> %s ' "${chain[@]}"
      echo
  )"
gitRun switch "$rootBranch"
prev="$rootBranch"
for branch in "${chain[@]}";do
  gitRun switch "$branch"
  gitRun merge "$prev"
  prev="$branch"
done


logf 'START: updating default remote for each branch...\n'
gitRun switch "$rootBranch"
gitRun push
for branch in "${chain[@]}";do
  gitRun switch "$branch"
  gitRun push
done
