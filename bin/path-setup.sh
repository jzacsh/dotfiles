#!/usr/bin/env bash

# Useful for some runtime logic - eg for symlinks or other artifacts that get
# dropped into "$DOTFOREST_BIN"/local
export DOTFOREST_BIN="$DOTFOREST_ROOT"/bin

export PATH="$DOTFOREST_BIN"/drop:"$PATH"
export PATH="$DOTFOREST_BIN"/utils:"$PATH"
export PATH="$DOTFOREST_BIN"/local:"$PATH"

function dotforest_extend_path() {
  export PATH="$DOTFOREST_BIN"/lib:"$PATH"
}
