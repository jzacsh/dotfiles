# If not running interactively, don't do anything
[[ ${-} = ${-/i/} ]] && return

# WARNING: do NOT set umask here; see "$HOSTFOREST_ROOT"/pick and "$HOSTFOREST_ROOT"/common/umask

# shell opts
shopt -s cdspell
shopt -s extglob
shopt -s histverify
shopt -s histappend
shopt -s no_empty_cmd_completion
shopt -s dirspell
set -o notify
set -o histexpand
set -o vi

# disable control character echoing
stty -ctlecho

# disable XON/XOFF flow control ("freeze" via ^q, ^s)
stty -ixon

# history options
export HISTIGNORE="&:ls:[bf]g:exit:hg in:hg out:reset:clear:ca:cl:l:cd*"
export HISTFILESIZE=20000
export HISTCONTROL='ignoreboth'
export HISTSIZE=5000

# SMALL subset of env. (safe for root); rest is set in the depths of my
# dotforest: $DOTFOREST_BIN/dotforest/env-exports.sh
export DIFF=' up '
export LESS=' --no-init --quit-if-one-screen  --RAW-CONTROL-CHARS ' # -XFR
export PAGER=less
export RLWRAP=' --ansi-colour-aware --always-readline --no-children ' # -AaN
export LESSOPEN='|lesspipe %s'
# text editor preferences
if type nvim >/dev/null 2>&1;then
  export EDITOR=nvim
else
  export EDITOR=vim
fi

# tput trick from http://unix.stackexchange.com/a/147
# ANSI color table: https://en.wikipedia.org/wiki/ANSI_escape_code#Colors
export LESS_TERMCAP_mb="$(tput bold; tput setaf 2)" # green: 2
export LESS_TERMCAP_md="$(tput bold; tput setaf 1)" # cyan: 6
export LESS_TERMCAP_me="$(tput sgr0)"
export LESS_TERMCAP_so="$(tput bold; tput setaf 3; tput setab 4)" # yellow on blue
export LESS_TERMCAP_se="$(tput rmso; tput sgr0)"
export LESS_TERMCAP_us="$(tput smul; tput bold; tput setaf 7)" # white
export LESS_TERMCAP_ue="$(tput rmul; tput sgr0)"
export LESS_TERMCAP_mr="$(tput rev)"
export LESS_TERMCAP_mh="$(tput dim)"
export LESS_TERMCAP_ZN="$(tput ssubm)"
export LESS_TERMCAP_ZV="$(tput rsubm)"
export LESS_TERMCAP_ZO="$(tput ssupm)"
export LESS_TERMCAP_ZW="$(tput rsupm)"

# Escape Hatch
#
# Launches a Bash shell without any .rc customizations
# Useful for testing behavior that might be impacted by your environment.
# https://stackoverflow.com/a/41554230/113632
#
# This function will pass additional arguments to Bash, e.g. `-c 'foo'` will
# invoke foo in a pristine shell non-interactively.
#
# Taken from https://github.com/dimo414/util.gem/blob/b02c3320/functions.sh#L301-L318
bash_pristine() {
  # Persist a few core environment variables, if they're set
  local env_vars=(
    ${DISPLAY:+"DISPLAY=${DISPLAY}"}
    ${HOME:+"HOME=${HOME}"}
    ${LANG:+"LANG=${LANG}"}
    ${PATH:+"PATH=${PATH}"}
    ${TERM:+"TERM=${TERM}"}
    ${USER:+"USER=${USER}"}
  )
  env -i "${env_vars[@]}" bash --noprofile --norc "$@"
}

# Source "$1" iff it exists; else we don't care.
function sourceMaybe() { [[ -e "$1" ]] || return; source "$1"; }

function sourceOrComplain() {
  [[ -s "$1" ]] || {
    printf 'ERROR: sourcing requires non-empty file: %s\n' "$1" >&2
    return 1
  }
  source "$1"
}

# NOTE: we don't sourceOrComplain here because these aren't guaranteed paths
# (eg: see nixos which guarantees these things are done by the system for us
# anyway).
sourceMaybe /usr/share/bash-completion/bash_completion
# autocompletion for man pages
sourceMaybe /usr/share/bash-completion/completions/man
complete -F _man -- mann

# by default source only autocompletes files, but we wnat it to also
# autocomplete scripts in our $PATH
complete -fc source

# WARNING: this block of code should be kept in sync, manually, with
# "$DOTFOREST_BIN"/dotforest/bash-prompt.sh
#
# if we detect[1] "bash preexec" in our environment, then we cannot use either
# of PROMPT_COMMAND or `trap debug` anywhere.
# [1]: detection logic, per https://github.com/rcaloras/bash-preexec#library-authors
if [[ -n "${bash_preexec_imported:-}" || "${__bp_imported:-}" = defined ]]; then
  function dotforest_retValSaver() { RET=$?; }
  precmd_functions+=( dotforest_retValSaver )
else
  PROMPT_COMMAND=( 'RET=$?' ) # see: man bash | less +/PROMPT_COMMAND
fi
PS1='\s^$RET  @\t \w\n\u@\h   $SHLVL:\$ ' # vcprompt-less version of below

############################################################################
# $UID > 0 BELOW THIS LINE
#   everything above *should* be plain old shell options, nothing executable
[[ "$UID" -ne "0" ]] || return 0
############################################################################

# Potentially _stop_ running remaining bashrc logic.
#
# Useful for quickly disabling most of my bashrc logic, while also logging the
# incidence to the signal file.
#
# INSTRUCTIONS:
# - in emergencies: `touch ~/.config/user.$USER.pristinelogin`
# - when debugging: `tail -f ~/.config/user.$USER.pristinelogin` and login
#   elsewhere.
# - to return to normal: `rm ~/.config/user.$USER.pristinelogin`
function should_continue_fancy_login() {
  local signal_file=~/.config/user."$USER".pristinelogin
  [[ -e "$signal_file" ]] || return 0
  printf 'SEVERE[%s]: found "%s"\n\t%s\n\t%s\n' \
      "$(date --iso-8601=ns)" \
      "$signal_file" \
      'ending non-root bashrc logic (see also `type bash_pristine`)' \
      'rm file to disable\n' |
    tee -a "$signal_file" >&2
  return 1
}
should_continue_fancy_login || return 0; unset should_continue_fancy_login

############################################################################
# Beware dragons below.... #################################################

# The repo holding this ~/.bashrc; for me, usually ~/back/dots
export DOTFOREST_ROOT="$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")"

# Note: this *must* happen early as it provides yabashlib and other things into path:
sourceOrComplain "$DOTFOREST_ROOT"/bin/path-setup.sh # depends on $DOTFOREST_ROOT

# provides dotforest_log, etc.; depends on above $PATH mods
sourceOrComplain "$DOTFOREST_BIN"/dotforest/setup.sh # depends on $DOTFOREST_ROOT

# this must be early on, but probably not safe for root
sourceOrComplain "$DOTFOREST_BIN"/dotforest/env-exports.sh

# Here ends the space that's extremely specific to my dotfiles repo's layout
# Below this line everything relies on $[DOTFOREST_ROOT}-relative paths
############################################################################

# Redefine sourceOrComplain with same logger used below
function sourceOrComplain() {
  [[ -s "$1" ]] || {
    dotforest_log warn 'bashrc skip: sourcing requires non-empty file: %s\n' "$1"
    return 1
  }
  source "$1"
}

# before running anything complicated below, save our $PWD so scripts can change their $PWD without
# messing up my shell's landing place
bashrc_landing="$(pwd)"

sourceOrComplain "$DOTFOREST_BIN"/dotforest/bash-prompt.sh
dotforest_setPrompts

dotforest_log info 'dircolors and other environment variables\n'
if [[ -r ~/.dircolors ]] && type dircolors >/dev/null 2>&1;then
  eval $(dircolors --bourne-shell ~/.dircolors)
fi
if type systemctl >/dev/null 2>&1;then
  dotforest_log info 'detected systemd, importing environ\n'
  systemctl --user import-environment PATH
  systemctl --user import-environment DISPLAY
fi

# Personal library of shell helpers (built _atop_ yabashlib); prereq for
# $HOSTFOREST_ROOT and the hostforest itself.
#
# WARNING: this is purposely _before_ autocompletions and bash_alias code runs
# so that hostforest can modify state first.
if sourceOrComplain "$DOTFOREST_ROOT"/.host/common/hostforest; then
  # Dynamic config (aka "dotforest")
  sourceOrComplain "$HOSTFOREST_ROOT"/pick
fi

# Auto-generated aliases and sources placed by some part of the above dotforest
if isFilledBashGlob ~/usr/local/source/'*.sh' >/dev/null 2>&1; then
  for s in ~/usr/local/source/*.sh; do source "$s"; done
fi

# TODO reconcile where very-general purpose things (running as often as .bashrc) should really live;
# places currently:
# - this repo's top-level
# - .host/common/ (as with the two examples below).
# - .host/common/allhosts (called by pick script)
# - bin/ directory recently created
"$DOTFOREST_BIN"/dotforest/vim-setup.sh # basic setup I always want run for vim usage
sourceOrComplain "$DOTFOREST_BIN"/lib/vcsq

sourceOrComplain "$DOTFOREST_BIN"/lib/bash-completion.sh

dotforest_log info 'sourcing custom ~/.bash_aliases\n'
sourceOrComplain ~/.bash_aliases

# in my nested tmux shells, my inherited `env` is old
dbusSessionBusAddress="$(cat ~/.dbus_address 2>/dev/null)"
if [[ "${dbusSessionBusAddress:-x}" != x ]] &&
   [[ "$DBUS_SESSION_BUS_ADDRESS" != "$dbusSessionBusAddress" ]];then
  if [[ -n "$DBUS_SESSION_BUS_ADDRESS" ]];then
    dotforest_log warn \
      'Blowing away _current_ $%s\n\tfrom: "%s"\n\t  to: "%s"\n' \
      DBUS_SESSION_BUS_ADDRESS \
      "$DBUS_SESSION_BUS_ADDRESS" "$dbusSessionBusAddress"
  else
    dotforest_log info \
      'resurrecting _some_ old $DBUS_SESSION_BUS_ADDRESS (%s) from ~/.dbus_address\n' \
      "$dbusSessionBusAddress"
  fi
  export DBUS_SESSION_BUS_ADDRESS="$dbusSessionBusAddress"
fi
unset dbusSessionBusAddress

# TODO: can probably kill this dbus noise now:
# for irssi's notify.pl
[[ "${DBUS_SESSION_BUS_ADDRESS:-x}" = x ]] ||
  echo "$DBUS_SESSION_BUS_ADDRESS" > ~/.dbus_address

############################################################################
# External Configuration. Provided by libraries, etc.; not written by me, or
# widely standard.
############################################################################

# AWS's CLI completion provided via pip installer
sourceMaybe ~/.local/bin/aws_bash_completer

sourceOrComplain ~/.hgbashrc

sourceMaybe "$CARGO_HOME/env" # rustup wants this

if type pip >/dev/null 2>&1; then
  source <(
    pip completion --bash ||
      dotforest_log err 'failed generating pip bash-completions\n'
  )
fi

if type jj >/dev/null 2>&1; then
  # TODO hunt down and fix this bug and remove this guard...
  # ... bug: as of `jj --version` 0.23.0 we see:
  #```sh
  # $ source <(COMPLETE=bash jj)
  # @: command not found
  # bash: /dev/fd/63: line 2: syntax error near unexpected token `no'
  # bash: /dev/fd/63: line 2: `│  (no description set)'
  #```
  if [[ "$(jj --version)" != 'jj 0.23.0' ]]; then
    # Install jj (VCS)'s  "dynamic" completion (attempts to complete branches,
    # revs, etc, not just its own static artifacts like CLI flags/sub-CMDs).
    source <(COMPLETE=bash jj) ||
      dotforest_log err 'failed generating jj dynamic bash completion\n'
  fi
fi

# Assumes you have ocaml setup, ie:
#   1) once: installed ocaml (and its pkg mgmt system, "opam", etc, etc.)
#   2) once: run `opam init`
#   3) optionally, once: installed vim ocaml things && `opam install ocp-indent`
sourceMaybe ~/.opam/opam-init/init.sh

# need `rbenv init` github.com/rbenv/rbenv#using-package-managers
if type rbenv >/dev/null 2>&1;then
  eval "$(rbenv init -)"
fi

################################################################################
# This section for things that might prompt me, which I might interrupt; if i do
# then the rest of my ~/.bashrc will not run
#
# Therefore anything below this section should be a nice-to-have only!
################################################################################

if sourceOrComplain "$DOTFOREST_BIN"/dotforest/setup-ssh-agent.sh; then
  dotforest_sshsetup_setupSshAgent && ( dotforest_sshsetup_autoAddSshKeys; )
fi

######################################################
# Below this line is strictly for _messages_ to myself
######################################################

dotforest_log info 'DONE most of ~/.bashrc; just helpful messages below:\n'

# Highlight currently authenticated keys
dotforest_sshsetup_impatientListKeys |
    grep --color=always --extended-regexp '^|\.ssh/.*\s' || true
#NOTE: this should be first, since it always prints

# Users
if who | grep --quiet --invert-match "$(whoami)"; then
  dotforest_log warn 'currently on %s, other than you:\n' "$(uname -snr)"
  who --heading | grep --invert-match "$(whoami)"
fi

#Laughs:
# curl --silent --location --connect-timeout 0.06 \
#     http://whatthecommit.com/index.txt 2>/dev/null

#Tmux
scowerForTmuxSessions() (
  [[ "${TMUX:-x}" = x ]] || return 0 # do not run inside tmux

  local firstPrinting=1
  local col_end='\033[0m'; local col_grn='\e[0;32m'
  local commonTmSocks=(default main "${USER}main")
  for sock in "${commonTmSocks[@]}"; do
    local tmSessions="$(tmux -L "$sock" list-sessions 2>/dev/null)"
    [[ "${tmSessions:-x}" != x ]] || continue

    if (( firstPrinting ));then
      havePrinted=0
      dotforest_log warn 'found some tmux sessions (rettach via `tmux attach -t NAME`):\n'
    fi
    printf "tmux -L ${col_grn}%s${col_end} sessions:\n" "$sock"
    printf '%s\n' "$tmSessions" |
        GREP_COLORS='mt=01;33' \grep --color=always '^\w*:'
  done
)
scowerForTmuxSessions; unset scowerForTmuxSessions

# Print local mail waiting for me
scowerForMail() (
  local mailSpool=/var/mail/"$(whoami)"
  [[ -s "$mailSpool" ]] || return 0
  dotforest_log err \
    'Unread local mail for some reason (`mail` to delete, manage them)\n%s\n' \
    "$mailSpool"
)
scowerForMail; unset scowerForMail

# notify myself when degrated state
if type systemctl >/dev/null 2>&1 &&
  ! ( systemctl --user --state=failed | grep --quiet --extended-regexp '^0 loaded units'; );then
  dotforest_log err 'systemctl failures above; investigate with:\n\t%s # %s\n' \
    'systemctl --user list-units --state=failed' \
    '`systemctl --user status` for an overall report'
fi

# just once, as i typically only one terminal and the rest is tmux window/pane shells
if [[ "$SHLVL" -eq 1 ]];then
  if type neofetch >/dev/null 2>&1;then
    neofetch
  fi
fi

# WARNING: this is *purposely* the last thing in this script (that's a promise
# to implementors of hostforest_bashrc_tailrc).
hostforest_run_bashrc_tailrc # made available by "$HOSTFOREST_ROOT"/pick

cd "$bashrc_landing"

unset bashrc_landing sourceOrComplain

true # don't assume last return status
